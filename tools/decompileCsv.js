const fs = require('fs').promises;
const path = require('path');
const { spawn } = require('child_process');
const { grepFile, makeSafeForFolder } = require('../libs/utils.js')

const inputCsvPath = process.argv[2] || 'openImagesData/oidv6-train-annotations-bbox.csv';
const minCharLength = parseInt(process.argv[3]) || 16;
const columnNumbers = process.argv[4] || "0";
let sourceFilename = inputCsvPath.split('/');
sourceFilename = sourceFilename[sourceFilename.length - 1].split('.');
sourceFilename = sourceFilename[0];
const filename = process.argv[5] || sourceFilename;
const outputDir = `decompiled/${filename}`;

async function processCsv() {
    const imageIdData = {};

    // Create the decompiled directory if it doesn't exist
    try {
        await fs.mkdir(outputDir, { recursive: true });

        // Read the CSV file and organize data by ImageID
        console.log(`Decompiling ${inputCsvPath}...`)
        await grepFile('^', inputCsvPath, (line) => {
            const row = line.split(',');
            columnNumbers.split(',').forEach((columnNumber) => {
                const imageId = (row[parseInt(columnNumber)] || '').trim(); // Assuming ImageID is the first column
                const saveIt = imageId && imageId.length > 2 && (minCharLength === 99 || imageId.length === minCharLength);
                if(saveIt){
                    if (!imageIdData[imageId]) {
                        imageIdData[imageId] = [];
                    }
                    imageIdData[imageId].push(line);
                }
            })
        });

        // Write data to separate CSV files organized by ImageID
        console.log(`Decompiled! ${inputCsvPath}...`)
        console.log(`Saving to ${outputDir}...`)
        for (const imageId in imageIdData) {
            const filePath = path.join(outputDir, `${makeSafeForFolder(imageId)}.csv`);
            await fs.writeFile(filePath, [... new Set(imageIdData[imageId])].join('\n'));
        }
        console.log(`Saved at ${outputDir}!`)

        console.log('CSV files have been created in the decompiled directory.');
    } catch (error) {
        console.error(`Error processing CSV: ${error.message}`,error);
    }
}

processCsv().catch(console.error);
