#!/bin/bash

# Directory to store the weights
WEIGHTS_DIR="./weights"
mkdir -p $WEIGHTS_DIR
mkdir -p './datasets'
mkdir -p './openImagesData'
mkdir -p './decompiled'

# URLs for YOLOv5 pre-trained weights
Yolov5URLPrefix="https://github.com/ultralytics/yolov5/releases/download/v7.0"
Yolov5nURL="$Yolov5URLPrefix/yolov5n.pt"
Yolov5sURL="$Yolov5URLPrefix/yolov5s.pt"
Yolov5mURL="$Yolov5URLPrefix/yolov5m.pt"
Yolov5lURL="$Yolov5URLPrefix/yolov5l.pt"
Yolov5xURL="$Yolov5URLPrefix/yolov5x.pt"
# 6 models
yolov5n6URL="$Yolov5URLPrefix/yolov5n6.pt"
Yolov5s6URL="$Yolov5URLPrefix/yolov5s6.pt"
Yolov5m6URL="$Yolov5URLPrefix/yolov5m6.pt"
Yolov5l6URL="$Yolov5URLPrefix/yolov5l6.pt"
Yolov5x6URL="$Yolov5URLPrefix/yolov5x6.pt"
# ... add URLs for other models (yolov5l.pt, yolov5x.pt) if needed

# Function to download weight if not exists
download_weight() {
    local url=$1
    local file_path="$WEIGHTS_DIR/$(basename $url)"

    if [ ! -f "$file_path" ]; then
        echo "Downloading $(basename $url) ..."
        wget -O "$file_path" "$url"
    else
        echo "$(basename $url) already exists."
    fi
}

# Download the weights
download_weight $Yolov5nURL
download_weight $Yolov5sURL
download_weight $Yolov5mURL
download_weight $Yolov5lURL
download_weight $Yolov5xURL
# 6 models
download_weight $Yolov5n6URL
download_weight $Yolov5s6URL
download_weight $Yolov5m6URL
download_weight $Yolov5l6URL
download_weight $Yolov5x6URL
# ... download other weights if needed

echo "Weights download completed."

# Function to download dataset if not exists
# download_dataset() {
#     local url=$1
#     local file_path="./openImagesData/$(basename $url)"
#
#     if [ ! -f "$file_path" ]; then
#         echo "Downloading $(basename $url) ..."
#         wget -O "$file_path" "$url"
#     else
#         echo "$(basename $url) already exists."
#     fi
# }
#
# # Download datasets
# download_dataset https://storage.googleapis.com/openimages/v5/train-annotations-human-imagelabels-boxable.csv
# download_dataset https://storage.googleapis.com/openimages/v6/oidv6-train-annotations-bbox.csv
# download_dataset https://storage.googleapis.com/openimages/v7/oidv7-class-descriptions-boxable.csv
# download_dataset https://storage.googleapis.com/openimages/v6/oidv6-train-annotations-vrd.csv
#
# echo "Decompiling CSVs for performance."
# echo "This may take some time and should only need to be done once."
# node tools/decompileCsv.js "openImagesData/oidv6-train-annotations-bbox.csv" 16 "0"
# node tools/decompileCsv.js "openImagesData/oidv6-train-annotations-vrd.csv" 16 "0"
# node tools/decompileVrdCsvToId.js
# node tools/decompileTahibCsvToId.js
# node tools/decompileCsv.js "openImagesData/oidv7-class-descriptions-boxable.csv" 99 "1"
