const fs = require('fs').promises;
const path = require('path');
const { spawn } = require('child_process');
const { grepFile, makeSafeForFolder } = require('../libs/utils.js')

async function processCsv() {
    const imageIdData = {};
    const humanNames = {};
    const humanNameListPath = 'openImagesData/oidv7-class-descriptions-boxable.csv';
    const inputCsvPath = 'openImagesData/oidv6-train-annotations-vrd.csv';
    const columnNumbers = "1,2";
    const sourceFilename = "oidv6-train-annotations-vrd";
    const filename = "oidv6-train-annotations-vrd-by-obj";
    const outputDir = `decompiled/oidv6-train-annotations-vrd-by-obj`;
    const humanNamesRaw = (await fs.readFile(humanNameListPath,'utf8')).split('\n').forEach((item) => {
        var parts = item.split(',');
        if(parts[0] && parts[1]){
            humanNames[parts[1].trim()] = parts[0];
        }
    });
    console.log(humanNames)
    console.log('humanNames2',(process.argv[2] || ''))
    const restrictTo = (process.argv[2] || '').split(',').map(labelName => humanNames[labelName]).filter(item => !!item);

    console.log(`restrictTo`, restrictTo)

    // Create the decompiled directory if it doesn't exist
    try {
        await fs.mkdir(outputDir, { recursive: true });

        // Read the CSV file and organize data by ImageID
        var iteration = 0;
        var currentNumber = 0;
        var timeStarted = new Date();
        var lastTime = new Date();
        await grepFile('^', inputCsvPath, (line) => {
            const row = line.split(',');
            const imageId = row[0];
            if(row[1] && row[2] && row[1].startsWith('/m/') && row[2].startsWith('/m/')){
                columnNumbers.split(',').forEach((columnNumber) => {
                    const objectId = (row[parseInt(columnNumber)] || '').trim();
                    if(restrictTo.length === 0 || restrictTo.indexOf(objectId) > -1){
                        if (!imageIdData[objectId]) {
                            imageIdData[objectId] = [];
                        }
                        imageIdData[objectId].push(line);
                    }
                });
                if(iteration === 10000){
                    iteration = 0;
                    var timeNow = new Date();
                    console.log(`Completed : ${currentNumber}, ${(timeNow - lastTime) / 1000}s`)
                    lastTime = timeNow;
                }
                ++iteration;
                ++currentNumber;
            }
        });

        // Write data to separate CSV files organized by ImageID
        for (const imageId in imageIdData) {
            const filePath = path.join(outputDir, `${makeSafeForFolder(imageId)}.csv`);
            await fs.writeFile(filePath, ([...new Set(imageIdData[imageId])]).join('\n'));
        }

        console.log('CSV files have been created in the decompiled directory.');
    } catch (error) {
        console.error(`Error processing CSV: ${error.message}`,error);
    }
}

processCsv().catch(console.error);
