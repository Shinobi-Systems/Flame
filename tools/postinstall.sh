#!/bin/bash

dos2unix ./tools/downloadYolo.sh
sh ./tools/downloadYolo.sh
dos2unix ./tools/downloadWeights.sh
sh ./tools/downloadWeights.sh
if [ -f conf.json ]; then
    echo 'Config Exists'
else
    cp conf.sample.json conf.json
fi
