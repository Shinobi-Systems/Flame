#!/bin/bash
set -e # Exit immediately if a command exits with a non-zero status.

# Check if GCC 10 is already installed
if [ -x "$(command -v gcc-10)" ]; then
    echo "GCC 10 is already installed."
    exit 0
fi

# Function to detect package manager
detect_package_manager() {
    if command -v dnf > /dev/null 2>&1; then
        echo "dnf"
    elif command -v yum > /dev/null 2>&1; then
        echo "yum"
    elif command -v apt > /dev/null 2>&1; then
        echo "apt"
    else
        echo "UNKNOWN"
    fi
}

# Try installing gcc-10 from the package manager
install_gcc_from_pm() {
    local PM=$(detect_package_manager)

    case $PM in
    "dnf" | "yum")
        if $PM list gcc-10 &>/dev/null; then
            $PM install -y gcc-10 gcc-c++-10
            echo "GCC 10 installed from $PM."
            exit 0
        fi
        ;;
    "apt")
        if apt-cache policy gcc-10 | grep -q 'Candidate'; then
            sudo apt update
            sudo apt install -y gcc-10 g++-10
            echo "GCC 10 installed from apt."
            exit 0
        fi
        ;;
    esac
}

install_gcc_from_pm

# If not available from the package manager, proceed with manual installation

echo "GCC 10 not found in package manager. Proceeding with manual installation."

# Install Dependencies
install_dependencies() {
    local PM=$(detect_package_manager)

    echo "Detected package manager: $PM"

    case $PM in
    "dnf" | "yum")
        $PM install -y wget gcc-c++ gmp-devel mpfr-devel libmpc-devel zlib-devel
        ;;
    "apt")
        sudo apt update
        sudo apt install -y wget g++ libgmp-dev libmpfr-dev libmpc-dev zlib1g-dev
        ;;
    "UNKNOWN")
        echo "Unsupported package manager!"
        exit 1
        ;;
    esac
}

echo "Installing dependencies..."
install_dependencies

# GCC Version
GCC_VERSION="10.3.0"

# Download GCC Source
echo "Downloading GCC source code..."
cd /tmp
wget https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz

# Extract Archive
echo "Extracting archive..."
tar -xf gcc-$GCC_VERSION.tar.gz

# Create Build Directory
echo "Creating build directory..."
cd gcc-$GCC_VERSION
mkdir build
cd build

# Configure
echo "Configuring..."
../configure --enable-languages=c,c++ --disable-multilib

# Make
echo "Compiling (this may take a while)..."
make -j$(nproc)

# Install
echo "Installing..."
make install

# Verify Installation
echo "Verifying installation..."
gcc --version

echo "GCC $GCC_VERSION has been installed successfully!"
