const mysql = require('mysql');
const spawn = require('child_process').spawn;
const csv = require('csv-parser');
const fs = require('fs');
const readline = require('readline');

class CsvToSql {
    constructor(dbConfig) {
        this.connection = mysql.createConnection(dbConfig);
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.connection.connect(err => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    disconnect() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    inferDataTypes(row) {
        return Object.entries(row).map(([key, value]) => {
            if (!isNaN(parseFloat(value)) && isFinite(value)) {
                return { name: key, type: 'FLOAT' };
            } else {
                return { name: key, type: 'VARCHAR(255)' };
            }
        });
    }

    createTable(tableName, columns) {
        const columnDefs = columns.map(col => `${col.name} ${col.type}`).join(', ');
        const sql = `CREATE TABLE IF NOT EXISTS ${tableName} (${columnDefs})`;

        return new Promise((resolve, reject) => {
            this.connection.query(sql, err => {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    convertToCamelCase(str) {
        return str
            // Remove leading slash and file extension
            .replace(/^\/|\.csv$/g, '')
            // Split on non-alphanumeric characters
            .split(/[^a-zA-Z0-9]/)
            // Capitalize first letter of each word except the first
            .map((word, index) => index === 0 ? word : word.charAt(0).toUpperCase() + word.slice(1))
            // Join words together
            .join('');
    }

    tableExists(tableName) {
        const sql = `SHOW TABLES LIKE '${tableName}'`;
        return new Promise((resolve, reject) => {
            this.connection.query(sql, (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results.length > 0);
                }
            });
        });
    }

    async processCsv(filePath) {
        const tableName = this.convertToCamelCase(filePath);
        const tableAlreadyExists = await this.tableExists(tableName);
        if (tableAlreadyExists) {
            console.log(`Table "${tableName}" already exists. Skipping table creation and data import.`);
            return;
        }

        // Read first line to infer columns
        const firstLine = await this.getFirstLine(filePath);
        const columns = firstLine.split(',').map(header => ({
            name: header.trim(),
            type: 'VARCHAR(255)' // Default type, modify as needed
        }));
        await this.createTable(tableName, columns);

        return new Promise((resolve, reject) => {
            const cat = spawn('cat', [filePath]);
            let buffer = '';
            let isPaused = false;

            cat.stdout.on('data', (data) => {
                buffer += data.toString();
                let lines = buffer.split('\n');
                buffer = lines.pop(); // Keep the last incomplete line in the buffer

                if (!isPaused) {
                    isPaused = true;
                    lines.forEach((line, index) => {
                        if (line) {
                            this.processLine(line, columns, tableName, () => {
                                if (index === lines.length - 1) {
                                    isPaused = false;
                                    cat.stdout.resume();
                                }
                            });
                        }
                    });
                    cat.stdout.pause();
                }
            });

            cat.on('close', () => {
                this.processLine(buffer, columns, tableName, resolve); // Process the last line
            });

            cat.stderr.on('data', data => {
                console.error(`cat stderr: ${data}`);
                reject(data.toString());
            });
        });
    }

    processLine(line, columns, tableName, callback) {
        // Process the line
        const lineParts = line.split(',');
        const row = {};
        lineParts.forEach((item, index) => {
            row[columns[index].name] = item;
        });
        const sqlColumns = Object.keys(row).join(', ');
        const values = Object.values(row).map(value => mysql.escape(value)).join(', ');
        const sql = `INSERT INTO ${tableName} (${sqlColumns}) VALUES (${values})`;
        console.log(sql)
        this.connection.query(sql, (err) => {
            if (err) console.error(err);
            callback();
        });
    }

    getFirstLine(filePath) {
        return new Promise((resolve, reject) => {
            const head = spawn('head', ['-n', 1, filePath]);
            let firstLine = '';

            head.stdout.on('data', data => {
                firstLine += data.toString();
            });

            head.on('close', () => {
                resolve(firstLine.trim());
            });

            head.stderr.on('data', data => {
                console.error(`head stderr: ${data}`);
                reject(data.toString());
            });
        });
    }

    getVrdAnnotations({ imageId = null, relationshipLabel = null, labelName1 = null, labelName2 = null }) {
        let sql = 'SELECT * FROM DatasetsOidv6TrainAnnotationsVrd';
        const conditions = [];

        // Add conditions based on provided parameters
        if (imageId) conditions.push(`ImageID = '${imageId}'`);
        if (relationshipLabel) conditions.push(`RelationshipLabel = '${relationshipLabel}'`);
        if (labelName1) conditions.push(`LabelName1 = '${labelName1}'`);
        if (labelName2) conditions.push(`LabelName2 = '${labelName2}'`);

        // Add WHERE clause if there are conditions
        if (conditions.length > 0) {
            sql += ' WHERE ' + conditions.join(' AND ');
        }

        return new Promise((resolve, reject) => {
            this.connection.query(sql, (err, results) => {
                if (err) reject(err);
                else resolve(results);
            });
        });
    }

    getBboxAnnotations({ imageId = null, labelName = null }) {
        let sql = 'SELECT * FROM DatasetsOidv6TrainAnnotationsBbox';
        const conditions = [];

        // Add conditions based on provided parameters
        if (imageId) conditions.push(`ImageID = '${imageId}'`);
        if (labelName) conditions.push(`LabelName = '${labelName}'`);

        // Add WHERE clause if there are conditions
        if (conditions.length > 0) {
            sql += ' WHERE ' + conditions.join(' AND ');
        }

        return new Promise((resolve, reject) => {
            this.connection.query(sql, (err, results) => {
                if (err) reject(err);
                else resolve(results);
            });
        });
    }

    getHumanImagelabelsBoxableAnnotations({ imageId = null, labelName = null }) {
        let sql = 'SELECT * FROM DatasetsTrainAnnotationsHumanImagelabelsBoxable';
        const conditions = [];

        // Add conditions based on provided parameters
        if (imageId) conditions.push(`ImageID = '${imageId}'`);
        if (labelName) conditions.push(`LabelName = '${labelName}'`);

        // Add WHERE clause if there are conditions
        if (conditions.length > 0) {
            sql += ' WHERE ' + conditions.join(' AND ');
        }

        return new Promise((resolve, reject) => {
            this.connection.query(sql, (err, results) => {
                if (err) reject(err);
                else resolve(results);
            });
        });
    }
}

module.exports = CsvToSql;
