// main.js
process.on('uncaughtException', function (err) {
    console.error(`Uncaught Exception occured! ${new Date()}`);
    console.error(err.stack);
});
const lang = require('../languages/en_CA.js');
const config = require('../conf.json');
const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const http = require('http');
const { Server } = require('socket.io');
const { ensureDirExists } = require('./dirManager.js');
let lastTrainSettings = {}
let lastTrainOnData = {msg: 'stderr', data: {type: 'notTrainedSinceStart'}};
const {
    shutdownSystem,
} = require('./utils.js');
const {
    getFreeMData,
    getNvidiaSmiInfo,
} = require('./sysCheck.js');
const {
    startTraining,
    stopTraining,
    convertWeightsToONNX,
    testLatestWeights,
    testWeights,
    listExperimentFolders,
    listTestResultFiles,
    listTrainResultFiles,
    findNewestExpFolder,
    suggestEpochs,
    getTrainedWeightsPath,
 } = require('./trainingManager.js');
 const {
     getActiveDatasetInfo,
     getClassIdForActiveDataset,
     addClassNameToActiveDataset,
     setActiveDatasetInfo,
 } = require('./trainingManager/activeDatasets.js')

const app = express();
const server = http.createServer(app);
const io = new Server(server);
const port = config.port || 8099;

app.set('view engine', 'ejs');
app.set('views', path.join(process.cwd(), 'public/pages'));

app.use(express.static('public'));
app.use('/yolov5/runs', express.static('yolov5/runs'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./webPaths/datasetViewer.js')(app,lang,config,io)
require('./webPaths/videoAnnotator.js')(app,lang,config,io)
require('./webPaths/openImagesDownloader.js')(app,lang,config,io)
require('./webPaths/savedPackages.js')(app,lang,config,io)
require('./webPaths/customDatasets.js')(app,lang,config,io)
require('./webPaths/testSwitcher.js')(app,lang,config,io)

app.get('/', (req, res) => {
    res.render('index', {
        lang,
        config,
    });
});

app.post('/api/train/start', async (req, res) => {
    const { classes } = await getActiveDatasetInfo();
    const { batchSize, epochs, weights, device, imgSize, doShutdown } = req.body;
    const finalBatchSize = batchSize || 4; // Default batch size
    const finalImgSize = imgSize || 640; // Default batch size
    const sourceWeights = weights ? weights.startsWith('weights/') ? weights : `weights/${weights}` : 'weights/yolov5m.pt'; // Default epochs
    const epochQueue = epochs.split(',')
    try {
        lastTrainSettings = {
            classes,
            imgSize: finalImgSize,
            weights: sourceWeights,
            batchSize: finalBatchSize,
            epochs: epochs,
            device: device,
        }
        async function runner(){
            let anyFailed = false;
            for(epoch of epochQueue){
                if(anyFailed)return;
                try{
                    await startTraining({
                        classes,
                        imgSize: finalImgSize,
                        weights: sourceWeights,
                        batchSize: finalBatchSize,
                        epochs: epoch,
                        device: device,
                        onData: (data) => {
                            lastTrainOnData = data;
                            io.emit('train',data)
                        },
                        io
                    });
                }catch(err){
                    anyFailed = true;
                    io.emit('train',{
                        msg: 'error',
                        error: err
                    });
                    console.error(`Training Error :`,err);
                }
            }
            if(!anyFailed && doShutdown == 'true'){
                shutdownSystem()
            }
        };
        runner();
        res.json({ message: 'Training started', classes, batchSize: finalBatchSize, epochs: epochs });
    } catch (error) {
        console.error(`Error starting training: ${error.message}`);
        res.status(500).json({ message: 'Error starting training', error: error.message });
    }
});


app.post('/api/train/stop', async (req, res) => {
    try {
        await stopTraining();
        res.json({ message: 'Training stopped' });
    } catch (error) {
        console.error(`Error stopping training: ${error.message}`);
        res.status(500).json({ message: 'Error stopping training', error: error.message });
    }
});

app.post('/api/train/test', async (req, res) => {
    let { weightsPath, expFolder, selectedWeights } = req.body;
    let response = {}
    if(weightsPath === 'latest'){
        response = await testLatestWeights((data) => {
            io.emit('trainTest',data)
        });
    }else{
        if(expFolder){
            weightsPath = getTrainedWeightsPath(expFolder,selectedWeights)
        }
        response = await testWeights({
            weightsPath,
            onData: (data) => {
                io.emit('trainTest',data)
            }
        });
    }
    res.json(response);
});

app.post('/api/train/convert', async (req, res) => {
    const { weightsPath, type } = req.body;
    const response = {ok: true}
    try{
        switch(type){
            case'onnx':
                const newOnnxPath = await convertWeightsToONNX({
                    expFolder: undefined,
                    opsetVersion: 11,
                });
                response.path = newOnnxPath;
            break;
        }
    }catch(err){
        response.err = err;
        response.ok = false;
    }
    res.json(response);
});

app.get('/api/train/test', async (req, res) => {
    let response = {ok: true}
    try{
        response.folders = await listExperimentFolders('detect');
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/train/result', async (req, res) => {
    let response = {ok: true}
    try{
        response.folders = await listExperimentFolders('train');
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/train/test/:expFolder', async (req, res) => {
    const expFolder = req.params.expFolder;
    let response = {ok: false}
    try{
        response = await listTestResultFiles(expFolder);
    }catch(err){
        console.log(err)
        response.err = err;
    }
    res.json(response);
});

app.get('/api/train/result/:expFolder', async (req, res) => {
    const expFolder = req.params.expFolder;
    let response = {ok: false}
    try{
        response = await listTrainResultFiles(expFolder);
    }catch(err){
        response.err = err;
    }
    res.json(response);
});

app.get('/api/train/newest/result', async (req, res) => {
    let response = {ok: true}
    try{
        response.exp = await findNewestExpFolder('train');
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/train/newest/test', async (req, res) => {
    let response = {ok: true}
    try{
        response.exp = await findNewestExpFolder('detect');
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/system/gpu', async (req, res) => {
    let response = {ok: true}
    try{
        response.data = await getNvidiaSmiInfo();
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/system/ram', async (req, res) => {
    let response = {ok: true}
    try{
        response.data = await getFreeMData();
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

app.get('/api/system/shutdown', async (req, res) => {
    let response = { ok: true, code: 1 }
    try{
        response.code = await shutdownSystem();
    }catch(err){
        response.ok = false;
        response.err = err;
    }
    res.json(response);
});

server.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});

io.on('connection',function(cn){
    cn.emit('lastTrainSettings', lastTrainSettings)
    cn.emit('train',lastTrainOnData)
    cn.on('formInput',function({
        formName,
        key,
        value,
        senderId
    }){
        io.emit('formInput', {
            formName,
            key,
            value,
            senderId
        })
    })
})
