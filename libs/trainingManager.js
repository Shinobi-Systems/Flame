const fs = require('fs/promises');
const { statSync, createReadStream, createWriteStream } = require('fs');
const path = require('path');
const { spawn } = require('child_process');
const { ensureDirExists } = require('./dirManager.js');
const imageSize = require('image-size');
const ImageDownloader = require('./openImagesDownloader.js');
const openImagesDL = new ImageDownloader();
const trainDir = path.join(process.cwd(), 'yolov5/runs/train');
const savedPackageDir = path.join(process.cwd(), 'savedPackages');
const venvPythonPath = `${__dirname}/../yolov5-venv/bin/python3`
const {
    createZipArchive,
    shutdownSystem,
} = require('./utils.js');
const {
    getActiveDatasetDir,
    getActiveDatasetInfo,
    getClassIdForActiveDataset,
    addClassNameToActiveDataset,
    setActiveDatasetInfo,
    createDatasetYAML,
} = require('./trainingManager/activeDatasets.js');
const {
    getFreeMData,
    getNvidiaSmiInfo,
} = require('./sysCheck.js');

let trainProcess;
let trainProcessStaleTimeout;

function parseTrainProgressLogs(data) {
  const allLines = data.split('\n').filter(line => line.trim()).map(line => {
    const parts = line.split(/\s+/);
    const missingPlace = parts[8] === '|' ? 0 : 1;
    const [epoch, totalEpochs] = parts[0].split('/').map(Number);
    const memory = parts[1];
    const metrics = parts.slice(2, 6).map(Number);
    const progress = parts[7].split('%')[0];
    const [currentStep, totalSteps] = parts[9 - missingPlace].split('/').map(Number);
    const [elapsedTime, remainingTime] = parts[10 - missingPlace].match(/\d+:\d+/g);
    const itPerS = parts[11 - missingPlace].match(/\d+\.\d+it\/s/)[0];
    return {
        epoch: epoch + 1,
        totalEpochs: totalEpochs + 1,
        memory,
        metrics,
        progress,
        currentStep,
        totalSteps,
        elapsedTime,
        remainingTime,
        itPerS,
        type: 'progress',
    };
  });
  return allLines[allLines.length - 1];
}

function parseTrainSummaryStatistics(data) {
    const parts = data.split(/\s+/);

    // Extracting the progress details
    const missingPlace = parts[8] === '|' ? 0 : 1;
    const progress = parts[7].split('%')[0];
    const [current, total] = parts[9 - missingPlace].split('/').map(Number);
    const [elapsedTime, remainingTime] = parts[10 - missingPlace].match(/\d+:\d+/g);
    let itPerS = parts[11 - missingPlace].match(/\d+\.\d+it\/s/);
    if(itPerS){
        itPerS = itPerS[0]
    }else{
        itPerS = '0.00it/s'
    }
    return {
      progress,
      currentStep: current,
      totalSteps: total,
      elapsedTime,
      remainingTime,
      itPerS,
      type: 'summary',
    };
}

function calculateTrainOverallProgress(data) {
  if (data.type !== 'progress') {
    return 'Invalid data type';
  }

  // Calculate current epoch progress
  const currentEpochProgress = (data.currentStep / data.totalSteps) * 100;

  // Adjust the current epoch's progress based on the progress percentage reported
  // If the progress field is a percentage string, parse it as a number
  const adjustedCurrentEpochProgress = isNaN(data.progress) ?
    parseFloat(data.progress) :
    currentEpochProgress;

  // Calculate overall progress
  // (Completed epochs + progress in current epoch) / total epochs
  const overallProgress = ((data.epoch - 1 + (adjustedCurrentEpochProgress / 100)) / data.totalEpochs) * 100;

  return overallProgress.toFixed(2) + '%';
}

function parseTrainString(text) {
    const lines = text.split(/\r?\n|\r|\n/g)
    const input = lines[lines.length - 1].trim();
    try{
        if (input.includes('GPU_mem')) {
          return '';
        } else if (input.includes('Class') && input.includes('Images')) {
          return parseTrainSummaryStatistics(input);
        } else if (input.indexOf('/') < input.indexOf('%') && input.includes('/') && input.includes('%') && input.includes('|') && input.includes('G ')) {
          return parseTrainProgressLogs(input);
        } else {
          return input;
        }
    } catch(err){
        // console.log(err,input.split(/\s+/))
        return input
    }
}

function extractFinalFolderName(str) {
  const parts = str.split('/');
  return parts[parts.length - 3];
}

async function listExperimentFolders(folderTarget) {
    try {
        const targetDir = path.join(process.cwd(), `yolov5/runs/${folderTarget}`);
        const items = await fs.readdir(targetDir, { withFileTypes: true });
        const isTestFolder = folderTarget === 'detect'
        // Filter directories that start with 'exp' and then sort them numerically
        const sortedExpItems = items.filter(item => item.isDirectory() && item.name.startsWith('exp')).sort((a, b) => {
            // Extract the numeric part of the folder names
            const numA = parseInt(a.name.replace('exp', ''), 10);
            const numB = parseInt(b.name.replace('exp', ''), 10);
            return numA - numB; // Compare numerically
        }).reverse();

        const expFoldersPromises = sortedExpItems.map(async dir => {
            const expPath = path.join(targetDir, dir.name);
            const stats = await fs.stat(expPath);
            const infoPath = path.join(expPath,isTestFolder ? 'trainInfo.json' : 'info.json')
            let info = {};
            let classes = ['No Info']
            try{
                info = JSON.parse(await fs.readFile(infoPath,'utf8'));
                classes = info.data.classes;
            }catch(err){
                // console.log(err)
            }
            return {
                name: dir.name,
                ctime: stats.ctime,
                classes: classes,
                info,
            };
        });

        const expFolders = await Promise.all(expFoldersPromises);
        return expFolders;
    } catch (error) {
        console.error(`Error listing 'exp' folders: ${error.message}`);
        return [];
    }
}

async function findNewestExpFolder(folderTarget = 'train') {
    try {
        const targetDir = path.join(process.cwd(), `yolov5/runs/${folderTarget}`);
        const items = await fs.readdir(targetDir, { withFileTypes: true });
        const expFolders = items.filter(item => item.isDirectory() && item.name.startsWith('exp'));

        let newestFolder = null;
        let newestTime = 0;

        for (const folder of expFolders) {
            const folderPath = path.join(targetDir, folder.name);
            const stats = await fs.stat(folderPath);
            if (stats.mtimeMs > newestTime) {
                newestTime = stats.mtimeMs;
                newestFolder = folder.name;
            }
        }

        return newestFolder;
    } catch (error) {
        console.error(`Error finding newest 'exp' folder: ${error.message}`);
        return 'exp';
    }
}

async function listTrainResultFiles(expFolder) {
    try {
        const response = { ok: true, files: [] };
        const expDir = path.join(process.cwd(), `yolov5/runs/train/${expFolder}`);
        const weightsDir = path.join(expDir, '/weights');
        const resultFiles = (await fs.readdir(expDir, { withFileTypes: true })).filter(file => file.isFile()).map(item => item.name);
        const resultWeights = (await fs.readdir(weightsDir)).filter(file => file.endsWith('.pt')).map(file => path.join('weights/', file));
        response.files = resultFiles;
        response.jpegs = resultFiles.filter(file => file.endsWith('.jpg'));
        response.weights = resultWeights;

        return response;
    } catch (error) {
        console.error(`Error listing .pt files: ${error.message}`);
        return { ok: false, files: [], jpegs: [], weights: [], error: error.message };
    }
}

async function listTestResultFiles(expFolder) {
    const result = { ok: true, jpegs: [], files: [] };
    try {
        const folderPrefix = `detect/${expFolder}`
        const detectDir = path.join(process.cwd(), `yolov5/runs/${folderPrefix}`);
        let testedOn = {exp: 'unknown', weightsPath: 'unknown'}
        try{
            testedOn = JSON.parse(await fs.readFile(`${detectDir}/testedOn.json`));
        }catch(err){
            console.log(err.toString())
        }
        const files = await fs.readdir(detectDir);
        result.videos = files.filter(file => file.endsWith('.mp4'));
        result.jpegs = files.filter(file => file.endsWith('.jpg'));
        result.files = files.filter(file => !file.endsWith('.jpg') && !file.endsWith('.mp4'));
        result.testedOn = testedOn;
        return result;
    } catch (error) {
        console.error(`Error listing .jpg files: ${error.message}`);
        return { ok: false, error: error.toString(), jpegs: [], files: [] };
    }
}

async function countJpgFiles(directory) {
    let count = {};

    async function traverseDir(dir) {
        const files = await fs.readdir(dir, { withFileTypes: true });
        if(!count[dir])count[dir] = 0;
        for (const file of files) {
            const fullPath = path.join(dir, file.name);
            if (file.isDirectory()) {
                await traverseDir(fullPath);
            } else if (file.isFile() && path.extname(file.name).toLowerCase() === '.jpg') {
                count[dir]++;
            }
        }
    }

    await traverseDir(directory);
    return count;
}

async function copyTrainingInfoToExp({
    expFolder,
    target = 'train',
    infoToSave
}){
    const datasetPath = path.join(process.cwd(), 'datasets');
    const imagesPath = path.join(datasetPath, 'images');
    const datasetYamlPath = path.join(datasetPath, 'dataset.yaml');
    const expDir = path.join(process.cwd(), `yolov5/runs/train/${expFolder}`);
    const weightsDir = path.join(expDir, 'weights');
    const classesTxtPath = path.join(weightsDir, 'classes.txt');
    const jsonPath = path.join(expDir, 'info.json');
    const imageCount = await countJpgFiles(imagesPath);
    const jsonToSave = {
        exp: expFolder,
        imageCount,
        target,
        data: infoToSave,
        datasetYaml: await fs.readFile(datasetYamlPath,'utf8')
    }
    await fs.writeFile(jsonPath,JSON.stringify(jsonToSave,null,3))
    await fs.writeFile(classesTxtPath,infoToSave.classes.join('\n'))
}

async function copyInfoFromTrainToTest(trainExpFolder,testExpFolder){
    const trainExpInfo = path.join(process.cwd(), `yolov5/runs/train/${trainExpFolder}`,'info.json');
    const trainExpCopy = path.join(process.cwd(), `yolov5/runs/detect/${testExpFolder}`,'trainInfo.json');
    await fs.writeFile(trainExpCopy,await fs.readFile(trainExpInfo));
}

async function createWeightsBackup(expFolder,currentSteps = new Date()){
    const weightsFolder = path.join(process.cwd(), `yolov5/runs/train/${expFolder}`,'weights');
    const bestPtPath = path.join(weightsFolder,'best.pt');
    const backupPtPath = path.join(weightsFolder,`backup-${currentSteps}.pt`);
    await fs.writeFile(backupPtPath,await fs.readFile(bestPtPath));
}

function resetStaleTrainingTimeout(){
    clearTimeout(trainProcessStaleTimeout);
    trainProcessStaleTimeout = setTimeout(() => {
        stopTraining();
    },120000)
}

async function startTraining({
    weights = 'weights/yolov5m.pt',
    imgSize,
    batchSize = 16,
    epochs = 1,
    device = '0',
    resume, // resume = expFolder
    onData = (data) => {console.log(data)},
    io
}) {
    if(weights.endsWith('6.pt')){
        imgSize = 1280
    }else{
        imgSize = 640
    }
    if(!device)device = '0';
    const response = {ok: false}
    const backupInterval = Math.round(epochs / 10) || 1; // Ensure minimum interval of 1
    if(trainProcess){
        response.msg = 'Already Training'
        return response;
    }
    const trainInfo = await getActiveDatasetInfo()
    const classes = trainInfo.classes.filter(item => !!item).map(item => item.trim());
    await clearImagesWithNoAnnotations();
    const datasetYAML = await createDatasetYAML();
    const gpuInfo = (await getNvidiaSmiInfo()).gpus
    const selectedGpus = device.split(',')
    const selectedGpuInfo = []
    selectedGpus.forEach((gpuId) => {
        const selectedGpu = gpuInfo[gpuId];
        selectedGpu.gpuId = gpuId;
        selectedGpuInfo.push(selectedGpu)
    });
    let resumePath = `${weights}`
    if (resume) {
        const checkpointPath = path.join(process.cwd(), `yolov5/runs/train/${resume}/weights/last.pt`);
        if (await fs.exists(checkpointPath)){
            resumePath = `${checkpointPath}`
        }
    }

    return new Promise((resolve, reject) => {
        const trainArgs = [
            'yolov5/train.py',
            '--img', imgSize.toString(),
            '--batch', batchSize.toString(),
            '--epochs', epochs.toString(),
            '--data', datasetYAML,
            '--weights', resume ? resumePath : weights,
            '--device', device,
        ];
        trainProcess = spawn(venvPythonPath, trainArgs);
        onData({msg: 'start', command: `${venvPythonPath} ${trainArgs.join(' ')}`, time: new Date()})
        trainProcess.stdout.on('data', (data) => {
            let lines = data.toString().split(/\r?\n/);
            lines.forEach((line) => {
                onData({msg: 'stdout', data: line.trim()})
            })
            resetStaleTrainingTimeout();
        });

        trainProcess.stderr.on('data', (raw) => {
            let lines = raw.toString().split(/\r?\n/);
            lines.forEach((line) => {
                try{
                    const data = parseTrainString(line.trim());
                    const isProgress = data.type === 'progress'
                    const isSummary = data.type === 'summary'
                    if(isProgress){
                        currentEpoch = data.epoch;
                    }
                    if(isSummary){
                        if(data.currentStep === data.totalSteps){
                            setTimeout(async () => {
                                if (currentEpoch % backupInterval === 0 || currentEpoch === epochs) {
                                    const expFolder = await findNewestExpFolder('train');
                                    createWeightsBackup(expFolder, currentEpoch);
                                }
                            },10000);
                        }
                    }
                    onData({msg: 'stderr', data})
                }catch(err){
                    console.log(line)
                }
            });
            resetStaleTrainingTimeout();
        });

        trainProcess.on('close', async (code) => {
            if (code === 0) {
                const expFolder = await findNewestExpFolder('train');
                console.log('Testing Recent Weights Generated', expFolder, new Date())
                await testLatestWeights(io ? (data) => {
                    io.emit('trainTest',data)
                } : (data) => { console.log(data) })
                resolve(response);
            } else {
                reject(`Training process exited with code ${code}`);
            }
            onData({msg: 'end'})
            trainProcess = null;
            clearTimeout(saveInfo);
            clearTimeout(trainProcessStaleTimeout);
        });
        const infoToSave = {
            classes,
            weights,
            imgSize,
            batchSize,
            epochs,
            device,
            trainArgs: trainArgs.join(' '),
            gpus: selectedGpuInfo,
        };
        let saveInfo = setTimeout(async () => {
            const expFolder = await findNewestExpFolder('train');
            infoToSave.builtWeights = `yolov5/runs/train/${expFolder}/weights/best.pt`
            copyTrainingInfoToExp({
                expFolder,
                infoToSave,
            });
        },10000);
    });
}

function stopTraining(){
    if(!trainProcess)return true;
    return new Promise((resolve, reject) => {
        try{
            trainProcess.once('close', (code) => {
                resolve(true)
            })
            trainProcess.kill();
        }catch(err){
            reject(err)
        }
    });
}

async function convertWeightsToONNX(options = {}) {
    let { expFolder, outputPath, weightsPath } = options;
    try {
        const expDirs = await fs.readdir(trainDir);

        let selectedExpDir;
        if (typeof expFolder === 'string' && expFolder !== '') {
            selectedExpDir = expDirs.find(dir => dir === expFolder);
        } else if (expFolder === undefined || expFolder === null) {
            const sortedDirs = expDirs
                .filter(dir => dir.startsWith('exp'))
                .sort((a, b) => statSync(path.join(trainDir, b)).mtime - statSync(path.join(trainDir, a)).mtime);
            selectedExpDir = sortedDirs[0];
        }

        if (!selectedExpDir) {
            throw new Error('No appropriate experiment directory found.');
        }

        weightsPath = weightsPath || path.join(trainDir, selectedExpDir, 'weights/best.pt');
        const finalOutputPath = outputPath || path.join(trainDir, selectedExpDir, 'weights/best.onnx');
        return new Promise((resolve, reject) => {
            const exporter = spawn(venvPythonPath, [
                'yolov5/export.py',
                '--weights', weightsPath,
                '--img', '640',
                '--batch', '1',
                '--include', 'onnx'
            ]);

            exporter.stdout.on('data', (data) => {
                console.log(`stdout: ${data}`);
            });

            exporter.stderr.on('data', (data) => {
                console.error(`stderr: ${data}`);
            });

            exporter.on('close', (code) => {
                if (code === 0) {
                    console.log(`ONNX conversion completed successfully.`);
                    resolve(finalOutputPath);
                } else {
                    reject(`ONNX conversion process exited with code ${code}`);
                }
            });
        });
    } catch (error) {
        console.error(`Error converting weights to ONNX: ${error.message}`);
        throw error;
    }
}

function isImageByName(filename){
    const lower = filename.toLowerCase();
    return lower.endsWith('.jpg') || lower.endsWith('.jpeg') || lower.endsWith('.png')
}

function fixBrokenVideo({
    expPath,
    fileName,
    onData = () => {},
}){
  return new Promise((resolve, reject) => {
      const videoPath = path.join(expPath, fileName)
      const outputPath = path.join(expPath, `temp_${fileName}`)
    const ffmpegArgs = [
      '-hwaccel', 'cuda',
      '-hwaccel_output_format', 'cuda',
      '-i', videoPath,
      '-c:v', 'h264_nvenc',
      '-preset', 'fast',
      '-crf', '1',
      '-c:a', 'copy',
      outputPath
    ];

    const ffmpegProcess = spawn('ffmpeg', ffmpegArgs);

    ffmpegProcess.stderr.on('data', (data) => {
      onData({msg: 'data', data: data.toString()})
    });

    ffmpegProcess.on('close', async (code) => {
        let isOk = code === 0;
        try{
            await fs.rm(videoPath)
            await fs.rename(outputPath, videoPath)
        }catch(err){
            onData({msg: 'fs.rm,fs.rename:error', err: err})
        }
        onData({msg: 'close', ok: isOk, code: code})
        resolve({ok: isOk, code: code});
    });
  });
}

function testWeights(options){
    const {
        weightsPath,
        onData = () => {}
    } = options;
    return new Promise((resolve, reject) => {
        if(!weightsPath)return reject('No Weights Selected');
        let stdoutString = ``
        let stderrString = ``
        const detector = spawn(venvPythonPath, [
            'yolov5/detect.py',
            '--weights', weightsPath,
        ]);
        onData({msg: 'start'})
        detector.stdout.on('data', (data) => {
            stdoutString += data;
            onData({msg: 'stdout', data: data.toString()})
        });

        detector.stderr.on('data', (data) => {
            stderrString += data;
            onData({msg: 'stderr', data: data.toString()})
        });

        detector.on('close', async (code) => {
            const stringArray = stderrString.split('\n');
            const expFolder = await findNewestExpFolder('detect')
            const expPath = path.join(process.cwd(), `yolov5/runs/detect/${expFolder}`)
            const testedOnPath = path.join(expPath, `testedOn.json`);
            const trainExp = extractFinalFolderName(weightsPath);
            await fs.writeFile(testedOnPath,JSON.stringify({
                exp: trainExp,
                weightsPath,
            }));
            const detections = await listTestResultFiles(expFolder);
            for (let i = 0; i < detections.videos.length; i++) {
                const fileName = detections.videos[i]
                await fixBrokenVideo({
                    expPath,
                    fileName,
                    onData: (data) => {
                        data.msg = `fixTestedVideo:${data.msg}`
                        onData(data)
                    }
                })
            }
            const result = {
                ok: code === 0,
                exp: expFolder,
                trainExp,
                path: weightsPath,
                stdout: stdoutString,
                stderr: stringArray,
                code: code,
                images: detections.files.filter(item => isImageByName(item)) || [],
            }
            onData({msg: 'end', data: result});
            copyInfoFromTrainToTest(trainExp, expFolder);
            resolve(result);
        });
    });
}

async function testLatestWeights(onData) {
    try {
        const expDirs = await fs.readdir(trainDir);

        // Find the latest weights file
        const sortedDirs = expDirs
            .filter(dir => dir.startsWith('exp'))
            .sort((a, b) => statSync(path.join(trainDir, b)).mtime - statSync(path.join(trainDir, a)).mtime);

        if (sortedDirs.length === 0) {
            throw new Error('No experiment directories found.');
        }

        const latestExpDir = sortedDirs[0];
        const weightsPath = path.join(trainDir, latestExpDir, 'weights/best.pt');
        return await testWeights({
            weightsPath,
            onData
        });
    } catch (error) {
        console.error(`Error running Test : ${error}`);
        throw error;
    }
}

async function createPackageFromTrainExp(expFolder, selectedWeights = 'best.pt'){
    const response = {ok: true}
    const weightsPath = path.join(trainDir, expFolder, 'weights', selectedWeights);
    const infoPath = path.join(trainDir, expFolder, 'info.json');
    const info = JSON.parse(await fs.readFile(infoPath,'utf8'));
    const classes = info.data.classes;
    const zipMetaPath = path.join(savedPackageDir, `${expFolder}.json`);
    const zipSaveFolder = path.join(savedPackageDir, `${expFolder}`)
    const zipSavePath = `${zipSaveFolder}.zip`
    try{
        zipMetaPath.datePackaged = new Date();
        zipMetaPath.expBase = expFolder;
        zipMetaPath.selectedWeights = selectedWeights;
        const infoTxt = JSON.stringify(info, null, 3);
        await fs.mkdir(zipSaveFolder, { recursive: true });
        await fs.writeFile(`${zipSaveFolder}/classes.txt`, classes.join('\n'));
        await fs.writeFile(`${zipSaveFolder}/info.json`, infoTxt);
        await fs.writeFile(zipMetaPath, infoTxt);
        await fs.copyFile(weightsPath,`${zipSaveFolder}/yolov5s.pt`);
        await createZipArchive(zipSaveFolder,zipSavePath);
        await fs.rm(zipSaveFolder, { recursive: true });
    }catch(err){
        console.log(err)
        response.ok = false
        response.err = err.toString()
    }
    return response
}

async function listDatasets() {
    try {
        const targetDir = path.join(process.cwd(), `openImagesData`, 'images');
        const items = await fs.readdir(targetDir, { withFileTypes: true });

        const foldersPromises = items
            .filter(item => item.isDirectory())
            .map(async dir => {
                const stats = await fs.stat(path.join(targetDir, dir.name));
                return {
                    name: dir.name,
                    ctime: stats.ctime
                };
            });

        const theFolders = await Promise.all(foldersPromises);
        return theFolders;
    } catch (error) {
        console.error(`Error listDatasets`, error);
        return [];
    }
}

async function listDatasetFiles(folderTarget) {
    try {
        const targetDir = path.join(process.cwd(), `openImagesData`, 'images', folderTarget);
        const items = await fs.readdir(targetDir, { withFileTypes: true });
        return items;
    } catch (error) {
        console.error(`Error listDatasetFiles`, new Error(error));
        return [];
    }
}

function labelFileToJson(text, imageWidth, imageHeight) {
    const lines = text.trim().split('\n');
    const newArray = [];
    lines.forEach(line => {
        const parts = line.split(' ').map(part => parseFloat(part));
        if (parts.length !== 5) {
            return;
        }

        // Convert normalized coordinates to pixel coordinates
        const xCenter = parts[1] * imageWidth;
        const yCenter = parts[2] * imageHeight;
        const width = parts[3] * imageWidth;
        const height = parts[4] * imageHeight;

        // Convert from center coordinates to top-left coordinates
        const x = xCenter - width / 2;
        const y = yCenter - height / 2;

         newArray.push({
            classNumber: parts[0],
            x: Math.round(x),
            y: Math.round(y),
            width: Math.round(width),
            height: Math.round(height)
        });
    });
    return newArray;
}

async function getImageMatrices(targetFolder,imageId){
    const txtPath = path.join(process.cwd(), 'openImagesData', 'labels', targetFolder, `${imageId}.txt`);
    const jpegPath = path.join(process.cwd(), 'openImagesData', 'images', targetFolder, `${imageId}.jpg`);
    const { width, height } = imageSize(jpegPath);
    const matrices = labelFileToJson(await fs.readFile(txtPath,'utf8'), width, height)
    return { width, height, matrices };
}
function getTrainedWeightsPath(expFolder,weightsName = 'best.pt'){
    return path.join(trainDir,expFolder,'weights',weightsName)
}
async function listSavedPackages(){
    const packages = (await fs.readdir(savedPackageDir)).filter(item => item.endsWith('.zip'))
    const itemPromises = packages.map(async item => {
        const shortName = item.split('.')[0]
        const stats = await fs.stat(path.join(savedPackageDir, item));
        const infoPath = `${savedPackageDir}/${shortName}.json`;
        return {
            name: shortName,
            ctime: stats.ctime,
            classes: JSON.parse(await fs.readFile(infoPath,'utf8')).data.classes
        };
    });
    const list = await Promise.all(itemPromises);
    return list;
}
async function getSavedPackageInfo(expFolder){
    const infoPath = `${savedPackageDir}/${expFolder}.json`;
    return JSON.parse(await fs.readFile(infoPath,'utf8'));
}

async function clearImagesWithNoAnnotations(fromDownloadedFolder) {
    const dataFolder = fromDownloadedFolder ? `openImagesData` : 'datasets'
    const datasetPath = path.join(process.cwd(), dataFolder);
    const imagesRoot = path.join(datasetPath, 'images');
    const labelsFolder = path.join(datasetPath, 'labels');
    const folderList = await fs.readdir(imagesRoot, { withFileTypes: true })
    for (const className of folderList) {
        const imagesPath = path.join(imagesRoot, className.name);
        const labelsPath = path.join(labelsFolder, className.name);
        if (className.isDirectory()) {
            const imageList = await fs.readdir(imagesPath)
            for (const fileName of imageList) {
                if(fileName.endsWith('.jpg') || fileName.endsWith('.jpeg')){
                    const filenamePrefix = `${fileName.replace(/\.\w+$/, '')}`
                    const imagePath = path.join(imagesPath, fileName);
                    const labelPath = path.join(labelsPath, `${filenamePrefix}.txt`);
                    async function deleteImage(imagePath){
                        console.log(`No Annotations : ${className.name} : ${imagePath}`)
                        await fs.rm(imagePath)
                    }
                    try{
                        const labelStats = await fs.stat(labelPath)
                        const labelFileSize = labelStats.size;
                        if(labelFileSize === 0){
                            await deleteImage(imagePath)
                            await fs.rm(labelPath)
                        }
                    }catch(err){
                        await deleteImage(imagePath)
                    }
                }
            }
        }
    }
}

module.exports = {
    startTraining,
    stopTraining,
    convertWeightsToONNX,
    testLatestWeights,
    testWeights,
    listExperimentFolders,
    listDatasets,
    listDatasetFiles,
    getImageMatrices,
    listTrainResultFiles,
    listTestResultFiles,
    findNewestExpFolder,
    createPackageFromTrainExp,
    getTrainedWeightsPath,
    savedPackageDir,
    listSavedPackages,
    getSavedPackageInfo,
    clearImagesWithNoAnnotations,
    labelFileToJson,
};
