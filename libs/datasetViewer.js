const fs = require('fs/promises');
const path = require('path');
const imageSize = require('image-size');
const {
    labelFileToJson,
} = require('./trainingManager.js');
const activeDatasetDir = path.join(process.cwd(), 'datasets');
async function listDatasets() {
    try {
        const targetDir = path.join(activeDatasetDir, 'images');
        const items = await fs.readdir(targetDir, { withFileTypes: true });

        const foldersPromises = items
            .filter(item => item.isDirectory())
            .map(async dir => {
                const stats = await fs.stat(path.join(targetDir, dir.name));
                return {
                    name: dir.name,
                    ctime: stats.ctime
                };
            });

        const theFolders = await Promise.all(foldersPromises);
        return theFolders;
    } catch (error) {
        console.error(`Error listDatasets`, error);
        return [];
    }
}

async function listDatasetFiles(folderTarget) {
    try {
        const targetDir = path.join(activeDatasetDir, 'images', folderTarget);
        const items = await fs.readdir(targetDir, { withFileTypes: true });
        return items;
    } catch (error) {
        console.error(`Error listDatasetFiles`, new Error(error));
        return [];
    }
}

async function getImageMatrices(targetFolder,imageId){
    const txtPath = path.join(activeDatasetDir, 'labels', targetFolder, `${imageId}.txt`);
    const jpegPath = path.join(activeDatasetDir, 'images', targetFolder, `${imageId}.jpg`);
    const { width, height } = imageSize(jpegPath);
    const matrices = labelFileToJson(await fs.readFile(txtPath,'utf8'), width, height)
    return { width, height, matrices };
}

module.exports = {
    listDatasets,
    listDatasetFiles,
    getImageMatrices,
}
