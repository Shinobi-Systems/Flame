const fs = require('fs').promises;
const fetch = require('node-fetch');
const spawn = require('child_process').spawn;
const FormData = require('form-data');
module.exports = async (config) => {
    const serverPath = config.dlibPath || `${process.cwd()}/libs/dlib_flask_server.py`;
    const serverPort = config.dlibPort || 8991;
    const flaskUrl = config.flaskUrl || `http://localhost:${serverPort}`;
    let modelProcess = null;
    const clearTrackerTimeouts = {}
    const trackerTimeoutPeriod = 1000 * 60 * 60;

    function getTrackerIdFromMatrix(cameraId,matrix){
        return `${matrix.tag}_${matrix.id}`
    }

    function setTrackerTimeout(cameraId,matrix){
        const trackerId = getTrackerIdFromMatrix(cameraId,matrix);
        const timeoutId = `${cameraId}${trackerId}`;
        clearTimeout(clearTrackerTimeouts[timeoutId])
        clearTrackerTimeouts[timeoutId] = setTimeout(() => {
            clearTracker(cameraId, trackerId)
        },trackerTimeoutPeriod)
    }
    function setTrackerTimeoutsForDetections(cameraId, detections){
        for (let i = 0; i < detections.length; i++) {
            const matrix = detections[i];
            setTrackerTimeout(cameraId, matrix)
        }
    }

    async function detect(imageBuffer, cameraId, matrices) {
        try {
            const formData = new FormData();
            formData.append('cameraId', cameraId);
            formData.append('matrices', matrices);  // Ensure this is a string
            formData.append('file', imageBuffer, { filename: `${new Date().getTime()}.jpg` });
            const response = await fetch(flaskUrl + '/detect', {
                method: 'POST',
                body: formData,
            });

            if (!response.ok) {
                throw new Error(`Server responded with ${response.status}: ${response.statusText}`);
            }
            const json = await response.json();
            return json;
        } catch (error) {
            console.error('Error uploading image:', error);
            return [];
        }
    }

    function filterByScore(cameraId, detections){
        const good = [];
        const bad = [];
        detections.forEach(matrix => {
            // console.log(`matrix.confidence ${matrix.tag}`,matrix.confidence)
            if(matrix.confidence >= 5){
                good.push(matrix)
            }else{
                // console.log(`bad matrix.confidence ${matrix.tag}`,matrix.confidence)
                // bad.push(matrix)
            }
        });
        return good
    }

    async function track(imageBuffer, cameraId, matrices){
        setTrackerTimeoutsForDetections(cameraId, matrices)
        let detections = await detect(imageBuffer, cameraId, matrices);
        detections = filterByScore(cameraId, detections);
        return detections
    }

    async function clearTracker(cameraId, trackerId) {
        try {
            const data = {
                cameraId: cameraId,
                trackerId: trackerId,
            };
            const jsonData = JSON.stringify(data);
            const response = await fetch(flaskUrl + '/clear_tracker', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: jsonData,
            });
            const json = await response.json();
            if (!json.ok) {
                console.log(json.message);
            }
            return json;
        } catch (error) {
            console.error('Error in clearTracker:', error);
            return [];
        }
    }

    function loadModel(options = {}) {
        return new Promise((resolve) => {
            console.log('Loading Model...')
            options.onData = options.onData || function(){};
            modelProcess = spawn('python3', [serverPath, '--port', serverPort]);
            let onReady = (data) => {
                const lines = data.toString();
                if(lines.indexOf('Serving Flask app') > -1){
                    console.log('Loaded Model!')
                    modelProcess.stdout.off('data', onReady)
                    resolve();
                }else if(lines.indexOf('Traceback') > -1){
                    console.error('Failed to Load Model')
                    modelProcess.stdout.off('data', onReady)
                    resolve();
                }
            }
            modelProcess.stdout.on('data', onReady);
            modelProcess.stdout.on('data', (data) => {
                const lines = data.toString();
                options.onData(lines);
            });

            modelProcess.on('close', (code) => {
                // never let it die
                console.log('Model Died!')
                setTimeout(() => {
                    loadModel(options);
                }, 5000);
            });

            modelProcess.stderr.on('data', (data) => {
                console.error(`Detector Server : ${data}`);
            });
        })
    }

    async function clearTrackersByMatrices(cameraId, matrices){
        for (let i = 0; i < matrices.length; i++) {
            const matrix = matrices[i];
            const trackerId = getTrackerIdFromMatrix(cameraId,matrix)
            await clearTracker(cameraId, trackerId)
        }
    }
    await loadModel();
    return {
        track,
        loadModel,
        clearTracker,
        getTrackerIdFromMatrix,
        clearTrackersByMatrices,
    };
}
