const path = require("path");
const fs = require("fs").promises;
const spawn = require('child_process').spawn;
const AdmZip = require("adm-zip");
function grepFile(pattern, filePath, handleLine) {
    return new Promise((resolve, reject) => {
        const grep = spawn('grep', ['-E', pattern, filePath]);
        grep.stdout.on('data', (data) => {
            const lines = data.toString().split('\n');
            lines.forEach((line) => {
                if (line) handleLine(line);
            });
        });

        grep.on('close', (code) => {
            if (code !== 0) {
                reject(`grep process exited with code ${code}`);
            } else {
                resolve({ok: true, msg: 'grepped'});
            }
        });

        grep.stderr.on('data', (data) => {
            console.error(`grep stderr: ${data}`);
        });
    });
}

function makeSafeForFolder(name) {
    return name
        .replace(/[<>:"/\\|?*]+/g, '')
        .replace(/^\s+|\s+$/g, '')
        .replace(/[.]+$/g, '')
        .replace(/\s+/g, '_');
}

function removeSpecialCharacters(string) {
    return string.replace(/[\s~`!@#$%^&*(){}\[\];:"'<,.>?\/\\|_+=-]/g, '')
}

function findCommonElements(arr1, arr2) {
  const set2 = new Set(arr2);
  return arr1.filter(item => set2.has(item));
}

async function createZipArchive(folderPath, outputFile) {
    const zip = new AdmZip();
    zip.addLocalFolder(folderPath);
    zip.writeZip(outputFile);
    delete(zip)
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
async function downloadAndUnzip(url, destinationFolder) {
    const filename = path.basename(url);
    const folderName = filename.replace(path.extname(filename), '').split('?')[0];
    const folderPath = path.join(destinationFolder, folderName);
    const response = {ok: true, folderName}
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to download: ${response.statusText}`);
        }
        const buffer = await response.arrayBuffer();
        const zipFilePath = path.join(destinationFolder, filename);
        await fs.mkdir(destinationFolder, { recursive: true });
        await fs.writeFile(zipFilePath, Buffer.from(buffer));
        const zip = new AdmZip(zipFilePath);
        zip.extractAllTo(folderPath,true);
        await fs.rm(zipFilePath)
    } catch (error) {
        console.log(error)
        response.ok = false;
        response.err = error;
    }
    return response
}

async function readAndSortByModifiedDate(directoryPath) {
    try {
        const files = await fs.readdir(directoryPath);
        const filesWithStats = await Promise.all(
            files.map(async (file) => {
                const filePath = path.join(directoryPath, file);
                const stats = await fs.stat(filePath);
                return { file, mtime: stats.mtime };
            })
        );

        filesWithStats.sort((a, b) => b.mtime - a.mtime);

        const sortedFiles = filesWithStats.map(fileWithStat => fileWithStat.file);
        return sortedFiles;
    } catch (err) {
        console.error('Error reading or sorting files:', err);
    }
}

function spawnPromise(appName, args, options = {
    stdout: (data) => {
        const lines = data.toString().split('\n');
        lines.forEach((line) => {
            if (line) handleLine(line);
        });
    },
    stderr: (data) => {
        console.error(`grep stderr: ${data}`);
    },
    onClose: (code) => {}
}){
    return new Promise((resolve, reject) => {
        const theProcess = spawn(appName, args);

        theProcess.stdout.on('data', options.stdout);

        theProcess.on('close', (code) => {
            options.stderr(code)
            resolve({ ok: code === 0, code })
        });

        theProcess.stderr.on('data', options.stderr);
    });
}

function shutdownSystem(){
    return spawnPromise('shutdown', ['now'])
}

module.exports = {
    grepFile,
    makeSafeForFolder,
    findCommonElements,
    createZipArchive,
    capitalizeFirstLetter,
    downloadAndUnzip,
    readAndSortByModifiedDate,
    spawnPromise,
    shutdownSystem,
}
