const fs = require('fs').promises;
const path = require('path');
const activeDatasetPath = path.join(process.cwd(), `datasets`);
const customDatasetsPath = path.join(process.cwd(), `custom_data`);
const customDataPath = path.join(customDatasetsPath, 'flame-video-annotator');
const {
    getActiveDatasetInfo,
    getClassIdForActiveDataset,
    addClassNameToActiveDataset,
    setActiveDatasetInfo,
} = require('../trainingManager/activeDatasets.js');
const {
    removeSpecialCharacters,
} = require('../utils.js');

async function findExistingImage(imageId, classes) {
    try {
        const classes = await fs.readdir(imagesDir);
        for (const folder of classes) {
            const possiblePath = path.join(imagesDir, folder, `${imageId}.jpg`);
            if (await this.fileExists(possiblePath)) {
                return possiblePath;
            }
        }
    } catch (error) {
        console.error(`Error searching for existing image: ${error}`);
    }
    return null;
}
async function saveVideoAnnotationFrame({
    videoName,
    filename,
    imageBuffer,
    matrices,
    height,
    width,
}){
    const response = { ok: true }
    try{
        const datasetPath = path.join(customDataPath, videoName);
        const imagesSavePath = path.join(datasetPath, 'train', 'images');
        const labelsSavePath = path.join(datasetPath, 'train', 'labels');
        await fs.mkdir(imagesSavePath, { recursive: true });
        await fs.mkdir(labelsSavePath, { recursive: true });
        
    }catch(err){
        console.error('saveVideoAnnotationFrame ERROR',err);
        response.ok = false;
        response.err = err.toString();
    }
    return response;
}
async function convertImageInfoToAnnotation(matrix, imageWidth, imageHeight) {
    const className = matrix.tag;
    const classNumber = await getClassIdForActiveDataset(className);
    const { tag, x, y, width, height } = matrix;
    const xCenter = x + width / 2;
    const yCenter = y + height / 2;
    const normalizedXCenter = xCenter / imageWidth;
    const normalizedYCenter = yCenter / imageHeight;
    const normalizedWidth = width / imageWidth;
    const normalizedHeight = height / imageHeight;
    return `${classNumber} ${normalizedXCenter.toFixed(6)} ${normalizedYCenter.toFixed(6)} ${normalizedWidth.toFixed(6)} ${normalizedHeight.toFixed(6)}`;
}
async function linkToActiveDataset(classes = []){
    const response = {ok: true}
    try {
        for(const className of classes){
            const imagesDirForClass = path.join(imagesDir, className);
            const labelsDirForClass = path.join(labelsDir, className);
            const images = await fs.readdir(imagesDirForClass);
            for(const imageFilename of images){
                const frameId = imageFilename.replace(/\.\w+$/, '');
                const imagePath = path.join(imagesDirForClass, imageFilename);
                const infoPath = path.join(labelsDirForClass, `${frameId}.json`);
                const targetImageDir = path.join(activeDatasetPath, 'images', className)
                const targetImagePath = path.join(targetImageDir, imageFilename);
                const targetLabelDir = path.join(activeDatasetPath, 'labels', className)
                const targetLabelPath = path.join(targetLabelDir, `${frameId}.txt`);
                await fs.mkdir(targetImageDir,{ recursive: true });
                await fs.mkdir(targetLabelDir,{ recursive: true });
                try{
                    const { matrices, width, height } = JSON.parse(await fs.readFile(infoPath,'utf8'));
                    const convertLabels = [];
                    for(matrix of matrices){
                        convertedLabels.push(convertImageInfoToAnnotation(matrix, width, height));
                    }
                    await fs.link(imagePath, targetImagePath);
                    await fs.writeFile(targetLabelPath, convertedLabels.join('\n'));
                }catch(err){
                    console.error('linkToActiveDataset file ERROR :', err);
                    response.ok = false;
                    response.err = err;
                }
            }
        }
    } catch (err) {
        console.error('An error occurred:', err);
        response.ok = false;
        response.err = err;
    }
    return response
}

module.exports = {
    annotatorDir,
    imagesDir,
    labelsDir,
    findExistingImage,
    saveVideoAnnotationFrame,
    linkToActiveDataset,
}
