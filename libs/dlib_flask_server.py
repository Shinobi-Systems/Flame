from flask import Flask, request, jsonify
import dlib
import argparse
from PIL import Image
import io
import json
import numpy as np
import logging

app = Flask(__name__)

logging.getLogger('werkzeug').setLevel(logging.WARNING)

trackers = {}

@app.route('/detect', methods=['POST'])
def detect():
    camera_id = request.form.get('cameraId')
    matrices_string = request.form.get('matrices')
    matrices = json.loads(matrices_string)

    image_file = request.files['file']
    image_bytes = image_file.read()
    img = Image.open(io.BytesIO(image_bytes))
    img = img.convert('RGB')
    img_array = np.array(img)

    if camera_id not in trackers:
        trackers[camera_id] = {}

    for matrix in matrices:
        tracker_id = f"{matrix['tag']}_{matrix['id']}"
        tracker = dlib.correlation_tracker()
        rect = dlib.rectangle(matrix['x'], matrix['y'], matrix['x'] + matrix['width'], matrix['y'] + matrix['height'])
        tracker.start_track(img_array, rect)
        if tracker_id in trackers[camera_id]:
            del trackers[camera_id][tracker_id]
        trackers[camera_id][tracker_id] = tracker

    # Create a response object containing the tracker positions
    response = []
    for tracker_id, tracker in trackers[camera_id].copy().items():
        tracking_quality = trackers[camera_id][tracker_id].update(img_array)
        tag, obj_id = tracker_id.split('_', 1)  # Split only once for performance
        pos = tracker.get_position()
        response.append({
            'tag': tag,
            'id': obj_id,
            'x': int(pos.left()),
            'y': int(pos.top()),
            'width': int(pos.width()),
            'height': int(pos.height()),
            'confidence': tracking_quality
        })

    return jsonify(response)

@app.route('/clear_tracker', methods=['POST'])
def clear_tracker():
    data = request.json
    camera_id = data.get('cameraId')
    tracker_id = data.get('trackerId')

    if camera_id in trackers and tracker_id in trackers[camera_id]:
        del trackers[camera_id][tracker_id]
        return jsonify({"ok": True, "message": f"Tracker '{tracker_id}' for camera '{camera_id}' cleared."})
    else:
        return jsonify({"ok": False, "message": "Tracker already gone"})


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Run the Flask server with optional port setting.")
    parser.add_argument("--port", type=int, default=8991, help="Port on which to run the Flask server")
    args = parser.parse_args()

    app.run(port=args.port)
