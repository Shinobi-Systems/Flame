const { exec } = require('child_process');
const xml2js = require('xml2js');

function getNvidiaSmiInfo() {
    return new Promise((resolve, reject) => {
        exec('nvidia-smi -q -x', (error, stdout, stderr) => {
            if (error) {
                reject(`exec error: ${error}`);
                return;
            }
            xml2js.parseString(stdout, (err, result) => {
                if (err) {
                    reject(`XML parsing error: ${err}`);
                    return;
                }
                removeSupportedGraphicsClock(result);
                result = filterNvidiaSmiInfo(result);
                resolve(result);
            });
        });
    });
}

function filterGpuData(data) {
    return data.map(gpu => {
        try{
            return {
                id: gpu["$"].id,
                name: gpu.product_name[0],
                persistence_mode: gpu.persistence_mode[0],
                bus_id: gpu.pci[0].pci_bus_id[0],
                display_active: gpu.display_active[0],
                fan_speed: gpu.fan_speed[0],
                temp: gpu.temperature[0].gpu_temp[0],
                performance_state: gpu.performance_state[0],
                power_usage: gpu.gpu_power_readings[0]?.power_draw[0], // Optional chaining
                power_cap: gpu.gpu_power_readings[0].current_power_limit[0],
                memory_usage: gpu.fb_memory_usage[0],
                gpu_util: gpu.utilization[0].gpu_util[0],
                compute_mode: gpu.compute_mode[0],
                processes: !gpu.processes[0] || gpu.processes[0] == '\n\t\t' ? [] : gpu.processes[0].process_info.map(p => ({
                    pid: p.pid[0],
                    type: p.type[0],
                    name: p.process_name[0],
                    used_memory: p.used_memory[0]
                }))
            };
        }catch(err){
            console.log(gpu)
            return {
                id: gpu["$"].id,
                name: gpu.product_name[0],
                persistence_mode: gpu.persistence_mode[0],
                bus_id: gpu.pci[0].pci_bus_id[0],
                display_active: gpu.display_active[0],
                fan_speed: gpu.fan_speed[0],
                temp: gpu.temperature[0].gpu_temp[0],
                performance_state: gpu.performance_state[0],
                power_cap: gpu.gpu_power_readings[0].current_power_limit[0],
                memory_usage: gpu.fb_memory_usage[0],
                gpu_util: gpu.utilization[0].gpu_util[0],
                compute_mode: gpu.compute_mode[0],
                processes: !gpu.processes[0] || gpu.processes[0] == '\n\t\t' ? [] : gpu.processes[0].process_info.map(p => ({
                    pid: p.pid[0],
                    type: p.type[0],
                    name: p.process_name[0],
                    used_memory: p.used_memory[0]
                }))
            }
        }
    });
}

function filterNvidiaSmiInfo(jsonData) {
  const driverVersion = jsonData.nvidia_smi_log.driver_version;
  const gpus = filterGpuData(jsonData.nvidia_smi_log.gpu)
  return {
    driverVersion,
    gpus,
  };
}

function removeSupportedGraphicsClock(obj) {
    if (typeof obj !== 'object' || obj === null) {
        return;
    }

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (key === 'supported_graphics_clock') {
                delete obj[key];
            } else if (typeof obj[key] === 'object') {
                removeSupportedGraphicsClock(obj[key]);
            }
        }
    }
}

function parseFreeMOutput(output) {
    let lines = output.trim().split("\n");
    const extractNumbers = (line) => line.split(/\s+/).slice(1).map(num => parseInt(num, 10));

    let memLine = lines[1];
    let swapLine = lines[2];

    let [memTotal, memUsed, memFree, memShared, memBuffCache, memAvailable] = extractNumbers(memLine);
    let [swapTotal, swapUsed, swapFree] = extractNumbers(swapLine);

    return {
        memory: {
            total: memTotal,
            used: memUsed,
            free: memFree,
            shared: memShared,
            buffCache: memBuffCache,
            available: memAvailable
        },
        swap: {
            total: swapTotal,
            used: swapUsed,
            free: swapFree
        }
    };
}

function getFreeMData() {
    return new Promise((resolve, reject) => {
        exec('free -m', (error, stdout, stderr) => {
            if (error) {
                reject(`exec error: ${error}`);
                return;
            }
            resolve(parseFreeMOutput(stdout));
        });
    });
}

module.exports = {
    getFreeMData,
    getNvidiaSmiInfo,
}
