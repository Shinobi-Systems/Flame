const { spawn } = require('child_process');
const path = require('path');

class DetectServer {
    constructor(pythonPath = 'python3', serverScriptPath = `${__dirname}/../yolov5/yolov5_flask_server.py`) {
        this.pythonPath = pythonPath;
        this.serverScriptPath = path.resolve(serverScriptPath);
        this.serverProcess = null;
    }

    start(options = {}) {
        const {
            port = 8989,
            weights = 'yolov5s.pt',
            device = '0',
            imgsz = 640,
            confThres = 0.25,
            iouThres = 0.45,
            classes = [],
            agnosticNms = false,
            maxDet = 1000,
            classesFile = ''
        } = options;

        if (this.serverProcess) {
            console.log('DetectServer is already running.');
            return;
        }

        console.log(`Starting DetectServer on port ${port}...`);
        const args = [
            this.serverScriptPath,
            '--port', port.toString(),
            '--weights', weights,
            '--device', device,
            '--imgsz', imgsz.toString(),
            '--conf-thres', confThres.toString(),
            '--iou-thres', iouThres.toString(),
            '--max-det', maxDet.toString(),
            '--classes-file', classesFile
        ];

        // Add classes if provided
        if (classes.length > 0) {
            args.push('--classes');
            args.push(...classes.map(c => c.toString()));
        }

        // Add agnostic-nms flag if true
        if (agnosticNms) {
            args.push('--agnostic-nms');
        }

        this.serverProcess = spawn(this.pythonPath, args);

        this.serverProcess.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });

        this.serverProcess.stderr.on('data', (data) => {
            console.error(`stderr: ${data}`);
        });

        this.serverProcess.on('close', (code) => {
            console.log(`DetectServer process exited with code ${code}`);
            this.serverProcess = null;
        });
    }

    stop() {
        if (this.serverProcess) {
            console.log('Stopping DetectServer...');
            this.serverProcess.kill();
            this.serverProcess = null;
        } else {
            console.log('DetectServer is not running.');
        }
    }
}

module.exports = DetectServer;
