let lastImageDownloaderOnData = {msg: 'notDownloadedSinceStart'};
const ImageDownloader = require('../openImagesDownloader.js');
const openImagesDL = new ImageDownloader();
module.exports = (app,lang,config,io) => {
    app.post('/api/dlImage/start', async (req, res) => {
        const { className = 'Person', maxImages = 100, relationships = [] } = req.body;
        let response = {ok: true}
        try{
            if(className && maxImages){
                openImagesDL.run({
                    classes: className.split(',').map(item => item.trim()),
                    relationships: relationships.split(',').map(item => item.trim()),
                    maxImages,
                    onEvent: (data) => {
                        lastImageDownloaderOnData = data;
                        io.emit('openImageDownloader',data)
                    }
                });
            }else{
                response.ok = false;
                response.msg = 'Missing Class or Max Images Limit';
            }
        }catch(err){
            response.ok = false;
            response.err = err.toString();
            console.log(err)
        }
        res.json(response);
    });

    app.post('/api/dlImage/start', async (req, res) => {
        const { className = 'Person', maxImages = 100, relationships = [] } = req.body;
        let response = {ok: true}
        try{
            if(className && maxImages){
                openImagesDL.run({
                    classes: className.split(',').map(item => item.trim()),
                    relationships: relationships.split(',').map(item => item.trim()),
                    maxImages,
                    onEvent: (data) => {
                        lastImageDownloaderOnData = data;
                        console.log(data)
                        io.emit('openImageDownloader',data)
                    }
                });
            }else{
                response.ok = false;
                response.msg = 'Missing Class or Max Images Limit';
            }
        }catch(err){
            response.ok = false;
            response.err = err.stack;
        }
        res.json(response);
    });

    app.post('/api/dlImage/stop', async (req, res) => {
        let response = {ok: false}
        try{
            response = await openImagesDL.stop();
        }catch(err){
            response.err = err;
        }
        res.json(response);
    });

    app.get('/api/dlImage/eraseLabels', async (req, res) => {
        let response = {ok: false}
        try{
            response = await openImagesDL.eraseLabels();
        }catch(err){
            response.err = err;
        }
        res.json(response);
    });

    app.post('/api/dlImage/link', async (req, res) => {
        const classes = req.body.classes;
        const relationships = req.body.relationships;
        const response = await openImagesDL.linkDatasetsToActive(classes,relationships);
        res.json(response);
    });

    io.on('connection',function(cn){
        cn.emit('openImageDownloader',lastImageDownloaderOnData)
    });
}
