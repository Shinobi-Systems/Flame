const fs = require('fs').promises;
const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');
const {
    getCustomDatasetTypes,
    getCustomDatasets,
    parseYAML,
    appendUniqueText,
    customDataDir,
    getCustomDatasetDirectory,
    getImageMatrices,
} = require('../customData/utils.js');
const {
    downloadAndUnzip,
} = require('../utils.js');
const roboflowYolov5 = require('../customData/roboflow-yolov5.js');
// roboflowYolov5 = {
//     getInfo,
//     convertLabels,
//     getCustomDatasetImages,
//     linkToActiveDataset,
// }

module.exports = (app,lang,config,io) => {
    app.use('/customDatasets',express.static('custom_data'));
    app.get('/api/customDatasets/types', async (req, res) => {
        let response = {ok: true}
        response.types = await getCustomDatasetTypes();
        res.json(response);
    });
    app.get('/api/customDatasets/data/:type', async (req, res) => {
        let response = {ok: true}
        const datasetType = req.params.type || 'roboflow-yolov5';
        response.datasets = await getCustomDatasets(datasetType);
        res.json(response);
    });
    app.get('/api/customDatasets/data/:type/:name', async (req, res) => {
        let response = {ok: true}
        const datasetType = req.params.type || 'roboflow-yolov5';
        const datasetName = req.params.name;
        const wantFilesToo = req.query.files == '1';
        switch(datasetType){
            case'shinobi-uploaded':
            case'roboflow-yolov5':
            case'flame-video-annotator':
                response.info = await roboflowYolov5.getInfo(datasetName, datasetType);
                if(wantFilesToo)response.files = (await roboflowYolov5.getCustomDatasetImages(datasetName, true, datasetType, true)).all.images;
            break;
        }
        res.json(response);
    });
    app.post('/api/customDatasets/link', async (req, res) => {
        const response = {ok: true}
        const datasetType = req.body.type || 'roboflow-yolov5';
        const datasetName = req.body.name;
        let classes = req.body.classes;
        classes = classes ? classes instanceof Array ? classes : classes.split(',') : [];
        switch(datasetType){
            case'shinobi-uploaded':
            case'roboflow-yolov5':
            case'flame-video-annotator':
                response.response = await roboflowYolov5.linkToActiveDataset(datasetName, datasetType, classes);
            break;
        }
        res.json(response);
    });
    app.post('/api/customDatasets/download', async (req, res) => {
        const datasetType = req.body.type || 'roboflow-yolov5';
        const datasetUrl = req.body.url;
        if(!datasetUrl){
            res.json({ok: false, err: 'Missing URL'});
            return
        }
        const saveDirectory = path.join(customDataDir, datasetType);
        const response = await downloadAndUnzip(datasetUrl, saveDirectory);
        switch(datasetType){
            case'shinobi-uploaded':
            case'roboflow-yolov5':
            case'flame-video-annotator':
                await roboflowYolov5.addLineToReadme(response.folderName, datasetUrl, datasetType);
            break;
        }
        res.json(response);
    });
    app.post('/api/customDatasets/uploadFile', fileUpload(), async (req, res) => {
        try {
            if (!req.files || !req.files.file) {
                return res.status(400).send('No files were uploaded.');
            }
            let response = { ok: false, msg: 'No Upload Method Used' };
            const file = req.files.file;
            const {
                filename,
                datasetType = 'shinobi-uploaded',
                datasetName,
                matrices,
                width,
                height,
            } = req.body;
            switch(datasetType){
                case'shinobi-uploaded':
                case'roboflow-yolov5':
                case'flame-video-annotator':
                    response = await roboflowYolov5.saveImageFrameToCustomDataset(datasetName, datasetType, {
                        filename,
                        imageBuffer: file,
                        matrices: JSON.parse(matrices),
                        height,
                        width,
                    });
                break;
            }
            res.json(response);
        } catch (error) {
            console.error(error)
            res.status(500).end('Error uploading file: ' + error.message);
        }
    });
    app.get('/api/customDatasets/label/:type/:name', async (req, res) => {
        const filePath = req.query.path;
        const datasetType = req.params.type;
        const datasetName = req.params.name;
        let response = {ok: true, data: []}
        try{
            response.data = await getImageMatrices(datasetType, datasetName, filePath);
        }catch(err){
            console.error(err)
            response.ok = false;
            response.err = err.toString();
        }
        res.json(response);
    });
}
