const fs = require('fs').promises;
const path = require('path');
const express = require('express');
const {
    listSavedPackages,
    getSavedPackageInfo,
    createPackageFromTrainExp,
} = require('../trainingManager.js');
module.exports = (app,lang,config,io) => {
    app.use('/api/savedPackages/fs', express.static('savedPackages'));

    app.post('/api/savedPackages/create', async (req, res) => {
        const { expFolder, selectedWeights } = req.body;
        let response = {ok: false}
        try{
            response = await createPackageFromTrainExp(expFolder, selectedWeights);
        }catch(err){
            response.err = err;
        }
        res.json(response);
    });

    app.get('/api/savedPackages/info/:expFolder', async (req, res) => {
        const expFolder = req.params.expFolder;
        let response = {ok: true}
        try{
            response.info = await getSavedPackageInfo(expFolder);
        }catch(err){
            response.ok = false;
            response.err = err;
        }
        res.json(response);
    });

    app.get('/api/savedPackages/list', async (req, res) => {
        let response = {ok: true, files: []}
        try{
            response.files = await listSavedPackages();
        }catch(err){
            console.log(err)
            response.ok = false;
            response.err = err.toString();
        }
        res.json(response);
    });
}
