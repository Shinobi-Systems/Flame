const {
    listDatasets,
    listDatasetFiles,
    getImageMatrices,
} = require('../datasetViewer.js');
const {
    datasetDir,
    getActiveDatasetInfo,
    clearActiveDataset,
    checkAnnotationClassIds,
    mergeClassIntoAnother,
    renameClassInActiveDataset,
    deleteClassInActiveDataset,
    adjustClassIdsToMatchClassOrderForAll,
} = require('../trainingManager/activeDatasets.js');
const fs = require('fs').promises;
const path = require('path');
const express = require('express');
module.exports = (app,lang,config,io) => {
    app.use('/datasets',express.static('datasets'));

    app.get('/api/datasets/info', async (req, res) => {
        let response = {ok: true}
        response.info = await getActiveDatasetInfo();
        res.json(response);
    });

    app.get('/api/datasets', async (req, res) => {
        let response = {ok: true}
        response.datasets = await listDatasets();
        res.json(response);
    });

    app.get('/api/datasets/:targetFolder', async (req, res) => {
        const targetFolder = req.params.targetFolder;
        let response = {ok: true, target: targetFolder}
        response.files = await listDatasetFiles(targetFolder);
        res.json(response);
    });

    app.get('/api/datasets/:targetFolder/:imageId', async (req, res) => {
        const imageId = req.params.imageId;
        const targetFolder = req.params.targetFolder;
        let response = {ok: true, data: []}
        try{
            response.data = await getImageMatrices(targetFolder,imageId);
        }catch(err){
            console.error(err)
            response.ok = false;
            response.err = err.toString();
        }
        res.json(response);
    });

    app.post('/api/datasets', async (req, res) => {
        const clearAll = req.body.clearAll == 1;
        const deleteThis = req.body.delete == 1;
        const checkThis = req.body.check == 1;
        const mergeClasses = req.body.merge == 1;
        const rename = req.body.rename == 1;
        const fixAnnotations = req.body.fixAnnotations == 1;
        let response = { ok: true };
        if(fixAnnotations){
            await adjustClassIdsToMatchClassOrderForAll();
        }else if(rename){
            const currentName = req.body.currentName;
            const newName = req.body.newName;
            if(currentName && newName){
                await renameClassInActiveDataset(currentName, newName);
            }else{
                response.ok = false;
                response.err = 'Missing Classes'
            }
        }else if(mergeClasses){
            const classToGrab = req.body.classToGrab;
            const classToMergeTo = req.body.classToMergeTo;
            if(classToGrab && classToMergeTo){
                response = await mergeClassIntoAnother(classToGrab, classToMergeTo);
            }else{
                response.ok = false;
                response.err = 'Missing Classes'
            }
        }else if(checkThis){
            const className = req.body.class;
            const labelsDir = path.join(datasetDir, 'labels', className)
            response.broken = await checkAnnotationClassIds(labelsDir);
        }else if(deleteThis){
            const className = req.body.class;
            if(className){
                response = await deleteClassInActiveDataset(className);
            }else{
                response.ok = false;
                response.err = 'Missing Class'
            }
        }else if(clearAll){
            await clearActiveDataset();
        }
        res.json(response);
    });
}
