const express = require('express');
const {
    testDataDir,
    activeTestDataDir,
    getTestSets,
    getTestSet,
    setTestSetToActive,
    getCurrentTestSet,
} = require('../customData/testSwitcher.js');
module.exports = async (app,lang,config,io) => {
    app.use('/testSets/current', express.static(activeTestDataDir));
    app.use('/testSets', express.static(testDataDir));

    app.get('/api/testSets', async (req, res) => {
        let response = { ok: true, sets: [] }
        try{
            response.sets = await getTestSets();
        }catch(err){
            response.ok = false;
            response.err = err.toString();
            console.log(err)
        }
        res.json(response);
    });

    app.get('/api/testSets/:folderName', async (req, res) => {
        let response = { ok: true, files: [] }
        const { folderName } = req.params;
        let gettingCurrent = folderName === 'current';
        try{
            if(gettingCurrent){
                response.files = await getCurrentTestSet()
            }else if(folderName){
                response.files = await getTestSet(folderName);
            }
        }catch(err){
            response.ok = false;
            response.err = err.toString();
            console.log(err)
        }
        res.json(response);
    });

    app.post('/api/testSets/link', async (req, res) => {
        let response = { ok: true }
        const { folderName } = req.body;
        try{
            await setTestSetToActive(folderName)
        }catch(err){
            response.ok = false;
            response.err = err.toString();
            console.log(err)
        }
        res.json(response);
    });
}
