const fs = require('fs').promises;
const path = require('path');
const datasetDir = path.join(process.cwd(), 'datasets');
const datasetImageDir = path.join(datasetDir, 'images');
const datasetLabelDir = path.join(datasetDir, 'labels');
const { ensureDirExists } = require('../dirManager.js');
let loadedActiveDatasetInfo = null;
async function getActiveDatasetInfo(){
    if(!loadedActiveDatasetInfo){
        try{
            const infoPath = path.join(datasetDir, 'info.json');
            loadedActiveDatasetInfo = JSON.parse(await fs.readFile(infoPath,'utf8'))
        }catch(err){
            loadedActiveDatasetInfo = { classes: [] }
        }
    }
    return loadedActiveDatasetInfo
}

async function getNextClassIdFromActiveDataset(){
    const info = await getActiveDatasetInfo();
    return info.classes.length;
}

async function getClassIdForActiveDataset(className){
    const info = await getActiveDatasetInfo();
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    const index = classes.indexOf(lowerClassName);
    if(index === -1){
        await addClassNameToActiveDataset(lowerClassName)
        return classes.length - 1;
    }else{
        return index;
    }
}

async function addClassNameToActiveDataset(className){
    const info = await getActiveDatasetInfo();
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    if(classes.indexOf(lowerClassName) === -1){
        classes.push(lowerClassName);
        info.classes = classes;
        await setActiveDatasetInfo(info);
        return true;
    }
    return false;
}

async function removeClassNameFromActiveDataset(className){
    const info = await getActiveDatasetInfo();
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    const classIndex = classes.indexOf(lowerClassName);
    if(classIndex > -1){
        classes.splice(classIndex, 1);
        info.classes = classes;
        await setActiveDatasetInfo(info);
        return classIndex;
    }
    return -1;
}

async function swapClassNameInActiveDataset(currentName, newName){
    const info = await getActiveDatasetInfo();
    const classes = info.classes;
    const lowerClassName = currentName.toLowerCase();
    const classIndex = classes.indexOf(lowerClassName);
    if(classIndex > -1){
        classes.splice(classIndex, 1, newName);
        info.classes = classes;
        await setActiveDatasetInfo(info);
        return classIndex;
    }
    return -1;
}

async function setActiveDatasetInfo(info, newFile){
    const infoPath = path.join(datasetDir, 'info.json');
    await fs.mkdir(datasetDir, { recursive: true });
    let datasetInfo = { classes: [] }
    if(!newFile){
        datasetInfo = await getActiveDatasetInfo();
    }
    datasetInfo = Object.assign(datasetInfo, info);
    await fs.writeFile(infoPath, JSON.stringify(datasetInfo,null,3));
    loadedActiveDatasetInfo = Object.assign({},datasetInfo);
}

async function clearActiveDataset(){
    try{
        await fs.rm(datasetDir, { recursive: true });
        await fs.mkdir(datasetDir, { recursive: true });
        await fs.mkdir(path.join(datasetDir, 'images'), { recursive: true });
    }catch(err){
        console.log(err)
    }
}

async function createDatasetYAML() {
    const datasetInfo = await getActiveDatasetInfo();
    const classes = datasetInfo.classes;
    await ensureDirExists(datasetDir);
    // YAML content
    let yamlContent = `path: ${datasetDir}\n`;
    yamlContent += `train: images\n`;
    yamlContent += `val: images\n\n`;  // Assuming the same images for validation for simplicity
    yamlContent += `names:\n`;
    classes.forEach((className, index) => {
        yamlContent += `  ${index}: ${className}\n`;
    });
    const writeFileTo = path.join(datasetDir, 'dataset.yaml')
    console.log(yamlContent)
    console.log(writeFileTo)
    await fs.writeFile(writeFileTo, yamlContent);
    await fs.writeFile(`${datasetDir}/classes.txt`,classes.join('\n'))
    return writeFileTo;
}

async function checkAnnotationClassIds(directory) {
    const nc = (await getActiveDatasetInfo()).classes.length;
    let incorrectFiles = [];

    async function checkFile(filePath) {
        try {
            const content = await fs.readFile(filePath, 'utf8');
            const lines = content.trim().split('\n');

            for (let line of lines) {
                const classId = parseInt(line.split(' ')[0], 10);
                if (classId >= nc) {
                    console.log(filePath)
                    console.log('bad line :',line)
                    console.log('all lines :')
                    console.log(lines)
                    incorrectFiles.push(filePath);
                    break;
                }
            }
        } catch (error) {
            console.error(`Error reading file ${filePath}: ${error}`);
        }
    }

    async function scanDirectory(directory) {
        try {
            const items = await fs.readdir(directory, { withFileTypes: true });
            for (let item of items) {
                const fullPath = path.join(directory, item.name);
                if (item.isDirectory()) {
                    await scanDirectory(fullPath);
                } else if (item.isFile() && item.name.endsWith('.txt')) {
                    await checkFile(fullPath);
                }
            }
        } catch (error) {
            console.error(`Error scanning directory ${directory}: ${error}`);
        }
    }

    await scanDirectory(directory);

    return incorrectFiles;
}
async function changeClassIdInLabelTxt(labelPath, newClassId){
    const labelString = await fs.readFile(labelPath,'utf8');
    const lines = labelString.split('\n');
    const newLines = []
    for(line of lines){
        const lineParts = line.split(' ');
        lineParts[0] = newClassId;
        newLines.push(lineParts.join(' '));
    }
    return newLines.join('\n');
}

async function adjustClassIdsToMatchClassOrder(className){
    const classLabelsPath = path.join(datasetLabelDir,className)
    const labels = await fs.readdir(classLabelsPath);
    const newClassId = await getClassIdForActiveDataset(className);
    for (const labelFilename of labels) {
        const labelPath = path.join(classLabelsPath,labelFilename);
        const newLabelString = await changeClassIdInLabelTxt(labelPath, newClassId);
        await fs.writeFile(labelPath, newLabelString);
    }
}

async function adjustClassIdsToMatchClassOrderForAll(ignoreBefore, doAnyway){
    const foldersPresent = await fs.readdir(datasetLabelDir);
    const { classes } = await getActiveDatasetInfo();
    let onNumber = 0;
    for (const className of classes) {
        try{
            if(onNumber >= ignoreBefore || doAnyway === onNumber)await adjustClassIdsToMatchClassOrder(className);
        }catch(err){
            console.log('adjustClassIdsToMatchClassOrderForAll ERR',err)
        }
        ++onNumber;
    }
}

async function mergeClassIntoAnother(classToGrab, classToMergeTo){
    const response = { ok: true }
    try{
        const folderOfLabelsToModify = path.join(datasetLabelDir, classToGrab);
        const folderOfLabelsWritingTo = path.join(datasetLabelDir, classToMergeTo);
        const folderOfImagesToMove = path.join(datasetImageDir, classToGrab);
        const folderOfImagesGoingTo = path.join(datasetImageDir, classToMergeTo);
        const images = await fs.readdir(folderOfImagesToMove);
        const labels = await fs.readdir(folderOfLabelsToModify);
        const classIndexRemoved = await removeClassNameFromActiveDataset(classToGrab);
        const newClassId = await getClassIdForActiveDataset(classToMergeTo);
        for (const imageFilename of images) {
            const sourcePath = path.join(folderOfImagesToMove, imageFilename);
            const destinationPath = path.join(folderOfImagesGoingTo, `${classToGrab}_${imageFilename}`);
            await fs.rename(sourcePath, destinationPath);
        }
        for (const labelFilename of labels) {
            const sourcePath = path.join(folderOfLabelsToModify, labelFilename);
            const destinationPath = path.join(folderOfLabelsWritingTo, `${classToGrab}_${labelFilename}`);
            await fs.rename(sourcePath, destinationPath);
        }
        await fs.rm(folderOfLabelsToModify, { recursive: true });
        await fs.rm(folderOfImagesToMove, { recursive: true });
        await adjustClassIdsToMatchClassOrderForAll(classIndexRemoved, newClassId)
    }catch(err){
        console.log(err)
        response.ok = false;
        response.err = err.toString()
    }
    return response
}

async function renameClassInActiveDataset(currentName, newName){
    const response = { ok: true }
    try{
        const oldLabelsFolderPath = path.join(datasetLabelDir, currentName);
        const newLabelsFolderPath = path.join(datasetLabelDir, newName);
        const oldImagesFolderPath = path.join(datasetImageDir, currentName);
        const newImagesFolderPath = path.join(datasetImageDir, newName);
        console.log(currentName, '--->', newName);
        await fs.rename(oldLabelsFolderPath, newLabelsFolderPath);
        await fs.rename(oldImagesFolderPath, newImagesFolderPath);
        await swapClassNameInActiveDataset(currentName, newName);
    }catch(err){
        console.log(err)
        response.ok = false;
        response.err = err.toString()
    }
    return response
}

async function deleteClassInActiveDataset(className){
    const response = { ok: true }
    try{
        const labelsFolderPath = path.join(datasetLabelDir, className);
        const imagesFolderPath = path.join(datasetImageDir, className);
        await fs.rm(labelsFolderPath, { recursive: true });
        await fs.rm(imagesFolderPath, { recursive: true });
        const classIndexRemoved = await removeClassNameFromActiveDataset(className);
        await adjustClassIdsToMatchClassOrderForAll(classIndexRemoved)
    }catch(err){
        console.log(err)
        response.ok = false;
        response.err = err.toString()
    }
    return response
}

module.exports = {
    datasetDir,
    getActiveDatasetInfo,
    getNextClassIdFromActiveDataset,
    addClassNameToActiveDataset,
    removeClassNameFromActiveDataset,
    setActiveDatasetInfo,
    getClassIdForActiveDataset,
    clearActiveDataset,
    createDatasetYAML,
    checkAnnotationClassIds,
    mergeClassIntoAnother,
    renameClassInActiveDataset,
    deleteClassInActiveDataset,
    adjustClassIdsToMatchClassOrderForAll,
};
