// dirManager.js
const fs = require('fs/promises');

async function ensureDirExists(dir) {
    try {
        await fs.access(dir);
    } catch (error) {
        if (error.code === 'ENOENT') {
            await fs.mkdir(dir, { recursive: true });
        } else {
            throw error;
        }
    }
}

module.exports = { ensureDirExists };
