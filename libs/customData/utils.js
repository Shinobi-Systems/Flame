const fs = require('fs').promises;
const path = require('path');
const imageSize = require('image-size');
const customDataDir = process.cwd() + '/custom_data'
const {
    readAndSortByModifiedDate,
} = require('../utils.js');
const {
    labelFileToJson,
} = require('../trainingManager.js');

function getCustomDatasetDirectory(datasetType, datasetName){
    const typeFolder = path.join(customDataDir, datasetType);
    return datasetName ? path.join(typeFolder, datasetName) : typeFolder;
}

async function getCustomDatasetTypes(){
    await fs.mkdir(customDataDir, { recursive: true })
    return await fs.readdir(customDataDir)
}

async function getCustomDatasets(datasetType = 'roboflow-yolov5'){
    const datasetsPath = path.join(customDataDir,datasetType);
    await fs.mkdir(datasetsPath, { recursive: true })
    return await readAndSortByModifiedDate(datasetsPath)
}

function parseYAML(yamlString) {
    const lines = yamlString.split('\n');
    const result = {};
    const keyValuePattern = /^(\w+):\s*(.*)$/;
    lines.forEach(line => {
        const trimmedLine = line.trim();
        const match = keyValuePattern.exec(trimmedLine);
        if (match) {
            const key = match[1];
            let value = match[2];
            if (value.startsWith('[') && value.endsWith(']')) {
                value = value.substring(1, value.length - 1).split(',').map(item => item.trim().replace(/^'(.*)'$/, '$1'));
            }
            result[key] = value;
        }
    });
    return result;
}

async function appendUniqueText(filePath, txtData) {
  try {
    // Read the existing content of the file
    const existingContent = await fs.readFile(filePath, 'utf8');

    // Check if the txtData is already in the file
    if (!existingContent.includes(txtData)) {
      // If not present, append the txtData
      await fs.appendFile(filePath, txtData);
      // console.log('Text appended successfully.');
    } else {
      // console.log('Text is already present in the file.');
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}

function labelPathToImagePath(filePath) {
    const newPath = filePath.replace('/labels/', '/images/');
    const newFilePath = newPath.replace(/(\.txt)$/, ".jpg");
    return newFilePath;
}

async function getImageMatrices(datasetType, datasetName, filePath){
    const txtPath = path.join(customDataDir, filePath)
    const jpegPath = labelPathToImagePath(txtPath);
    const { width, height } = imageSize(jpegPath);
    const matrices = labelFileToJson(await fs.readFile(txtPath,'utf8'), width, height)
    return { width, height, matrices };
}

function getUniqueTags(matrices) {
    const tagsSet = new Set();
    matrices.forEach(matrix => {
        tagsSet.add(matrix.tag);
    });
    return Array.from(tagsSet);
}

module.exports = {
    getUniqueTags,
    getCustomDatasetTypes,
    getCustomDatasets,
    parseYAML,
    appendUniqueText,
    customDataDir,
    getImageMatrices,
    getCustomDatasetDirectory,
}
