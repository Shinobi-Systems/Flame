const path = require('path');
const fs = require('fs').promises;
const testDataDir = process.cwd() + '/test_sets'
const activeTestDataDir = process.cwd() + '/yolov5/data/images'

async function getTestSets(){
    await fs.mkdir(testDataDir, { recursive: true })
    return await fs.readdir(testDataDir)
}

async function getTestSet(folderName){
    const folderPath = path.join(testDataDir, folderName)
    return await fs.readdir(folderPath)
}

async function getCurrentTestSet(){
    return await fs.readdir(activeTestDataDir)
}

async function clearActiveDirectory(){
    const activeContents = await fs.readdir(activeTestDataDir)
    for(file of activeContents){
        const filePath = path.join(activeTestDataDir, file)
        await fs.unlink(filePath)
    }
}

async function copyToActiveDirectory(folderName){
    const folderPath = path.join(testDataDir, folderName)
    const targetContents = await fs.readdir(folderPath)
    for(file of targetContents){
        const filePath = path.join(folderPath, file)
        const activeFilePath = path.join(activeTestDataDir, file)
        const fileBuffer = await fs.readFile(filePath)
        await fs.writeFile(activeFilePath, fileBuffer)
    }
}

async function setTestSetToActive(folderName){
    await clearActiveDirectory()
    await copyToActiveDirectory(folderName)
}

module.exports = {
    testDataDir,
    activeTestDataDir,
    getTestSets,
    getTestSet,
    copyToActiveDirectory,
    clearActiveDirectory,
    setTestSetToActive,
    getCurrentTestSet,
}
