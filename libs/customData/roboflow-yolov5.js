const fs = require('fs').promises;
const path = require('path');
const { promisify } = require("util");
const sizeOf = promisify(require("image-size"))
const customDataDir = process.cwd() + '/custom_data'
const activeDatasetPath = process.cwd() + '/datasets'
const {
    getCustomDatasetTypes,
    getCustomDatasets,
    parseYAML,
    appendUniqueText,
    getUniqueTags,
} = require('./utils.js')
const {
    getActiveDatasetInfo,
    getClassIdForActiveDataset,
    addClassNameToActiveDataset,
    setActiveDatasetInfo,
} = require('../trainingManager/activeDatasets.js')

async function getInfo(datasetName, datasetType = 'roboflow-yolov5'){
    try{
        const datasetPath = path.join(customDataDir,datasetType)
        const identifierPath = path.join(datasetPath,datasetName,'README.dataset.txt');
        const readmeLines = (await fs.readFile(identifierPath,'utf8')).split('\n');
        const link = readmeLines[1];
        const downloadUrl = readmeLines[6];
        const modelLicense = readmeLines[4].split(':')[1].trim();
        let yaml = {names:[]};
        const yamlString = await fs.readFile(path.join(datasetPath,datasetName,'data.yaml'),'utf8')
        yaml = await parseYAML(yamlString);
        console.log('## Reading YAML...')
        const {
            trainDir,
            validDir,
            testDir,
            names,
            workspace,
            project,
            version,
            license,
            url,
        } = yaml;
        const classes = yaml.names
        const numberOfClasses = classes.length
        const response = {
            link,
            downloadUrl,
            modelLicense,
            classes,
            numberOfClasses,
            trainDir,
            validDir,
            testDir,
            names,
            workspace,
            project,
            version,
            license,
            url,
        };
        return response;
    }catch(err){
        console.error('No YAML',err)
        return null;
    }
}
async function addLineToReadme(folderName, lineString, datasetType = 'roboflow-yolov5'){
    const datasetPath = path.join(customDataDir, datasetType)
    const identifierPath = path.join(datasetPath, folderName, 'README.dataset.txt');
    await appendUniqueText(identifierPath, lineString)
}
async function convertLabels(inputLabels, imageWidth, imageHeight, classes, isNormal) {
    const lines = inputLabels.trim().split('\n');
    const convertedLabels = [];
    const separated = {};
    const response = {};
    for (const line of lines){
        const values = line.split(' ');
        if(values.length > 3){
            let convertedLabel = ''
            let className = ''
            let classId = ''
            if(isNormal){
                const classIdInRaw = parseInt(values[0]);
                className = classes[classIdInRaw];
                const trainingClasses = (await getActiveDatasetInfo()).classes;
                classId = await getClassIdForActiveDataset(className);
                values[0] = classId;
                convertedLabel = values.join(' ');
            }else{
                const x1 = parseFloat(values[0]);
                const y1 = parseFloat(values[1]);
                const x2 = parseFloat(values[4]);
                const y2 = parseFloat(values[5]);
                className = values[8];
                classId = await getClassIdForActiveDataset(className);
                const width = x2 - x1;
                const height = y2 - y1;
                const centerX = x1 + width / 2;
                const centerY = y1 + height / 2;
                const normalizedCenterX = centerX / imageWidth;
                const normalizedCenterY = centerY / imageHeight;
                const normalizedWidth = width / imageWidth;
                const normalizedHeight = height / imageHeight;
                convertedLabel = `${classId} ${normalizedCenterX.toFixed(6)} ${normalizedCenterY.toFixed(6)} ${normalizedWidth.toFixed(6)} ${normalizedHeight.toFixed(6)}`;
            }
            convertedLabels.push(convertedLabel);
            if(!separated[className])separated[className] = [];
            separated[className].push(convertedLabel)
        }
    }
    for(const className in separated){
        separated[className] = separated[className].join('\n');
    }
    response.byClass = separated;
    response.string = convertedLabels.join('\n');
    return response;
}
async function getCustomDatasetImages(datasetName, withFullPath, datasetType = 'roboflow-yolov5', noPathPrefix){
    const datasetPath = path.join(customDataDir, datasetType)
    const datasetFolder = path.join(datasetPath,datasetName);
    const response = {
        'all': {'images':[],'labelTxt':[]}
    }
    const imageFolders = ['valid', 'train', 'test'];
    for (let i = 0; i < imageFolders.length; i++) {
        const imageFolder = imageFolders[i];
        const subDirs = ['images', 'labelTxt', 'labels'];
        response[imageFolder] = {};
        for (let i = 0; i < subDirs.length; i++) {
            const subDir = subDirs[i];
            const subDirPath = path.join(datasetFolder,imageFolder,subDir);
            try{
                let files = await fs.readdir(subDirPath)
                const isNormal = subDir === 'labels';
                if(isNormal)response.isNormal = true;
                if(noPathPrefix){
                    files = files.map((item) => path.join(imageFolder,subDir,item));
                }else if(withFullPath){
                    files = files.map((item) => path.join(subDirPath,item));
                }
                response[imageFolder][isNormal ? 'labelTxt' : subDir] = files;
                response['all'][subDir].push(...files)
            }catch(err){
                // console.log(err)
            }
        }
    }
    return response
}
async function linkToActiveDataset(
    datasetName,
    datasetType = 'roboflow-yolov5',
    selectedClasses = [],
){
    const response = {ok: true}
    try {
        console.log(`Setting Dataset for Training : ${datasetType}/${datasetName}`);
        const dataset = await getCustomDatasetImages(datasetName, true, datasetType);
        const { images, labelTxt } = dataset.all;
        const isNormal = dataset.isNormal;
        const dataInfo = await getInfo(datasetName, datasetType);
        const { classes } = dataInfo;
        const doAllClasses = selectedClasses.length === 0;
        for(const imageFilePath of images){
            const labelPath = imageFilePath.replace('/images/', isNormal ? '/labels/' : '/labelTxt/').replace(/\.\w+$/, '.txt');
            const imageFileName = imageFilePath.split('/').pop();
            const imageName = imageFileName.replace(/\.\w+$/, '');
            try{
                const labelString = await fs.readFile(labelPath,'utf8');
                const { width: imageWidth, height: imageHeight } = await sizeOf(imageFilePath);
                const convertedLabels = await convertLabels(labelString, imageWidth, imageHeight, classes, isNormal);
                const byClass = convertedLabels.byClass;
                for(const className in byClass){
                    if(doAllClasses || selectedClasses.includes(className)){
                        const targetImageDir = path.join(activeDatasetPath, 'images', className)
                        const targetImagePath = path.join(targetImageDir, imageFileName);
                        const targetLabelDir = path.join(activeDatasetPath, 'labels', className)
                        const targetLabelPath = path.join(targetLabelDir, `${imageName}.txt`);
                        await fs.mkdir(targetImageDir,{ recursive: true });
                        await fs.mkdir(targetLabelDir,{ recursive: true });
                        try{
                            await fs.stat(targetLabelPath);
                            await appendUniqueText(targetLabelPath, byClass[className]);
                        }catch(err){
                            await fs.writeFile(targetLabelPath, byClass[className]);
                        }
                        try{
                            await fs.link(imageFilePath, targetImagePath);
                            // console.log(imageFilePath, '--->', targetImagePath)
                        }catch(err){

                        }
                    }
                }
            }catch(err){
                console.error('linkToActiveDataset file ERROR :', err);
            }
        }
        const roboflowDatasetsLinked = (await getActiveDatasetInfo()).roboflowDatasetsLinked || {};
        roboflowDatasetsLinked[datasetName] = dataInfo;
        await setActiveDatasetInfo({
            roboflowDatasetsLinked
        });
    } catch (err) {
        console.error('An error occurred:', err);
        response.ok = false;
        response.err = err;
    }
}
async function getNextClassIdFromArchivedDataset(datasetName, datasetType){
    const info = await getInfo(datasetName, datasetType);
    return info.classes.length;
}
async function getClassIdForArchivedDataset(datasetName, datasetType, className){
    const info = await getInfo(datasetName, datasetType);
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    const index = classes.indexOf(lowerClassName);
    if(index === -1){
        await addClassNameToArchivedDataset(datasetName, datasetType, lowerClassName)
        return classes.length - 1;
    }else{
        return index;
    }
}
async function addClassNameToArchivedDataset(datasetName, datasetType, className){
    const info = await getInfo(datasetName, datasetType);
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    if(classes.indexOf(lowerClassName) === -1){
        classes.push(lowerClassName);
        info.classes = classes;
        await setArchivedDatasetYaml(datasetName, datasetType, info);
        return true;
    }
    return false;
}
async function removeClassNameFromArchivedDataset(datasetName, datasetType, className){
    const info = await getInfo(datasetName, datasetType);
    const classes = info.classes;
    const lowerClassName = className.toLowerCase();
    const classIndex = classes.indexOf(lowerClassName);
    if(classIndex > -1){
        classes.splice(classIndex, 1);
        info.classes = classes;
        await setArchivedDatasetYaml(datasetName, datasetType, info);
        return classIndex;
    }
    return -1;
}
async function swapClassNameInArchivedDataset(datasetName, datasetType, currentName, newName){
    const info = await getInfo(datasetName, datasetType);
    const classes = info.classes;
    const lowerClassName = currentName.toLowerCase();
    const classIndex = classes.indexOf(lowerClassName);
    if(classIndex > -1){
        classes.splice(classIndex, 1, newName);
        info.classes = classes;
        await setArchivedDatasetYaml(datasetName, datasetType, info);
        return classIndex;
    }
    return -1;
}
async function setArchivedDatasetYaml(datasetName, datasetType, {
    trainDir = './train/images',
    validDir = './valid/images',
    testDir = './test/images',
    classes = [],
    workspace = 'Flame_Video_Annotator',
    project,
    version = '3',
    license = 'PRIVATE, PERMISSION REQUIRED',
    url = 'https://shinobi.video',
}){
    const writePath = path.join(customDataDir, datasetType, datasetName, 'data.yaml');
    var readmeData = `train: ${trainDir}
val: ${validDir}
test: ${testDir}

nc: ${classes.length}
names: [${classes.map(item => `'${item}'`).join(', ')}]

roboflow:
  workspace: ${workspace}
  project: ${project}
  version: ${version}
  license: ${license}
  url: ${url}`;
    await fs.writeFile(writePath, readmeData);
}
async function setArchivedDatasetReadme(datasetName, datasetType, {
    workspace = 'Flame_Video_Annotator',
    project,
    license = 'PRIVATE, PERMISSION REQUIRED',
    url = 'https://shinobi.video',
    downloadUrl = 'https://shinobi.video',
    time = new Date()
}){
    const writePath = path.join(customDataDir, datasetType, datasetName, 'README.dataset.txt');
    var readmeData = `# ${project || datasetName} > ${time}
${url}

Provided by ${workspace}
License: ${license}

${downloadUrl}`;
    await fs.writeFile(writePath, readmeData);
}
async function setAnnotationsFromMatrices(datasetName, datasetType, {
    filename,
    matrices,
    width: imageWidth,
    height: imageHeight,
}){
    const writePath = path.join(customDataDir, datasetType, datasetName, 'train', 'labels', `${filename}.txt`);
    let string = [];
    for(matrix of matrices){
        const className = matrix.tag;
        const classNumber = await getClassIdForArchivedDataset(datasetName, datasetType, className);
        const { tag, x, y, width, height } = matrix;
        const xCenter = x + width / 2;
        const yCenter = y + height / 2;
        const normalizedXCenter = xCenter / imageWidth;
        const normalizedYCenter = yCenter / imageHeight;
        const normalizedWidth = width / imageWidth;
        const normalizedHeight = height / imageHeight;
        string.push(`${classNumber} ${normalizedXCenter.toFixed(6)} ${normalizedYCenter.toFixed(6)} ${normalizedWidth.toFixed(6)} ${normalizedHeight.toFixed(6)}`);
    }
    await fs.writeFile(writePath, string.join('\n'));
}
async function saveImageFrameToCustomDataset(datasetName, datasetType, {
    videoName,
    filename,
    imageBuffer,
    matrices,
    height,
    width,
}){
    videoName = videoName || datasetName;
    const response = { ok: true }
    const info = await getInfo(datasetName, datasetType);
    const datasetPath = path.join(customDataDir, datasetType, datasetName);
    const imagesSavePath = path.join(datasetPath, 'train', 'images');
    const labelsSavePath = path.join(datasetPath, 'train', 'labels');
    const imagePath = path.join(imagesSavePath, `${filename}.jpg`);
    await fs.mkdir(imagesSavePath, { recursive: true });
    await fs.mkdir(labelsSavePath, { recursive: true });
    console.log(`imagesSavePath`,imagesSavePath)
    console.log(`labelsSavePath`,labelsSavePath)
    if(!info){
        const setupInfo = {
            classes: getUniqueTags(matrices),
            workspace: datasetType,
            project: datasetName,
        }
        await setArchivedDatasetYaml(datasetName, datasetType, setupInfo)
        await setArchivedDatasetReadme(datasetName, datasetType, setupInfo)
    }
    if(imageBuffer.mv){
        const moveError = await (new Promise((resolve) => {imageBuffer.mv(imagePath, (err) => {resolve(err)})}))
        if(moveError){
            response.ok = false;
            response.err = moveError.toString()
            return response
        }
    }else{
        await fs.writeFile(imagePath, imageBuffer)
    }
    await setAnnotationsFromMatrices(datasetName, datasetType, {
        filename,
        matrices,
        width,
        height,
    })
    return response
}

module.exports = {
    getInfo,
    convertLabels,
    getCustomDatasetImages,
    linkToActiveDataset,
    addLineToReadme,
    saveImageFrameToCustomDataset,
}
