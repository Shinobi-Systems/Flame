const fs = require('fs').promises;
const path = require('path');
const axios = require('axios');
const csv = require('csv-parser');
const imageSize = require('image-size');
const async = require('async');
const { spawn } = require('child_process');
const { createReadStream, createWriteStream } = require('fs');
const { pipeline } = require('stream');
const { promisify } = require('util');
const streamPipeline = promisify(pipeline);
const { ensureDirExists } = require('./dirManager.js');
const { grepFile, makeSafeForFolder, findCommonElements } = require('./utils.js');
const openImageDataDir = path.join(process.cwd(), `openImagesData`);
const datasetDir = path.join(process.cwd(), `datasets`);
const decoBboxFolder = path.join(process.cwd(), 'decompiled','oidv6-train-annotations-bbox');
const decoVrdFolder = path.join(process.cwd(), 'decompiled','oidv6-train-annotations-vrd');
const classDescriptionsCsvPath = path.join(process.cwd(), 'openImagesData/oidv7-class-descriptions-boxable.csv');
const {
    getActiveDatasetInfo,
    getClassIdForActiveDataset,
    addClassNameToActiveDataset,
    setActiveDatasetInfo,
} = require('./trainingManager/activeDatasets.js')

async function linkDatasetsToActive(classNames = [], relationships = []) {
    const sourceFolder = path.join(openImageDataDir)
    const targetFolder = path.join(datasetDir)
    const response = { ok: true }
    try {
        const classes = [...classNames, ...relationships];
        await fs.mkdir(targetFolder, { recursive: true });
        console.log(`Adding to Dataset for Training : ${sourceFolder} --> ${targetFolder}`);
        console.log('Classes : ',classes.join(', '));
        try{
            for (const className of classes) {
                const labelId = await getLabelId(className);
                const folder = `images/${className}`;
                const sourceDir = path.join(sourceFolder, folder);
                const targetDir = path.join(targetFolder, folder);
                await fs.mkdir(targetDir, { recursive: true });
                const files = await fs.readdir(sourceDir);

                for (const file of files) {
                    const imageId = file.replace(/\.\w+$/, '');
                    const sourceFile = path.join(sourceDir, file);
                    const targetFile = path.join(targetDir, file);
                    try {
                        await fs.access(targetFile);
                    } catch (err) {
                        await fs.link(sourceFile, targetFile);
                    }
                    try {
                        await saveAnnotations(imageId, sourceFile, className, labelId);
                    } catch (err) {
                        // await fs.rm(targetFile);
                        console.error(`Error creating annotes for ${file} in ${folder}:`, err);
                    }
                }
            }
        }catch(err){
            console.error('An error occurred:', err);
            response.ok = false;
            response.err = err;
        }
    } catch (err) {
        console.error('An error occurred:', err);
        response.ok = false;
        response.err = err;
    }
    return response
}


async function saveAnnotations(imageId, filePath, className, labelId) {
    try{
        const classNumber = await getClassIdForActiveDataset(className);
        const labelsFolder = path.join(datasetDir, `labels`, className);
        const decoFilePath = path.join(decoBboxFolder, `${imageId}.csv`);
        const annotationPath = path.join(labelsFolder, `${imageId}.txt`);
        const dimensions = imageSize(filePath);
        const convertedLines = [];
        const lines = (await fs.readFile(decoFilePath,'utf8')).split('\n');
        await fs.mkdir(labelsFolder, { recursive: true }).catch(() => {});
        lines.forEach((line) => {
            if (!line) return;
            const lineParts = line.split(',');
            const lineLabelId = lineParts[2];
            if(labelId === lineLabelId){
                const convertedLine = convertLineToYOLO(
                    classNumber,
                    line,
                    dimensions.width,
                    dimensions.height
                );
                if (convertedLine) {
                    convertedLines.push(convertedLine);
                }
            }
        });
        if(convertedLines.length > 0){
            await fs.appendFile(annotationPath, convertedLines.join('\n'));
        }
    }catch(err){
        console.error('saveAnnotations',err)
    }
}

async function getLabelId(labelName) {
    let labelId = null;
    return new Promise((resolve, reject) => {
        createReadStream(classDescriptionsCsvPath)
            .pipe(csv())
            .on('data', (data) => {
                if (data.DisplayName.toLowerCase() === labelName.toLowerCase()) {
                    labelId = data.LabelName;
                }
            })
            .on('end', () => {
                if (labelId) {
                    resolve(labelId);
                } else {
                    console.log(`Label name ${labelName} not found.`)
                    resolve(null);
                }
            })
            .on('error', reject);
    });
}

function convertLineToYOLO(classNumber, line, imageWidth, imageHeight) {
    const parts = line.split(',');

    if (parts.length < 8) return ''; // Ensure the line has enough data

    const [imageID, source, labelName, confidence, xMin, xMax, yMin, yMax] = parts;

    // Convert string values to numbers and normalize
    const xMinNum = parseFloat(xMin);
    const xMaxNum = parseFloat(xMax);
    const yMinNum = parseFloat(yMin);
    const yMaxNum = parseFloat(yMax);

    const boxWidth = (xMaxNum - xMinNum) * imageWidth;
    const boxHeight = (yMaxNum - yMinNum) * imageHeight;
    const xCenter = (xMinNum + (boxWidth / imageWidth) / 2) * imageWidth;
    const yCenter = (yMinNum + (boxHeight / imageHeight) / 2) * imageHeight;

    // Normalize coordinates to be relative to image dimensions
    const normalizedWidth = boxWidth / imageWidth;
    const normalizedHeight = boxHeight / imageHeight;
    const normalizedXCenter = xCenter / imageWidth;
    const normalizedYCenter = yCenter / imageHeight;

    if (normalizedXCenter < 0 || normalizedXCenter > 1 ||
        normalizedYCenter < 0 || normalizedYCenter > 1 ||
        normalizedWidth < 0 || normalizedWidth > 1 ||
        normalizedHeight < 0 || normalizedHeight > 1) {
        return '';
    }

    return `${classNumber} ${normalizedXCenter.toFixed(6)} ${normalizedYCenter.toFixed(6)} ${normalizedWidth.toFixed(6)} ${normalizedHeight.toFixed(6)}`;
}

function convertVRDLineToYOLO(classNumber, line, imageWidth, imageHeight) {
    const parts = line.split(',');

    if (parts.length < 12) return ''; // Ensure the line has enough data

    const [imageID, labelName1, labelName2, xMin1, xMax1, yMin1, yMax1, xMin2, xMax2, yMin2, yMax2, relationshipLabel] = parts;

    // Convert string values to numbers and normalize
    const xMinNum = parseFloat(xMin1);
    const xMaxNum = parseFloat(xMax1);
    const yMinNum = parseFloat(yMin1);
    const yMaxNum = parseFloat(yMax1);

    const boxWidth = (xMaxNum - xMinNum) * imageWidth;
    const boxHeight = (yMaxNum - yMinNum) * imageHeight;
    const xCenter = xMinNum * imageWidth + boxWidth / 2;
    const yCenter = yMinNum * imageHeight + boxHeight / 2;

    // Normalize coordinates to be relative to image dimensions
    const normalizedWidth = boxWidth / imageWidth;
    const normalizedHeight = boxHeight / imageHeight;
    const normalizedXCenter = xCenter / imageWidth;
    const normalizedYCenter = yCenter / imageHeight;

    if (normalizedXCenter < 0 || normalizedXCenter > 1 ||
        normalizedYCenter < 0 || normalizedYCenter > 1 ||
        normalizedWidth < 0 || normalizedWidth > 1 ||
        normalizedHeight < 0 || normalizedHeight > 1) {
        return '';
    }

    // Assuming '0' as the class index for VRD annotations
    // Modify as needed to reflect the correct class index for your use case
    return `${classNumber} ${normalizedXCenter.toFixed(6)} ${normalizedYCenter.toFixed(6)} ${normalizedWidth.toFixed(6)} ${normalizedHeight.toFixed(6)}`;
}

class ImageDownloader {
    constructor() {
        this.IMAGE_LABELS_CSV = path.join(process.cwd(), 'openImagesData/train-annotations-human-imagelabels-boxable.csv');
        this.BOUNDING_BOXES_CSV = path.join(process.cwd(), 'openImagesData/oidv6-train-annotations-bbox.csv');
        this.BOUNDING_BOXES_VRD_CSV = path.join(process.cwd(), 'openImagesData/oidv6-train-annotations-vrd.csv');
        this.CLASS_DESCRIPTIONS_CSV = classDescriptionsCsvPath;
        this.isRunning = false;
        this.noContinue = false;
        this.getLabelId = getLabelId;
        this.linkDatasetsToActive = linkDatasetsToActive;
    }


    axiosTimeout(url, ms, options = {}) {
        const source = axios.CancelToken.source();
        setTimeout(() => {
            source.cancel(`Timeout of ${ms}ms exceeded`);
        }, ms);
        return axios.get(url, { ...options, responseType: 'stream', cancelToken: source.token });
    }

    async getImagePath(imageId, className, relationships) {
        const downloadFolder = path.join(process.cwd(), `openImagesData/images/${className}`);
        const filePath = path.join(downloadFolder, `${imageId}.jpg`);
        return filePath;
    }
    async downloadOneImage(imageId, className, relationships) {
        const downloadFolder = path.join(process.cwd(), `openImagesData/images/${className}`);
        await fs.mkdir(downloadFolder, { recursive: true }).catch(() => {});

        const filePath = path.join(downloadFolder, `${imageId}.jpg`);

        // Check if the image already exists in the specified class folder
        if (await this.fileExists(filePath)) {
            return filePath; // Image already exists and is not empty
        }

        // Check if image already exists in any class folder
         const existingFilePath = await this.findExistingImage(imageId);
         if (existingFilePath) {
             await fs.link(existingFilePath, filePath); // Create hard link
             return filePath;
         }


        // Image not found in other folders, proceed to download
        return await this.downloadAndSaveImage(imageId, filePath);
    }

    async findExistingImage(imageId) {
        const imagesRoot = path.join(process.cwd(), 'openImagesData/images');
        try {
            for (const folder of this.classes) {
                const possiblePath = path.join(imagesRoot, folder, `${imageId}.jpg`);
                if (await this.fileExists(possiblePath)) {
                    return possiblePath;
                }
            }
        } catch (error) {
            console.error(`Error searching for existing image: ${error}`);
        }
        return null;
    }

    async createHardLinksForRelationships(imageId, filePath, labelName, relationships) {
        for (const relationship of relationships) {
            const relationshipFolder = path.join(process.cwd(), `openImagesData/images/${relationship}`);

            const relationshipFilePath = path.join(relationshipFolder, `${imageId}.jpg`);
            if (!await this.fileExists(relationshipFilePath)) {
                await fs.mkdir(relationshipFolder, { recursive: true }).catch(() => {});
                try {
                    await fs.link(filePath, relationshipFilePath);
                } catch (error) {
                    console.error(`Error creating hard link for ${relationship}: ${error}`);
                }
            }
        }
    }

    async fileExists(filePath) {
        try {
            const stats = await fs.stat(filePath);
            if (stats.size > 0) {
                return true;
            }
            return false;
        } catch {
            return false;
        }
    }

    async downloadAndSaveImage(imageId, filePath) {
        try {
            const imageUrl = `https://open-images-dataset.s3.amazonaws.com/train/${imageId}.jpg`;
            const response = await axios.get(imageUrl, { responseType: 'stream', timeout: 10000 });
            await streamPipeline(response.data, createWriteStream(filePath));
            return filePath;
        } catch (error) {
            console.error(`Error downloading image ${imageId}: ${error.code}`);
            await fs.unlink(filePath).catch(() => {}); // Delete empty or partially downloaded file
            throw error;
        }
    }

    async saveVRDAnnotations(imageId, filePath, className, labelId, relationships = []) {
        for (const relationshipClassName of relationships) {
            try{
                const classNumber = await getClassIdForActiveDataset(relationshipClassName);
                const decoFilePath = path.join(decoVrdFolder, `${imageId}.csv`);
                const labelsFolder = path.join(process.cwd(), `openImagesData/labels/${relationshipClassName}`);
                const vrdAnnotationPath = path.join(labelsFolder, `${imageId}.txt`);
                const lines = (await fs.readFile(decoFilePath,'utf8')).split('\n');
                const dimensions = imageSize(filePath);
                let convertedLines = new Set();
                await fs.mkdir(labelsFolder, { recursive: true }).catch(() => {});
                lines.forEach((line) => {
                    if (!line) return;
                    const lineParts = line.split(',');
                    const lineLabelId = lineParts[1];
                    const lineLabelId2 = lineParts[2];
                    const lineRelationship = lineParts[lineParts.length - 1];
                    if((labelId === lineLabelId || labelId === lineLabelId2) && relationshipClassName === lineRelationship){
                        const convertedLine = convertVRDLineToYOLO(
                            classNumber,
                            line,
                            dimensions.width,
                            dimensions.height
                        );
                        if (convertedLine) {
                            convertedLines.add(convertedLine);
                        }
                    }
                });
                await fs.appendFile(vrdAnnotationPath, Array.from(convertedLines).join('\n'));
            }catch(err){
                console.error('saveVRDAnnotations',err)
            }
        }
    }

    async getImageIdsForClass(labelId, maxImages, relationships = []) {
        const relationsSelected = relationships.length > 0;
        const csvFolder = path.join(process.cwd(), 'decompiled','train-annotations-human-imagelabels-boxable-by-obj');
        const csvFilePath = path.join(csvFolder, `${makeSafeForFolder(labelId)}.csv`);
        const imageIds = (await fs.readFile(csvFilePath,'utf8')).split('\n').slice(0,maxImages);
        return imageIds;
    }

    async getImageIdsForRelationshipClass(labelId, maxImages, relationships = []) {
        const imageIds = [];
        const relationsSelected = relationships.length > 0;
        if(relationsSelected){
            try{
                const csvFolder = path.join(process.cwd(), 'decompiled','oidv6-train-annotations-vrd-by-obj');
                const csvFilePath = path.join(csvFolder, `${makeSafeForFolder(labelId)}.csv`);
                const lines = (await fs.readFile(csvFilePath,'utf8')).split('\n');
                lines.forEach((line) => {
                    const lineParts = line.split(',');
                    const lineLabelId = lineParts[1];
                    const lineLabelId2 = lineParts[2];
                    const lineRelationship = lineParts[lineParts.length - 1].trim();
                    for (const relationshipClassName of relationships) {
                        if(relationshipClassName === lineRelationship){
                            const imageId = lineParts[0];
                            if (imageIds.length < maxImages){
                                imageIds.push(imageId);
                            }
                        }
                    }
                });
            }catch(err){
                console.log(err)
                return [];
            }
        }
        return [...new Set(imageIds)];
    }

    async eraseLabels(classes, relationships){
        const folders = [...classes, ...relationships];
        for (let i = 0; i < folders.length; i++) {
            const labelName = folders[i]
            const labelsFolder = path.join(process.cwd(), `openImagesData/labels`, labelName);
            try{await fs.rm(labelsFolder, {recursive: true});}catch(err){}
            try{await fs.rm(`${labelsFolder}.cache`, {recursive: true});}catch(err){}
            try{await fs.mkdir(labelsFolder, {recursive: true});}catch(err){}
        }
        return {ok: true}
    }

    async stop(){
        this.noContinue = true;
        return {ok: true}
    }



    async run({
        classes = [],
        relationships = [],
        maxImages,
        labelCreationOnly = false,
        onEvent = (data) => { console.log(data) }
    }) {
        try {
            if (this.isRunning) {
                onEvent({ msg: 'running' })
                return;
            }
            classes = classes.filter(item => !!item);
            relationships = relationships.filter(item => !!item);
            onEvent({ msg: 'eraseLabels' });
            await this.eraseLabels(classes, relationships);
            onEvent({ msg: 'starting' });
            this.isRunning = true;
            this.noContinue = false;
            this.classes = classes;
            onEvent({ msg: 'started', classes });
            let n = 0;
            const hasRelationships = relationships.length > 0;
            const queueSize = 5;
            let completedImages = 0;
            let overallTotal = 0;

            for (let i = 0; i < classes.length; i++) {
                if (this.noContinue) {
                    onEvent({ msg: 'stopped', classes })
                    break;
                }

                const labelName = classes[i];
                onEvent({ msg: 'startedClass', label: labelName, relationships });
                const labelId = await this.getLabelId(labelName);
                if(!labelId)continue;
                const imageIds = await this.getImageIdsForClass(labelId, maxImages, relationships);
                const relationshipImageIds = hasRelationships ? await this.getImageIdsForRelationshipClass(labelId, maxImages, relationships) : [];
                console.log('relationshipImageIds',relationshipImageIds.length)
                const commonIds = new Set(hasRelationships ? findCommonElements(imageIds, relationshipImageIds) : []);
                const totalImages = imageIds.length + relationshipImageIds.length;
                overallTotal += totalImages;
                // Queue for processing imageIds
                const queue = async.queue(async (task, callback) => {
                    if (this.noContinue) {
                        queue.pause();  // Pause the queue
                        queue.remove(() => true);  // Remove all pending tasks
                        callback();  // End the current task
                        return;
                    }
                    try {
                        const { imageId, labelName, relationships, commonIds, labelId, classes, totalImages } = task;
                        const timeStartDownload = new Date();
                        const filePath = labelCreationOnly ? await this.getImagePath(imageId, labelName, relationships) : await this.downloadOneImage(imageId, labelName, relationships);
                        if (commonIds.has(imageId)) await this.createHardLinksForRelationships(imageId, filePath, labelName, relationships);
                        ++n;
                        ++completedImages;
                        onEvent({
                            msg: 'progress',
                            label: labelName,
                            relationships,
                            id: imageId,
                            progress: (completedImages / overallTotal * 100).toFixed(1),
                            creationSpeed: new Date() - timeStartDownload,
                            on: n,
                            of: totalImages,
                            overall: completedImages,
                            overallTotal,
                        });
                    } catch (error) {
                        onEvent({
                            label: labelName,
                            msg: 'error',
                            failedDownload: n,
                            error: error.toString()
                        });
                    }
                    callback();
                }, queueSize); // Concurrency level is 3

                // Add tasks to the queue
                imageIds.forEach(imageId => {
                    queue.push({ imageId, labelName, relationships, commonIds, labelId, classes, totalImages });
                });

                // Wait for all tasks to finish
                await queue.drain();

                // Check if we should continue after the first batch
                if (this.noContinue) {
                    break;
                }

                if(relationshipImageIds.length > 0){
                    // Repeat similar logic for relationshipImageIds
                    const relationshipQueue = async.queue(async (task, callback) => {
                        if (this.noContinue) {
                            relationshipQueue.pause();  // Pause the queue
                            relationshipQueue.remove(() => true);  // Remove all pending tasks
                            callback();  // End the current task
                            return;
                        }
                        try {
                            const { imageId, labelName, relationships, labelId, classes, totalImages } = task;
                            const timeStartDownload = new Date();
                            const filePath = labelCreationOnly ? await getImagePath(imageId, labelName, relationships) : await this.downloadOneImage(imageId, labelName, relationships);
                            ++n;
                            ++completedImages;
                            onEvent({
                                msg: 'progress',
                                label: labelName,
                                relationships,
                                id: imageId,
                                progress: (completedImages / overallTotal * 100).toFixed(1),
                                creationSpeed: new Date() - timeStartDownload,
                                on: n,
                                of: totalImages,
                                overall: completedImages,
                                overallTotal,
                            });
                        } catch (error) {
                            onEvent({
                                label: labelName,
                                msg: 'error',
                                failedDownload: n,
                                error: error.toString()
                            });
                        }
                        callback();
                    }, queueSize);

                    relationshipImageIds.forEach(imageId => {
                        relationshipQueue.push({ imageId, labelName, relationships, commonIds, labelId, classes, totalImages });
                    });

                    await relationshipQueue.drain();
                }

                if (this.noContinue) {
                    break;
                }
                n = 0;
            }

            this.isRunning = false;
            onEvent({ classes, msg: 'success' });
        } catch (error) {
            console.error('DL RUNNER', error);
            onEvent({ classes, msg: 'error', error });
        }
    }

}

module.exports = ImageDownloader;
