import argparse
from flask import Flask, request, jsonify, render_template_string
from pathlib import Path
import torch
from models.common import DetectMultiBackend
from utils.general import non_max_suppression, scale_boxes, check_img_size
from utils.torch_utils import select_device
import numpy as np
from PIL import Image
from utils.augmentations import letterbox  # Import letterbox from YOLOv5 utils
import logging

app = Flask(__name__)

logging.getLogger('werkzeug').setLevel(logging.WARNING)

# HTML template for the upload form
UPLOAD_FORM = '''
<!doctype html>
<title>Upload Image for Detection</title>
<h1>Upload Image for Detection</h1>
<form method=post enctype=multipart/form-data action="/detect">
  <input type=file name=file>
  <input type=submit value=Upload>
</form>
'''

def load_classes(path):
    with open(path, 'r') as file:
        return [line.strip() for line in file.readlines()]

def detect_image(image, model, device, imgsz, conf_thres, iou_thres, classes, agnostic_nms, max_det, class_names):
    # Convert PIL image to NumPy array
    img = np.array(image)

    # Resize and pad image to the expected size
    img = letterbox(img, imgsz, stride=model.stride, auto=True)[0]  # Resize and pad

    # Convert from BGR (OpenCV standard) to RGB
    img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to CHW format

    # Normalize and add batch dimension
    img = np.ascontiguousarray(img)
    img = torch.from_numpy(img).to(device)
    img = img.float()  # uint8 to fp32
    img /= 255.0  # 0 - 255 to 0.0 - 1.0
    img = img.unsqueeze(0)  # Add batch dimension

    # Run model
    pred = model(img, augment=False, visualize=False)

    # NMS
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)
    det = pred[0]

    # Process detections
    results = []
    if len(det):
        det[:, :4] = scale_boxes(img.shape[2:], det[:, :4], image.size).round()
        for *xyxy, conf, cls in reversed(det):
            class_id = int(cls)
            class_name = class_names[class_id] if class_names else str(class_id)
            results.append({
                'class': class_name,
                'confidence': float(conf),
                'bbox': [int(xyxy[0]), int(xyxy[1]), int(xyxy[2]), int(xyxy[3])]
            })

    return results

@app.route('/')
def index():
    return render_template_string(UPLOAD_FORM)

@app.route('/detect', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify(error='No file part'), 400
    file = request.files['file']
    if file.filename == '':
        return jsonify(error='No selected file'), 400
    if file:
        image = Image.open(file.stream)
        results = detect_image(image, model, device, imgsz, conf_thres, iou_thres, classes, agnostic_nms, max_det, class_names)
        return jsonify(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=8989, help='port number')
    parser.add_argument('--weights', type=str, default='yolov5s.pt', help='model path')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--imgsz', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25, help='confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='NMS IoU threshold')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--max-det', type=int, default=1000, help='maximum detections per image')
    parser.add_argument('--classes-file', type=str, help='Path to classes.txt file')

    args = parser.parse_args()

    device = select_device(args.device)
    model = DetectMultiBackend(args.weights, device=device, dnn=True, fp16=False)
    imgsz = check_img_size(args.imgsz, s=model.stride)  # check image size

    conf_thres = args.conf_thres
    iou_thres = args.iou_thres
    classes = args.classes
    agnostic_nms = args.agnostic_nms
    max_det = args.max_det
    class_names = load_classes(args.classes_file) if args.classes_file else None

    app.run(host='0.0.0.0', port=args.port)
