# Flame🔥

Flame is an object detection model trainer with support for YoloV5. It can download images and labels from Google Open Images Database v7 just by having the Class name and Max number of images to download. Once image and label data is downloaded, you can begin training on this data right away just by specifying the Class Names you have downloaded.

This tool is in its infancy and is currently only developed to fulfill the purpose of training weights. The UI may have breakage.

## How to Run

1. Download this repository (on Ubuntu 22.04.3) and enter it.

    ```
    git clone https://gitlab.com/Shinobi-Systems/Flame.git
    cd Flame
    ```

2. Run the install command. During this process if you choose to install NVIDIA Drivers for GPU Acceleration you may need to reboot.

    ```
    npm i
    ```

3. Run app. App will start on port `8989` by default. See `conf.json` for additional system configurations.

    ```
    node app.js
    ```

4. Open Web page.

    ```
    http://SERVER_IP:8099/
    ```

#Interface Notes

https://gitlab.com/Shinobi-Systems/Flame/-/blob/main/documents/INTERFACE.md?ref_type=heads

# Google Open Images v7

https://storage.googleapis.com/openimages/web/visualizer/index.html?type=detection&set=train&c=%2Fm%2F0pcr
