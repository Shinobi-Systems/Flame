$(document).ready(function(){
    const testExperiments = $('#testExperiments')
    const previousTests = $('#previousTests')
    const displayExperiment = $('#displayExperiment')
    async function getTests(){
        const response = await getRequest(`train/test`)
        const folders = response.folders;
        return folders;
    }
    async function getTest(expFolder){
        const response = await getRequest(`train/test/${expFolder}`)
        return response;
    }
    async function drawTests(){
        let html = ''
        const tests = await getTests();
        tests.forEach(function(item){
            var info = item.info || {};
            var data = info.data || {};
            var weightBase = data ? data.weights : 'Unknown';
            html += `<div class="card mb-3" exp-action="viewTest" exp-folder="${item.name}">
                <div class="card-body">
                    <div>${makeBadge(info.exp,'success')} ${makeBadge(weightBase,'info')}</div>
                    <div>${makeBadge(`${lang['Batch Size']} : ${data.batchSize}`,'info')} ${makeBadge(`${lang['Epochs']} : ${data.epochs}`,'info')}</div>
                    <div>${item.classes.map(item => makeBadge(item,'primary')).join('')}</div>
                    <small>${new Date(item.ctime)}</small>
                </div>
            </div>`
        })
        previousTests.html(html)
    }
    async function viewTest(expFolder){
        closeTest()
        let html = ''
        const testData = await getTest(expFolder);
        const otherFiles = testData.files;
        const jpgFiles = testData.jpegs;
        const videoFiles = testData.videos;
        videoFiles.forEach(function(item){
            const videoUrl = `yolov5/runs/detect/${expFolder}/${item}`
            html += `<div class="col-6 contain-image">
                <video src="${videoUrl}" controls autoplay></video>
                <a class="btn btn-sm btn-success" href="${videoUrl}" download>Download</a>
            </div>`
        })
        jpgFiles.forEach(function(item){
            html += `<div class="col-6 contain-image">
                <img src="yolov5/runs/detect/${expFolder}/${item}">
            </div>`
        })
        displayExperiment.html(html)
    }
    function closeTest(){
        displayExperiment.empty()
        // previousTests.find('.card.bg-primary').removeClass('bg-primary')
    }
    testExperiments.on('click','[exp-action]',function(){
        var el = $(this)
        var expAction = el.attr('exp-action')
        switch(expAction){
            case'refreshLists':
                drawTests()
            break;
            case'closeTest':
                closeTest()
            break;
            case'viewTest':
                testExperiments.find('[exp-action="viewTest"]').removeClass('bg-primary')
                var expFolder = el.attr('exp-folder')
                viewTest(expFolder)
                el.addClass('bg-primary')
            break;
        }
    });
    var drawnTestsOnce = false;
    $('[href="#testExperiments"]')
        .on('shown.bs.tab', async function(e) {
            if(!drawnTestsOnce){
                drawnTestsOnce = true;
                drawTests()
            }
        })
        .on('hidden.bs.tab', async function(e) {
            closeTest()
        });
})
