function parseTrainProgressLogs(data) {
  const allLines = data.split('\n').filter(line => line.trim()).map(line => {
    const parts = line.split(/\s+/);
    const missingPlace = parts[8] === '|' ? 0 : 1;
    const [epoch, totalEpochs] = parts[0].split('/').map(Number);
    const memory = parts[1];
    const metrics = parts.slice(2, 6).map(Number);
    const progress = parts[7].split('%')[0];
    const [currentStep, totalSteps] = parts[9 - missingPlace].split('/').map(Number);
    const [elapsedTime, remainingTime] = parts[10 - missingPlace].match(/\d+:\d+/g);
    const itPerS = parts[11 - missingPlace].match(/\d+\.\d+it\/s/)[0];
    return {
        epoch: epoch + 1,
        totalEpochs: totalEpochs + 1,
        memory,
        metrics,
        progress,
        currentStep,
        totalSteps,
        elapsedTime,
        remainingTime,
        itPerS,
        type: 'progress',
    };
  });
  return allLines[allLines.length - 1];
}

function parseTrainSummaryStatistics(data) {
    const parts = data.split(/\s+/);

    // Extracting the progress details
    const missingPlace = parts[8] === '|' ? 0 : 1;
    const progress = parts[7].split('%')[0];
    const [current, total] = parts[9 - missingPlace].split('/').map(Number);
    const [elapsedTime, remainingTime] = parts[10 - missingPlace].match(/\d+:\d+/g);
    let itPerS = parts[11 - missingPlace].match(/\d+\.\d+it\/s/);
    if(itPerS){
        itPerS = itPerS[0]
    }else{
        itPerS = '0.00it/s'
    }
    return {
      progress,
      currentStep: current,
      totalSteps: total,
      elapsedTime,
      remainingTime,
      itPerS,
      type: 'summary',
    };
}

function calculateTrainOverallProgress(data) {
  if (data.type !== 'progress') {
    return 'Invalid data type';
  }

  // Calculate current epoch progress
  const currentEpochProgress = (data.currentStep / data.totalSteps) * 100;

  // Adjust the current epoch's progress based on the progress percentage reported
  // If the progress field is a percentage string, parse it as a number
  const adjustedCurrentEpochProgress = isNaN(data.progress) ?
    parseFloat(data.progress) :
    currentEpochProgress;

  // Calculate overall progress
  // (Completed epochs + progress in current epoch) / total epochs
  const overallProgress = ((data.epoch - 1 + (adjustedCurrentEpochProgress / 100)) / data.totalEpochs) * 100;

  return overallProgress.toFixed(2) + '%';
}

function parseTrainString(text) {
    const lines = text.split(/\r?\n|\r|\n/g)
    const input = lines[lines.length - 1].trim();
    try{
        if (input.includes('GPU_mem')) {
          return '';
        } else if (input.includes('Class') && input.includes('Images')) {
          return parseTrainSummaryStatistics(input);
        } else if (input.indexOf('/') < input.indexOf('%') && input.includes('/') && input.includes('%') && input.includes('|') && input.includes('G ')) {
          return parseTrainProgressLogs(input);
        } else {
          return input;
        }
    } catch(err){
        console.log(err,input)
        return input
    }
}

function suggestEpochs(numImages) {
    // Constants to define the scaling of epochs with the number of images
    const minEpochs = 3; // Minimum number of epochs
    const scale = 0.5; // Scaling factor

    // Calculate suggested epochs based on a square root scale
    const suggestedEpochs = minEpochs + Math.sqrt(numImages) * scale;

    // Ensure that the number of epochs is at least the minimum
    return Math.max(Math.round(suggestedEpochs), minEpochs);
}
