window.dashboardEventHandlerActions = {}
function createEventHandler(eventName){
    window.dashboardEventHandlerActions[eventName] = []
    return window.dashboardEventHandlerActions[eventName];
}
function executeEventHandlers(eventName, args = []){
    console.log('executeEventHandlers',eventName, dashboardEventHandlerActions[eventName].length, new Error())
    for(theAction of window.dashboardEventHandlerActions[eventName]){
        theAction(...args)
    }
}
function addToEventHandler(eventName, theAction){
    console.log('addToEventHandler',eventName, new Error())
    if(!window.dashboardEventHandlerActions[eventName])createEventHandler(eventName);
    window.dashboardEventHandlerActions[eventName].push(theAction);
}
