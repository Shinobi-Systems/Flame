async function startTraining(){
    const trainOptions = trainForm.serializeObject()
    const doShutdown = $('#trainControl-shutdown').val() === '1';
    trainOptions.doShutdown = doShutdown;
    return await postRequest(`train/start`,trainOptions)
}
async function testWeights(expFolder,selectedWeights){
    const response = await postRequest(`train/test`,{ expFolder, selectedWeights })
    return response;
}
$(document).ready(function(){
    const trainExperiments = $('#trainExperiments')
    const previousTests = $('#previousResults')
    const displayResult = $('#displayResult')
    const selectedResults = $('#selectedResults')
    let loadedResults = {}
    async function getResults(){
        const response = await getRequest(`train/result`)
        const folders = response.folders;
        loadedResults = {}
        folders.forEach((item, i) => {
            loadedResults[item.name] = item;
        });
        return folders;
    }
    async function getResult(expFolder){
        const response = await getRequest(`train/result/${expFolder}`)
        const folders = response.files;
        return folders;
    }
    async function drawResults(){
        let html = ''
        const tests = await getResults();
        tests.forEach(function(item){
            var info = item.info || {};
            var data = info.data || {};
            var weightBase = data ? data.weights : 'Unknown';
            html += `<div class="card mb-3" exp-action="viewResult" exp-folder="${item.name}">
                <div class="card-body">
                    <div>${item.name}</div>
                    <div>${makeBadge(weightBase,'info')}</div>
                    <div>${makeBadge(`${lang['Batch Size']} : ${data.batchSize}`,'info')} ${makeBadge(`${lang['Epochs']} : ${data.epochs}`,'info')}</div>
                    <div>${item.classes.map(item => makeBadge(item,'primary')).join('')}</div>
                    <small>${new Date(item.ctime)}</small>
                    <div>
                        <a exp-action="testResult" exp-folder="${item.name}" class="btn btn-warning btn-sm">Test</a>
                        <a exp-action="createSavedPackage" exp-folder="${item.name}" class="btn btn-success btn-sm">Package</a>
                    </div>
                </div>
            </div>`
        })
        previousTests.html(html)
    }
    async function viewResult(expFolder){
        let html = ''
        const loadedResult = loadedResults[expFolder]
        const allFiles = await getResult(expFolder);
        const jpgFiles = allFiles.filter(item => item.endsWith('.jpg'));
        if(jpgFiles.length === 0){
            html = '<h3 class="text-center">No Data</h3>'
        }else{
            jpgFiles.reverse().forEach(function(item){
                html += `<div class="col-12 mb-3 contain-image">
                    <img src="yolov5/runs/train/${expFolder}/${item}">
                </div>`
            })
        }
        displayResult.html(html)
        selectedResults.html(`<div><pre>${JSON.stringify(loadedResult,null,3)}</pre></div>`)
    }
    trainExperiments.on('click','[exp-action]', async function(){
        var el = $(this)
        var expAction = el.attr('exp-action')
        switch(expAction){
            case'refreshLists':
                drawResults()
            break;
            case'testResult':
                var expFolder = el.attr('exp-folder')
                testWeights(expFolder)
                $('.nav-tabs a[href="#trainingControl"]').tab('show');
            break;
            case'viewResult':
                trainExperiments.find('[exp-action="viewResult"]').removeClass('bg-primary')
                var expFolder = el.attr('exp-folder')
                viewResult(expFolder)
                el.addClass('bg-primary')
            break;
            case'createSavedPackage':
                var expFolder = el.attr('exp-folder')
                var response = await createSavedPackage(expFolder)
                if(!response.ok){
                    $.confirm.create({
                        title: `Failed to Save`,
                        body: `Could not save package.`,
                        clickOptions: {
                            class: 'btn-success',
                            title: 'Ok',
                        },
                        clickCallback: function(){}
                    })
                }else{
                    $.confirm.create({
                        title: `Packaged Weights`,
                        body: `Download now?`,
                        clickOptions: {
                            class: 'btn-success',
                            title: 'Download',
                        },
                        clickCallback: function(){
                            downloadSavedPackage(expFolder)
                        }
                    })
                }
            break;
        }
    });
    $('[href="#trainExperiments"]')
        .on('shown.bs.tab', async function(e) {
            drawResults()
        })
})
