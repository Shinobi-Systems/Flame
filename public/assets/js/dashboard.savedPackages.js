async function listSavedPackages(){
    const response = await getRequest(`savedPackages/list`)
    return response.files;
}
async function createSavedPackage(expFolder, selectedWeights){
    const response = await postRequest(`savedPackages/create`, { expFolder, selectedWeights })
    return response;
}
async function getSavedPackageInfo(expFolder){
    const response = await getRequest(`savedPackages/info/${expFolder}`)
    const info = response.info;
    return info;
}
async function downloadSavedPackage(expFolder){
    const url = `/api/savedPackages/fs/${expFolder}.zip`
    const info = await getSavedPackageInfo(expFolder)
    const classes = info.data.classes
    downloadFile(url, `${expFolder}-${classes.join('_')}.zip`)
}
$(document).ready(function(){
    const theEnclosure = $('#savedPackages')
    const theList = $('#savedPackagesList')
    const displayInfo = $('#displayInfo')
    async function getList(){
        const response = await getRequest(`savedPackages/list`)
        const folders = response.files;
        return folders;
    }
    async function drawList(){
        let html = ''
        const tests = await getList();
        tests.forEach(function(item){
            html += `<div class="card mb-3" exp-action="viewInfo" exp-folder="${item.name}">
                <div class="card-body">
                    <div>${item.name}</div>
                    <div>${item.classes.map(item => makeBadge(item,'primary')).join('')}</div>
                    <small>${new Date(item.ctime)}</small>
                    <div>
                        <a class="btn btn-sm btn-success" exp-action="download" href="api/savedPackages/fs/${item.name}.zip">Download</a>
                    </div>
                </div>
            </div>`
        })
        theList.html(html)
    }
    async function drawListItem(expFolder){
        const info = await getSavedPackageInfo(expFolder);
        displayInfo.html(`
            <pre class="mb-3"><code>${info.datasetYaml}</code></pre>
            <pre><code>${JSON.stringify(info, null, 3)}</code></pre>
        `)
    }
    theEnclosure.on('click','[exp-action]',async function(){
        var el = $(this)
        var expAction = el.attr('exp-action')
        switch(expAction){
            case'refreshLists':
                drawList()
            break;
            case'viewInfo':
                theEnclosure.find('[exp-action="viewInfo"]').removeClass('bg-primary')
                var expFolder = el.attr('exp-folder')
                drawListItem(expFolder)
                el.addClass('bg-primary')
            break;
            case'download':
                downloadSavedPackage(expFolder)
            break;
        }
    });
    var drawnTestsOnce = false;
    $('[href="#savedPackages"]')
        .on('shown.bs.tab', async function(e) {
            if(!drawnTestsOnce){
                drawnTestsOnce = true;
                drawDatasets()
            }
        })
})
