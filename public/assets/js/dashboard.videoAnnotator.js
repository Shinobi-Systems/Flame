$(document).ready(function(){
    const theEnclosure = $("#videoAnnotator")
    const matrixEditor = theEnclosure.find(".matrix-editor")
    const uploadElement = $("#videoAnnotatorDropUploadArea")
    const remoteUrlInput = $("#videoAnnotatorRemoteUrlInput")
    const shinobiUrlInput = $("#videoAnnotatorShinobiUrlInput")
    var frameAnnotationsInMemory = {}
    var frameAnnotationsMadeByHuman = JSON.parse(localStorage.getItem('frameAnnotationsMadeByHuman')) || {}
    var frameBuffersInMemory = {}
    var loadedVideo
    var videoPlaying = false
    var seekBar = theEnclosure.find(".seekbar")
    var seekBarSpan = seekBar.find("span")
    var playVideoCurrentTime = 0
    var videoPlaying = false
    var playVideoInterval = null
    var resetMatricesInterval = null
    let stopGeneratingFrames = false;
    let doSaveImagesQueue = true;
    function saveFrameAnnotations(videoId){
        const frameAnnotationsVideoIds = JSON.parse(localStorage.getItem(`frameAnnotationsVideoIds`)) || {};
        frameAnnotationsVideoIds[videoId] = true;
        localStorage.setItem(`frameAnnotationsVideoIds`, JSON.stringify(frameAnnotationsVideoIds))
        localStorage.setItem(`frameAnnotations_${videoId}`, JSON.stringify(frameAnnotationsInMemory[videoId]))
    }
    function deleteFrameAnnotations(videoId){
        const frameAnnotationsVideoIds = JSON.parse(localStorage.getItem(`frameAnnotationsVideoIds`)) || {};
        delete(frameAnnotationsVideoIds[videoId])
        localStorage.setItem(`frameAnnotationsVideoIds`, JSON.stringify(frameAnnotationsVideoIds))
        localStorage.removeItem(`frameAnnotations_${videoId}`)
    }
    function getFrameAnnotations(videoId){
        var annotations = frameAnnotationsInMemory[videoId]
        if(!annotations){
            annotations = JSON.parse(localStorage.getItem(`frameAnnotations_${videoId}`)) || {}
            frameAnnotationsInMemory[videoId] = annotations
        }
        return frameAnnotationsInMemory[videoId]
    }
    window.getFrameAnnotations = getFrameAnnotations;
    window.getNextAvailableFrameAnnotationsStep = getNextAvailableFrameAnnotationsStep;
    function getAllSetFrameTimes(){
        var matrixContainers = getFrameAnnotations(loadedVideo.name)
        return Object.keys(matrixContainers)
    }
    function resetMatrixPositions(time, ratios) {
        var matrixEls = theEnclosure.find('[matrix-id]');
        var matrices = getFrameAnnotations(loadedVideo.name)[time]
        if(matrices){
            var matrixSpeed = 10
            var setFrameKeys = getAllSetFrameTimes(loadedVideo)
            var arrayPositionOfFrame = setFrameKeys.indexOf(time)
            if(arrayPositionOfFrame > 0){
                matrixSpeed = (time - setFrameKeys[arrayPositionOfFrame - 1]) * 1000 || 10
            }
            matrices.forEach(function(matrix){
                moveMatrix(matrix,ratios,matrixSpeed)
            })
        }else{
            matrixEls.addClass('border-warning')
        }
    }
    function scanFrames(){
        var lastTime = -1
        var currentVideo = loadedVideo.element[0]
        var ratios = getVideoRatios()
        var time = ensureMultipleOfPointOne(currentVideo.currentTime)
        resetMatrixPositions(time, ratios)
        clearTimeout(resetMatricesInterval)
        resetMatricesInterval = setInterval(() => {
            var time = ensureMultipleOfPointOne(currentVideo.currentTime)
            if(videoPlaying){
                if (time !== lastTime) {
                    resetMatrixPositions(time, ratios)
                    lastTime = time
                }
            }else{
                clearTimeout(resetMatricesInterval)
            }
        },1000 / 25)
    }
    function renderUrlOnToCanvas(theUrl, name) {
        matrixEditor.html(`<div class="matrices"></div><video crossorigin="anonymous" class="annotation-video" preload><source crossorigin="anonymous" src="${theUrl}" type="video/mp4"></source></video>`)
        var smallSlice = theUrl.slice(0,50)
        loadedVideo = {
            name: name || theUrl,
            id: smallSlice.indexOf('data:video') > -1 ? smallSlice : theUrl,
            href: theUrl,
            element: matrixEditor.find('.annotation-video')
        }
        if(!frameBuffersInMemory[loadedVideo.name])frameBuffersInMemory[loadedVideo.name] = {}
        if(!frameAnnotationsMadeByHuman[loadedVideo.name])frameAnnotationsMadeByHuman[loadedVideo.name] = {}

        var vid = loadedVideo.element[0]
        loadedVideo.element.on('timeupdate',function(){
            var percentage = (vid.currentTime / vid.duration) * 100;
            seekBarSpan.css("width", percentage + "%")
        })
        var annotations = getFrameAnnotations(loadedVideo.name);
        var onLoadStepAnnoations = annotations[0] || annotations[getNextAvailableFrameAnnotationsStep(loadedVideo.name, -0.1)] || []
        addNewMatricesToEditor(onLoadStepAnnoations)
        matrixEditor.find('.matrices').mousemove(closestChildOnMouseMove)
    }
    function renderVideoOnToCanvas(file) {
        var reader = new FileReader()
        reader.onload = function(event) {
            theUrl = event.target.result
            renderUrlOnToCanvas(theUrl, file.name)
        }
        reader.readAsDataURL(file)
    }
    function getVideoRatios(){
        var imageEl = matrixEditor.find('.annotation-video')
        var imageWidthInView = imageEl.width()
        var imageHeightInView = imageEl.height()
        imageEl.css({'max-width':'none!important'})
        var imageWidth = imageEl[0].videoWidth
        var imageHeight = imageEl[0].videoHeight
        imageEl.css({'max-width':'100%!important'})
        var xRatio = imageWidth / imageWidthInView
        var yRatio = imageHeight / imageHeightInView
        return {
            xRatio: xRatio,
            yRatio: yRatio
        }
    }
    function getCurrentPositionOfMatrices(){
        var matriceData = {}
        var matricesFoundInView = {
            boundingBoxes: []
        }
        var matricesContainer = matrixEditor.find('.matrices:visible')
        var matricesInView = matricesContainer.find('.matrix')
        var imageEl = matrixEditor.find('.annotation-video')
        var imageWidthInView = imageEl.width()
        var imageHeightInView = imageEl.height()
        imageEl.css({'max-width':'none!important'})
        var imageWidth = imageEl[0].videoWidth
        var imageHeight = imageEl[0].videoHeight
        imageEl.css({'max-width':'100%!important'})
        var xRatio = imageWidth / imageWidthInView
        var yRatio = imageHeight / imageHeightInView
        matricesInView.each(function(n,el){
            var matrix = $(el)
            var position = matrix.position()
            var width = matrix.width()
            var height = matrix.height()
            var id = matrix.attr('matrix-id')
            var tag = matrix.find('input').val().toLowerCase()
            if(tag !== ''){
                matricesFoundInView.boundingBoxes.push({
                    id: id,
                    height: height * yRatio,
                    width: width * xRatio,
                    x: position.left * xRatio,
                    y: position.top * yRatio,
                    tag: tag,
                });
            }else{
                console.log('Object Tag Empty!')
            }
        })
        matriceData.boundingBoxes = matricesFoundInView.boundingBoxes
        matriceData.imageWidth = imageWidth
        matriceData.imageHeight = imageHeight
        return matriceData
    }
    function downloadFile(blob, filename) {
      // if (window.navigator.msSaveOrOpenBlob) {
      //   window.navigator.msSaveOrOpenBlob(blob, filename);
      // } else {
        const a = document.createElement('a');
        document.body.appendChild(a);
        const url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = filename;
        a.click();
        setTimeout(() => {
          window.URL.revokeObjectURL(url);
          document.body.removeChild(a);
        }, 0)
      // }
    }
    async function saveImage(blob, {
        datasetName,
        filename,
        matrices,
        height,
        width,
    }) {
        var formData = new FormData()
        formData.append("datasetType", 'flame-video-annotator')
        formData.append("datasetName", datasetName)
        formData.append("filename", filename + '.txt')
        if(matrices && matrices.length > 0){
            formData.append("matrices", JSON.stringify(matrices))
            formData.append("height", height)
            formData.append("width", width)
            formData.set("file", blob , filename + '.jpg')
            const response = await imageUpload(`customDatasets/uploadFile`,formData)
            return response;
        }
    }
    function snapshotVideo(videoElement){
        var image_data
        var base64
        var c = document.createElement("canvas")
        var img = videoElement
        var width = img.videoWidth
        var height = img.videoHeight
        c.width = width
        c.height = height
        var ctx = c.getContext('2d')
        ctx.drawImage(img, 0, 0,c.width,c.height)
        var base64 = c.toDataURL('image/jpeg')
        var image_data = atob(base64.split(',')[1])
        var arraybuffer = new ArrayBuffer(image_data.length)
        var view = new Uint8Array(arraybuffer)
        for (var i=0; i<image_data.length; i++) {
            view[i] = image_data.charCodeAt(i) & 0xff
        }
        try {
            var blob = new Blob([arraybuffer], {type: 'image/jpeg'})
        } catch (e) {
            var bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder)
            bb.append(arraybuffer)
            var blob = bb.getBlob('image/jpeg')
        }
        return {
            blob: blob,
            buffer: arraybuffer,
            width: width,
            height: height
        }
    }
    async function generateFramesAndLabelsFromVideoOnCanvasAndMemory() {
        doSaveImagesQueue = true;
        const videoName = loadedVideo.name;
        const videoElement = loadedVideo.element[0];
        const width = videoElement.videoWidth;
        const height = videoElement.videoHeight;
        var matrixContainers = getFrameAnnotations(loadedVideo.name);
        var imageContainers = frameBuffersInMemory[loadedVideo.name];
        if (!videoElement.paused) videoElement.pause();

        var markedTimes = getAllSetFrameTimes();

        for (time of markedTimes) {
            if(!doSaveImagesQueue)break;
            var matrices = matrixContainers[time];
            if (matrices) {
                var image = imageContainers[time];
                if (!image) {
                    setVideoTime(time);
                    await new Promise(resolve => setTimeout(resolve, 1000));
                    imageContainers[time] = snapshotVideo(videoElement);
                    image = imageContainers[time];
                }
                var tagNames = getUniqueTags(matrices);
                var filename = `${`${time}`.replace('.','-')}_${tagNames.join('-')}`;
                await saveImage(image.blob,{
                    datasetType: 'flame-video-annotator',
                    datasetName: videoName,
                    filename,
                    matrices: matrices,
                    width: width,
                    height: height,
                });
            }
        }
    }
    function saveMatricesOnTime(currentTimeInt, frameBuffer){
        if(!currentTimeInt){
            var video = matrixEditor.find('.annotation-video')[0]
            currentTimeInt = ensureMultipleOfPointOne(video.currentTime)
        }
        var boundingBoxes = getCurrentPositionOfMatrices().boundingBoxes
        if(frameBuffersInMemory[loadedVideo.name][currentTimeInt])frameBuffersInMemory[loadedVideo.name][currentTimeInt] = frameBuffer || snapshotVideo(video);
        getFrameAnnotations(loadedVideo.name)[currentTimeInt] = boundingBoxes
        frameAnnotationsMadeByHuman[loadedVideo.name][currentTimeInt] = true
        localStorage.setItem('frameAnnotationsMadeByHuman',JSON.stringify(frameAnnotationsMadeByHuman))
        saveFrameAnnotations(loadedVideo.name)
    }
    function moveMatrix(matrix,ratios,matrixSpeed){
        if(!matrix)matrix = {}
        if(!matrix.tag)matrix.tag = ''
        var theMatrix = matrixEditor.find(`[matrix-id="${matrix.id}"]`)
        if(theMatrix.length === 0){
            theMatrix = addNewMatrixToEditor(matrix)
        }else{
            theMatrix.css({
                width: matrix.width / ratios.xRatio,
                height: matrix.height / ratios.yRatio,
                top: matrix.y / ratios.yRatio,
                left: matrix.x / ratios.xRatio
            }).removeClass('border-warning')
        }
    }
    function buildMatrix(matrix){
        if(!matrix)matrix = {}
        if(!matrix.id)matrix.id = generateId(5)
        if(!matrix.width)matrix.width = '100'
        if(!matrix.height)matrix.height = '100'
        // if(!matrix.x)matrix.x = '100'
        // if(!matrix.y)matrix.y = '100'
        if(!matrix.tag)matrix.tag = ''
        var html = `<div matrix-id="${matrix.id}" class='matrix' style="left: ${matrix.x}px; top: ${matrix.y}px; width: ${matrix.width}px; height: ${matrix.height}px;">
                      <div class="handle text-end">
                          <a class="removeMatrixFromAllFrames cursor-pointer"><i class="fa fa-times"></i></a>
                      </div>
                      <input class="form-control object-tag" value="${matrix.tag}" placeholder="Object Tag">
                      <div class='resizers'>
                        <div class='resizer bottom-right'></div>
                      </div>
                    </div>`
        return html
    }
    function activateMatrix(newMatrixElement,matrix,xRatio,yRatio){
        function saveAndResetAppearance() {
            newMatrixElement.removeClass('border-warning')
            saveMatricesOnTime()
        }
        newMatrixElement.click(() => {
            selectMatrix(null, newMatrixElement)
            saveAndResetAppearance()
        })
        newMatrixElement.draggable({
            handle: ".handle",
            containment: "parent",
            start: saveAndResetAppearance,
            stop: function() {
                saveMatricesOnTime()
            }
        })
        newMatrixElement.find('.object-tag').change(function(){
            saveMatricesOnTime()
        })
        if(matrix){
            if(!matrix.width)matrix.width = 100
            if(!matrix.height)matrix.height = 100
            if(!matrix.x)matrix.x = 100
            if(!matrix.y)matrix.y = 100
            if(!matrix.tag)matrix.tag = ''
            newMatrixElement.css({
                width: matrix.width / xRatio + 'px',
                height: matrix.height / yRatio + 'px',
                top: matrix.y / yRatio + 'px',
                left: matrix.x / xRatio + 'px',
            })
        }
        newMatrixElement.resizable({
            containment: "#videoAnnotator .matrix-editor .matrices",
            handles: 'n, e, s, w, ne, nw, se, sw',
            stop: saveAndResetAppearance
        })

    }
    function addNewMatricesToEditor(matrices = []){
        matrices.forEach((matrix) => {
            addNewMatrixToEditor(matrix)
        })
    }
    function addNewMatrixToEditor(options){
        var matrixOptions = Object.assign({
            tag: ''
        },options)
        var matricesContainer = matrixEditor.find('.matrices')
        matricesContainer.append(buildMatrix(matrixOptions))
        var newMatrixElement = matricesContainer.find('.matrix').last()
        activateMatrix(newMatrixElement)
        return newMatrixElement
    }
    function removeMatrixFromEditor(options){
        var matrixOptions = Object.assign({
            tag: ''
        },options)
        var matricesContainer = matrixEditor.find('.matrices')
        matricesContainer.find(`[matrix-id="${matrixOptions.id}"]`).remove()
        saveFrameAnnotations(loadedVideo.name)
    }
    function removeMatrixFromAllFrames(matrixId){
        var matrixEl = matrixEditor.find(`[matrix-id="${matrixId}"]`)
        var video = matrixEditor.find('.annotation-video')[0]
        var annotations = getFrameAnnotations(loadedVideo.name);
        $.each(annotations,function(currentTimeInt,matrices){
            annotations[currentTimeInt] = matrices.filter(matrix => matrix.id !== matrixId)
        })
        matrixEl.remove()
        saveFrameAnnotations(loadedVideo.name)
    }
    function deselectMatrix(){
        matrixEditor.find('.matrix.selected').removeClass('selected')
    }
    function selectMatrix(matrixId, newMatrixElement){
        var matrixEl = newMatrixElement || matrixEditor.find(`[matrix-id="${matrixId}"]`)
        deselectMatrix()
        matrixEl.addClass('selected')
    }
    function playVideo(){
        videoPlaying = true
        var video = matrixEditor.find('.annotation-video')[0]
        playVideoInterval = setInterval(function(){
            playVideoCurrentTime += 0.1
            video.currentTime = playVideoCurrentTime
        },100)
    }
    function pauseVideo(){
        videoPlaying = false
        delete(playVideoInterval)
        clearInterval(playVideoInterval)
    }
    function setVideoTime(time){
        var video = matrixEditor.find('.annotation-video')[0]
        playVideoCurrentTime = time
        video.currentTime = time
    }
    function cacheVideoFrameBuffer(step){
        var videoId = loadedVideo.name;
        var videoEl = loadedVideo.element[0];
        var imageBuffers = frameBuffersInMemory[videoId];
        if(!imageBuffers[step]){
            setVideoTime(step);
            var buffer = snapshotVideo(videoEl);
            imageBuffers[step] = buffer;
        }
        return imageBuffers[step]
    }
    function getNextAvailableFrameAnnotationsStep(videoId, step){
        const videoEl = loadedVideo.element[0];
        const annotations = getFrameAnnotations(videoId);
        const madeByHuman = frameAnnotationsMadeByHuman[videoId];
        const videoLength = videoEl.duration;
        for (let n = step + 0.1; n < videoLength; n += 0.1) {
            n = ensureMultipleOfPointOne(n)
            if(annotations[n]){
                return n
            }
        }
        return null;
    }
    function generateMatricesBetweenSteps(currentStep, futureStep, currentStepAnnotations, futureStepAnnotations, annotations) {
        let step = currentStep;
        const stepSize = 0.1;
        const stepsCount = Math.ceil((futureStep - currentStep) / stepSize);

        const futureAnnotationsMap = new Map();
        futureStepAnnotations.forEach(item => {
            futureAnnotationsMap.set(`${item.id}-${item.tag}`, item);
        });

        for (let i = 0; i < stepsCount; i++) {
            let interpolatedStep = Math.round((currentStep + i * stepSize) * 10) / 10;
            annotations[interpolatedStep] = currentStepAnnotations.map(currentAnnotation => {
                const key = `${currentAnnotation.id}-${currentAnnotation.tag}`;
                const futureAnnotation = futureAnnotationsMap.get(key);

                if (futureAnnotation) {
                    const factor = (interpolatedStep - currentStep) / (futureStep - currentStep);
                    return {
                        id: currentAnnotation.id,
                        tag: currentAnnotation.tag,
                        confidence: currentAnnotation.confidence,
                        x: currentAnnotation.x + factor * (futureAnnotation.x - currentAnnotation.x),
                        y: currentAnnotation.y + factor * (futureAnnotation.y - currentAnnotation.y),
                        width: currentAnnotation.width + factor * (futureAnnotation.width - currentAnnotation.width),
                        height: currentAnnotation.height + factor * (futureAnnotation.height - currentAnnotation.height),
                    };
                } else {
                    return { ...currentAnnotation };
                }
            });
        }

        annotations[futureStep] = futureStepAnnotations;

        return annotations;
    }
    function findPreviousFrameWithManualMatrix(step, useEmptyIfNone){
        var videoId = loadedVideo.name;
        var annotations = getFrameAnnotations(videoId);
        var imageBuffers = frameBuffersInMemory[videoId];
        var foundOne = null;
        var stepBack = step - 1;
        if(annotations[stepBack]){
            foundOne = {
                imageBuffer: imageBuffers[stepBack],
                matrices: annotations[stepBack] || []
            }
        }else if(useEmptyIfNone){
            foundOne = {
                imageBuffer: imageBuffers[0],
                matrices: annotations[0]
            }
        }else{
            for (let i = 0; i < step; i++) {
                if(annotations[i]){
                    foundOne = {
                        imageBuffer: imageBuffers[i],
                        matrices: annotations[i] || []
                    }
                }
            }
        }
        return foundOne;
    }
    async function generateFrameTimesWithNoMatricesIntoMemory(){
        var videoId = loadedVideo.name;
        var videoEl = loadedVideo.element[0];
        var videoLength = videoEl.duration;
        var currentStep = 0;
        var annotations = getFrameAnnotations(videoId);
        var imageBuffers = frameBuffersInMemory[videoId];
        var futureStep = getNextAvailableFrameAnnotationsStep(videoId, currentStep)
        videoEl.currentTime = 0;
        for (let step = 0; step < videoLength; step = ensureMultipleOfPointOne(step + 0.1)) {
            if(stopGeneratingFrames){
                stopGeneratingFrames = false;
                break;
            }
            var nextStep = step + 0.1;
            if(futureStep == step)futureStep = getNextAvailableFrameAnnotationsStep(videoId, step)
            if(futureStep && annotations[futureStep]){
                var frameAnnotations = annotations[step];
                var nextStepAnnotations = annotations[nextStep];
                var futureStepAnnotations = annotations[futureStep];
                var frameBuffer = cacheVideoFrameBuffer(step);
                if(frameAnnotations && !nextStepAnnotations)await generateMatricesBetweenSteps(step, futureStep, frameAnnotations, futureStepAnnotations, annotations);
            }
        }
        saveFrameAnnotations(videoId)
    }
    function stopGenerateFrameTimes(){
        stopGeneratingFrames = true;
    }
    function stopSaveImagesQueue(){
        doSaveImagesQueue = false;
    }
    var mousemoveTimeout = null;
    function closestChildOnMouseMove(event) {
        if(mousemoveTimeout)return;
        mousemoveTimeout = setTimeout(() => {
            mousemoveTimeout = null;
        },500)
        const rect = this.getBoundingClientRect();
        const mouseX = event.clientX - rect.left;
        const mouseY = event.clientY - rect.top;
        let closestElement = null;
        let minDistance = Infinity;

        const children = this.children;
        for (let i = 0; i < children.length; i++) {
            const child = children[i];
            const childRect = child.getBoundingClientRect();
            const offsetX = childRect.left - rect.left;
            const offsetY = childRect.top - rect.top;
            const distance = Math.sqrt(
                Math.pow(mouseX - offsetX, 2) + Math.pow(mouseY - offsetY, 2)
            );

            if (distance < minDistance) {
                minDistance = distance;
                closestElement = child;
            }
        }

        // For demonstration, highlight the closest element
        Array.from(children).forEach(child => child.classList.remove('closest'));
        if (closestElement) {
            closestElement.classList.add('closest');
        }
    }
    function setPlayButtonIcon(doPlay){
        var classButtonAdd = doPlay ? 'btn-warning' : 'btn-primary'
        var classButtonRemove = doPlay ? 'btn-primary' : 'btn-warning'
        var classIconAdd = doPlay ? 'fa-pause' : 'fa-play'
        var classIconRemove = doPlay ? 'fa-play' : 'fa-pause'
        var button = theEnclosure.find('.playPause')
        button.addClass(classButtonAdd).removeClass(classButtonRemove)
        button.find('i').addClass(classIconAdd).removeClass(classIconRemove)
    }
    theEnclosure
        .on('click','.generateMatrices',function(){
            generateFrameTimesWithNoMatricesIntoMemory()
        })
        .on('click','.stopGenerateMatrices',function(){
            stopGenerateFrameTimes()
        })
        .on('click','.stopSavingMatrices',function(){
            stopSaveImagesQueue()
        })
        .on('click','.saveMatrices',function(){
            generateFramesAndLabelsFromVideoOnCanvasAndMemory()
        })
        .on('click','.addMatrix',function(){
            addNewMatrixToEditor()
            saveMatricesOnTime()
        })
        .on('click','.deselectMatrix',function(){
            deselectMatrix()
        })
        .on('click','.removeMatrixFromFrame',function(){
            removeMatrixFromEditor({
              id: $(this).parents('[matrix-id]').attr('matrix-id')
            })
        })
        .on('click','.removeMatrixFromAllFrames',function(){
            var matrixId = $(this).parents('[matrix-id]').attr('matrix-id')
            $.confirm.create({
                title: lang['Delete Matrix'],
                body: lang.confirmDeleteVideoAnnotatorMatrixText,
                clickOptions: {
                    class: 'btn-danger',
                    title: 'Yes',
                },
                clickCallback: async function(){
                    removeMatrixFromAllFrames(matrixId)
                }
            })
        })
        .on('click','.resetVideoTime',function(){
            pauseVideo()
            setVideoTime(0)
            setPlayButtonIcon(false)
        })
        .on('click','.playPause',function(){
            var el = $(this)
            var currentVideo = theEnclosure.find('.matrix-editor .annotation-video')[0]
            if(!videoPlaying){
                setPlayButtonIcon(true)
                videoPlaying = true
                scanFrames()
                playVideo()
            }else{
                setPlayButtonIcon(false)
                videoPlaying = false
                pauseVideo()
            }
        })
        .on('click','.loadVideoByUrl',function(e){
            e.preventDefault()
            var href = $(this).attr('href')
            renderUrlOnToCanvas(href)
            setVideoTime(0)
            return false
        })
        .on('mouseover','.loaded-api-video-row video',function(e){
            var video = $(this)[0]
            if(video.paused)video.play()
        })
        .on('mouseout','.loaded-api-video-row video',function(e){
            var video = $(this)[0]
            if(!video.paused)video.pause()
        })
    uploadElement.change(function() {
        renderVideoOnToCanvas(this.files[0])
    })
    remoteUrlInput.change(function() {
        renderUrlOnToCanvas($(this).val())
    })
    // shinobiUrlInput.change(function() {
    //     var apiUrl = $(this).val()
    //     var el = $('#videoAnnotatorShinobiVideos tbody')
    //     var html = ''
    //     var urlInfo = document.createElement('a')
    //     urlInfo.href = apiUrl
    //     $.ajax({
    //         type: "GET",
    //         url: apiUrl,
    //         error: function(xhr, statusText) {
    //             html += `<tr><td class="text-center">${lang['No Videos Found']}</td></tr>`
    //             el.html(html)
    //         },
    //         success: function(data){
    //             if(data && data.videos){
    //                 $.each(data.videos,function(n,video){
    //                     var videoHref = urlInfo.origin + video.href
    //                     html += `<tr class="loaded-api-video-row">
    //                                 <td><div><small>${video.time}</small></div><div><small>${video.end}</small></div></td>
    //                                 <td class="text-right"><a href="${videoHref}" class="loadVideoByUrl btn btn-sm btn-primary">${lang['Load Video']}</a></td>
    //                              </tr>`
    //                 })
    //             }else{
    //                 html += `<tr><td class="text-center">${lang['No Videos Found']}</td></tr>`
    //             }
    //             el.html(html)
    //         }
    //     })
    // })
    seekBar.on("mouseup", function(e){
        var vid = loadedVideo.element[0]
        var el = $(this)
        var offset = el.offset()
        var left = (e.pageX - offset.left)
        var totalWidth = el.width()
        var percentage = ( left / totalWidth )
        var vidTime = parseFloat((vid.duration * percentage).toFixed(1))
        setVideoTime(vidTime)
        var ratios = getVideoRatios()
        var time = ensureMultipleOfPointOne(vid.currentTime)
        resetMatrixPositions(time, ratios)
    })
    matrixEditor.dblclick(deselectMatrix)
})
