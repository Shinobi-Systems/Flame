function postRequest(thePoint, form){
    return new Promise((resolve) => {
        $.post(`/api/${thePoint}`, form, function(data) {
            resolve(data)
        });
    })
}
function getRequest(thePoint){
    return new Promise((resolve) => {
        $.get(`/api/${thePoint}`, function(data) {
            resolve(data)
        });
    })
}
function imageUpload(thePoint, form){
    return new Promise((resolve, reject) => {
        try{
            $.ajax({
                url: `/api/${thePoint}`,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form,
                type: 'post',
                success: function (data) {
                    resolve(data)
                },
                error: function (err) {
                    console.log('err', err)
                    reject(err);
                }
            });
        }catch(err){
            console.log('err', err)
            reject(err);
        }
    })
}
function generateId(x){
    if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < x; i++ )
        t += p.charAt(Math.floor(Math.random() * p.length));
    return t;
}

function compareArraysOrSplit(param1, param2) {
    if(param2 === undefined){
        return false
    }else if (Array.isArray(param1)) {
        if (!Array.isArray(param2)) {
            param2 = param2.split(',');
        }
        if (param1.length !== param2.length) {
            return false;
        }
        let secondArrayCopy = [...param2];
        for (let item of param1) {
            const index = secondArrayCopy.indexOf(item);
            if (index === -1) {
                return false;
            } else {
                secondArrayCopy.splice(index, 1);
            }
        }
        return true;
    } else {
        return param1 == param2;
    }
}

function downloadFile(uri, name){
    var link = document.createElement("a");
    link.setAttribute('download', name);
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    link.remove();
}

function makeBadge(item,color){
    return `<span class="badge badge-sm bg-${color}">${item}</span>`
}

function dashboardOptions(r,rr,rrr){
    if(!rrr){rrr={};};if(typeof rrr === 'string'){rrr={n:rrr}};if(!rrr.n){rrr.n='FlameOptions_'+location.host}
    ii={o:localStorage.getItem(rrr.n)};try{ii.o=JSON.parse(ii.o)}catch(e){ii.o={}}
    if(!ii.o){ii.o={}}
    if(r&&rr&&!rrr.x){
        ii.o[r]=rr;
    }
    switch(rrr.x){
        case 0:
            delete(ii.o[r])
        break;
        case 1:
            delete(ii.o[r][rr])
        break;
    }
    localStorage.setItem(rrr.n,JSON.stringify(ii.o))
    return ii.o
}

function doSearchFilter(itemsContainerId, itemClass, searchQueryElId){
    var searchQuery = $(searchQueryElId).val();
    var itemsContainer = $(itemsContainerId);
    var items = itemsContainer.find(itemClass);
    if(!searchQuery)return items.show();
    items.each(function(n,v){
        var el = $(v);
        var text = el.html();
        var showIt = text.toLowerCase().indexOf(searchQuery) > -1;
        if(showIt){
            el.show();
        }else{
            el.hide();
        }
    })
}

function doSearchFilterOnArray(theArray, searchQuery){
    if(!searchQuery)return theArray;
    var found = [];
    theArray.forEach(function(item){
        var text = item instanceof Object ? JSON.stringify(item) : item;
        var showIt = text.toLowerCase().indexOf(searchQuery) > -1;
        if(showIt){
            found.push(item)
        }
    });
    return found;
}
function calculateTotalTime(time, epochs, timeMultiplier = 1) {
    const [minutes, seconds] = time.split(':').map(Number);
    const totalSeconds = ((minutes * 60) + seconds) * timeMultiplier;
    const multipliedSeconds = totalSeconds * epochs;
    const days = Math.floor(multipliedSeconds / (24 * 3600));
    const remainderSecondsAfterDays = multipliedSeconds % (24 * 3600);
    const hoursAfterDays = Math.floor(remainderSecondsAfterDays / 3600);
    const remainderSecondsAfterHours = remainderSecondsAfterDays % 3600;
    const minutesAfterHours = Math.floor(remainderSecondsAfterHours / 60);
    const secondsAfterMinutes = remainderSecondsAfterHours % 60;
    const humanString = `${days}:${hoursAfterDays.toString().padStart(2, '0')}:${minutesAfterHours.toString().padStart(2, '0')}:${secondsAfterMinutes.toString().padStart(2, '0')}`;
    return {
        string: humanString,
        seconds: multipliedSeconds,
    }
}
function addTimes(time1, time2) {
    // Split each time string by the colon and convert to numbers
    const [minutes1, seconds1] = time1.split(':').map(Number);
    const [minutes2, seconds2] = time2.split(':').map(Number);

    // Convert both times to total seconds
    const totalSeconds1 = (minutes1 * 60) + seconds1;
    const totalSeconds2 = (minutes2 * 60) + seconds2;

    // Add the total seconds together
    const totalSecondsSum = totalSeconds1 + totalSeconds2;

    // Convert the total sum back into minutes and seconds
    const sumMinutes = Math.floor(totalSecondsSum / 60);
    const sumSeconds = totalSecondsSum % 60;

    // Format and return the result
    // Assuming that seconds are always "00", we simplify the output
    return `${sumMinutes}:00`;
}

function formatTimeHumanReadable(timeString) {
    // Split the input string by colon to get the time components
    const [days, hours, minutes, seconds] = timeString.split(':');

    // Initialize an array to hold the formatted time components
    let timeComponents = [];

    // Add day(s) if more than 0
    if (parseInt(days) > 0) {
        timeComponents.push(`${days} day${days === '1' ? '' : 's'}`);
    }

    // Add hour(s) if more than 0
    if (parseInt(hours) > 0) {
        timeComponents.push(`${hours} hour${hours === '01' ? '' : 's'}`);
    }

    // Add minute(s) if more than 0
    if (parseInt(minutes) > 0) {
        timeComponents.push(`${minutes} minute${minutes === '01' ? '' : 's'}`);
    }

    // Add second(s) if more than 0
    if (parseInt(seconds) > 0) {
        timeComponents.push(`${seconds} second${seconds === '01' ? '' : 's'}`);
    }

    // Join the time components with commas and return the result
    return timeComponents.length > 0 ? timeComponents.join(', ') : 'Calculating...';
}

function getUniqueTags(matrices) {
    const tagsSet = new Set();
    matrices.forEach(matrix => {
        tagsSet.add(matrix.tag);
    });
    return Array.from(tagsSet);
}

function ensureMultipleOfPointOne(number) {
    const roundedNumber = Math.round(number * 10) / 10;
    return roundedNumber;
}
