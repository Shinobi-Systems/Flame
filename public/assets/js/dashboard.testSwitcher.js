$(document).ready(function() {
    const theWindow = $('#testSets');
    const testSetList = $('#testSetList');
    const testSetDisplay = $('#testSetDisplay');
    const testSetLogOutput = $('#testSetLogOutput');
    let currentlyLoaded = { files: [] }
    let openedTabOnce = false;

    function writeToLogOutput(item){
        testSetLogOutput.append(`<div>${item instanceof Object ? JSON.stringify(item,null,3) : item}</div>`)
    }
    async function getTestSets(){
        return (await getRequest(`testSets`)).sets
    }
    async function getTestSet(folderName){
        return (await getRequest(`testSets/${folderName}`)).files
    }
    async function linkTestSetToActive(folderName){
        return await postRequest(`testSets/link`,{
            folderName,
        })
    }
    async function drawTestSets(){
        const folders = await getTestSets();
        var html = ''
        for(folderName of folders){
            html += `<div class="card mb-3" testSet-folder="${folderName}">
                <div class="card-body">
                    <div>${folderName}</div>
                    <div><a testSet-action="view" testSet-name="${folderName}" class="btn bg-success"><i class="fa fa-eye"></i></a></div>
                    <div class="mt-2">
                        <a testSet-action="link" testSet-name="${folderName}" class="btn btn-warning btn-sm">${lang['Link']}</a>
                    </div>
                </div>
            </div>`
        }
        testSetList.html(html)
    }
    async function viewTestSet(folderName){
        let html = ''
        const files = await getTestSet(folderName);
        currentlyLoaded = {
            target: folderName,
            files,
        };

        files.forEach(function(filename){
            const filenameParts = filename.split('.')
            const fileExt = filenameParts[filenameParts.length - 1]
            const filePath = `${folderName}/${filename}`
            const fileUrl = `testSets/${folderName}/${filename}`
            switch(fileExt){
                case'mp4':
                case'avi':
                case'mkv':
                    html += `<div class="col-6 contain-image">
                        <a class="btn btn-sm btn-success" href="${fileUrl}" download><i class="fa fa-download"></i></a>
                        <video src="${fileUrl}" controls autoplay></video>
                    </div>`
                break;
                default:
                    html += `<div class="col-6 p-0 contain-image">
                        <div class="img">
                            <img class="lazyload" data-src="${fileUrl}">
                        </div>
                    </div>`
                break;
            }
        });
        testSetDisplay.html(html)
        lazyload(testSetDisplay.find('img'))
    }
    $('[href="#testSets"]').on('shown.bs.tab', async function(e) {
        if(!openedTabOnce){
            openedTabOnce = true;
            const firstOne = await drawTestSetTypes();
            if(firstOne)await drawTestSets(firstOne);
        }
    });

    $('body')
    .on('click','[testSet-action]',async function(){
        const el = $(this);
        const trainAction = el.attr('testSet-action')
        let response;
        switch(trainAction){
            case'reload':
                await drawTestSets();
            break;
            case'view':
                var parentCard = el.parents('[testSet-folder]')
                var folderName = el.attr('testSet-name')
                testSetList.find('[testSet-folder]').removeClass('bg-primary')
                viewTestSet(folderName)
                parentCard.addClass('bg-primary')
            break;
            case'link':
                var folderName = el.attr('testSet-name')
                response = await linkTestSetToActive(folderName)
                writeToLogOutput(response)
            break;
        }
    });
});
