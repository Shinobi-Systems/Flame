$(document).ready(function(){
    const theEnclosure = $('#datasetViewer')
    const theList = $('#datasetList')
    const theDisplay = $('#datasetDisplay')
    let currentlyLoaded = {files: []}
    let loadedDatasets = [];
    async function writeToDisplay(item){
        theDisplay.append(`<div>${item instanceof Object ? JSON.stringify(item,null,3) : item}</div>`)
    }
    function clearDisplay(){
        theDisplay.empty();
    }
    async function fixAnnotations(){
        const response = await postRequest(`datasets`,{ fixAnnotations: 1 })
        return response;
    }
    async function deleteDataset(className){
        const response = await postRequest(`datasets`,{ delete: 1, class: className })
        return response;
    }
    async function clearDatasets(){
        const response = await postRequest(`datasets`,{ clearAll: 1 })
        return response;
    }
    async function checkDatasets(className){
        const response = await postRequest(`datasets`,{ check: 1, class: className })
        return response;
    }
    async function mergeClassIntoAnother(classToGrab, classToMergeTo){
        const response = await postRequest(`datasets`,{ merge: 1, classToGrab, classToMergeTo })
        return response;
    }
    async function renameClass(currentName, newName){
        const response = await postRequest(`datasets`,{ rename: 1, currentName, newName })
        return response;
    }
    async function getDatasets(){
        const response = await getRequest(`datasets`)
        const folders = response.datasets;
        return folders;
    }
    async function getDatasetFiles(targetFolder){
        const response = await getRequest(`datasets/${targetFolder}`)
        const files = response.files;
        return files;
    }
    async function getDatasetFileJson(targetFolder,imageId){
        const response = await getRequest(`datasets/${targetFolder}/${imageId}`)
        const data = response.data;
        return data;
    }
    async function getActiveDatasetInfo(){
        const response = await getRequest(`datasets/info`)
        const data = response.info;
        return data;
    }
    async function drawDatasets(){
        let html = ''
        const datasets = await getDatasets();
        datasets.forEach(function(item){
            html += `<div class="card mb-3" dataset-folder="${item.name}">
                <div class="card-body">
                    <div>
                        <div class="mb-2"><input dataviewer-rename-class="${item.name}" type="text" class="form-control" placeholder="${item.name}" value="${item.name}"></div>
                        <small>${new Date(item.ctime)}</small>
                    </div>
                    <div>
                        <a dataviewer-action="view" dataset-name="${item.name}" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></a>
                        <a dataviewer-action="check" dataset-name="${item.name}" class="btn btn-warning btn-sm">${lang['Audit']}</a>
                        <div class="dropdown d-inline-block">
                          <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            ${lang['Merge into']}
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            ${datasets.filter(data => item.name !== data.name).map(data => `<li><a href="#" class="dropdown-item" dataviewer-action="merge" classtoget="${item.name}" classtomergeto="${data.name}">${data.name}</a></li>`).join('\n')}
                          </ul>
                        </div>
                        <a dataviewer-action="delete" dataset-name="${item.name}" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>`
        });
        theList.html(html)
        loadedDatasets = (await getActiveDatasetInfo()).classes;
    }
    function rescaleMatrices(matrices, dimensions) {
        const { oldWidth, oldHeight, newWidth, newHeight } = dimensions;

        // Calculate the scaling factors
        const scaleX = newWidth / oldWidth;
        const scaleY = newHeight / oldHeight;

        // Rescale each matrix in the array
        return matrices.map(matrix => {
            return {
                classNumber: matrix.classNumber,
                x: matrix.x * scaleX,
                y: matrix.y * scaleY,
                width: matrix.width * scaleX,
                height: matrix.height * scaleY
            };
        });
    }
    function getAndDrawMatrices(jsonTarget){
        var el = $(`[dataset-image="${jsonTarget}"]`).find('.img');
        var newWidth = el.width();
        var newHeight = el.height();
        $.getJSON(`api/datasets/${jsonTarget}`,function(response){
            const { matrices: rawMatrices, width, height } = response.data;
            console.log('rawMatrices',rawMatrices);
            const matrices = rescaleMatrices(rawMatrices,{
                oldWidth: width,
                oldHeight: height,
                newWidth,
                newHeight
            });
            let html = '';
            matrices.forEach((matrix) => {
                html += `<div class="matrix" style="top:${matrix.y}px;left:${matrix.x}px;width:${matrix.width}px;height:${matrix.height}px;"><span class="tag label label-danger">${loadedDatasets[matrix.classNumber]}</span></div>`
            })
            el.append(html)
        })
    }
    async function viewDataset(targetFolder,page = 1){
        let html = ''
        let jpgFiles = []
        let itemsPerPage = 30
        let slice = (parseInt(page) - 1) * itemsPerPage;
        if(currentlyLoaded.target !== targetFolder){
            jpgFiles = await getDatasetFiles(targetFolder);
            let numerOfPages = jpgFiles.length / itemsPerPage;
            numerOfPages = numerOfPages < 1 ? 1 : numerOfPages;
            currentlyLoaded = {
                target: targetFolder,
                files: jpgFiles,
                page,
                pages: numerOfPages,
                slice,
            };
        }else{
            currentlyLoaded.page = page;
        }

        let selectedVideos = currentlyLoaded.files.slice(slice,slice + itemsPerPage);
        selectedVideos.forEach(function(item){
            const imageName = item.name.replace(/\.\w+$/, '');
            const jsonTarget = `${targetFolder}/${imageName}`
            html += `<div class="col-3 p-0 contain-image dataset-block" dataset-image="${jsonTarget}">
                <div class="img">
                    <img class="lazyload" data-src="datasets/images/${targetFolder}/${item.name}" data-json="${jsonTarget}">
                </div>
            </div>`
        });
        theDisplay.html(html)
        var imageEls = theDisplay.find('img')
        imageEls.each(function(n,v){
            var el = $(v);
            var jsonTarget = el.attr('data-json');
            el[0].onload = function(){
                getAndDrawMatrices(jsonTarget)
            }
        });
        lazyload(imageEls)
    }
    theEnclosure.on('change','[dataviewer-rename-class]',async function(e){
        var el = $(this);
        var currentName = el.attr('dataviewer-rename-class')
        var newName = el.val();
        await renameClass(currentName, newName);
        el.attr('dataviewer-rename-class', newName)
        theEnclosure.find(`[dataset-folder="${currentName}"]`).attr('dataset-folder', newName)
        theEnclosure.find(`[dataset-name="${currentName}"]`).attr('dataset-name', newName)
    })
    theEnclosure.on('click','[dataviewer-action]',async function(e){
        var el = $(this)
        var theAction = el.attr('dataviewer-action')
        switch(theAction){
            case'merge':
                $.confirm.create({
                    title: lang['Merge Datasets'],
                    body: lang.confirmMergeDatasetText,
                    clickOptions: {
                        class: 'btn-warning',
                        title: 'Yes',
                    },
                    clickCallback: async function(){
                        clearDisplay();
                        var classToGrab = el.attr('classtoget')
                        var classToMergeTo = el.attr('classtomergeto')
                        var response = await mergeClassIntoAnother(classToGrab,classToMergeTo)
                        writeToDisplay(response)
                        drawDatasets()
                    }
                })
            break;
            case'check':
                e.stopPropagation()
                clearDisplay();
                var className = el.attr('dataset-name')
                var response = await checkDatasets(className)
                writeToDisplay(response)
            break;
            case'clearDatasets':
                $.confirm.create({
                    title: lang['Clear Datasets'],
                    body: lang.confirmClearDatasetText,
                    clickOptions: {
                        class: 'btn-danger',
                        title: 'Yes',
                    },
                    clickCallback: function(){
                        clearDatasets();
                        clearDisplay();
                    }
                })
            break;
            case'delete':
                var className = el.attr('dataset-name')
                $.confirm.create({
                    title: lang['Delete Dataset'],
                    body: lang.confirmDeleteDatasetText,
                    clickOptions: {
                        class: 'btn-danger',
                        title: 'Yes',
                    },
                    clickCallback: async function(){
                        var response = await deleteDataset(className);
                        theEnclosure.find(`[dataset-folder="${className}"]`).remove();
                        writeToDisplay(response);
                    }
                })
            break;
            case'fixAnnotations':
                clearDisplay();
                var response = await fixAnnotations();
                writeToDisplay(response)
            break;
            case'refreshLists':
                drawDatasets();
            break;
            case'view':
                var parentCard = el.parents('[dataset-folder]')
                var targetFolder = el.attr('dataset-name')
                theEnclosure.find('[dataset-folder]').removeClass('bg-primary')
                viewDataset(targetFolder)
                parentCard.addClass('bg-primary')
            break;
        }
    });
    $('[href="#datasetViewer"]')
        .on('shown.bs.tab', async function(e) {
            drawDatasets()
        })
})
