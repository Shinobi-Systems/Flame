$(document).ready(function() {
    const customDatasets = $('#customDatasets');
    const customDatasetList = $('#customDatasetList');
    const customDatasetDisplay = $('#customDatasetDisplay');
    const customDatasetLogOutput = $('#customDatasetLogOutput');
    const customDatasetsDownloadForm = $('#customDatasetsDownloadForm');
    const customDatasetForm = $('#customDatasetForm');
    const customDatasetsRecentDownloads = $('#customDatasetsRecentDownloads');
    const customDatasetSearchLoaded = $('#customDatasetSearchLoaded');
    const customDatasetTypes = customDatasetForm.find('[name="types"]');
    let currentlyLoaded = {files: []}
    let loadedClassNames = {};
    let openedTabOnce = false;

    function imageUrlToLabelUrl(url) {
        const urlObj = `${url}`.replace('/images/', '/labels/').replace(/(\.[^/.]+)$/, ".txt");
        return urlObj;
    }

    function rescaleMatrices(matrices, dimensions) {
        const { oldWidth, oldHeight, newWidth, newHeight } = dimensions;

        // Calculate the scaling factors
        const scaleX = newWidth / oldWidth;
        const scaleY = newHeight / oldHeight;

        // Rescale each matrix in the array
        return matrices.map(matrix => {
            return {
                classNumber: matrix.classNumber,
                x: matrix.x * scaleX,
                y: matrix.y * scaleY,
                width: matrix.width * scaleX,
                height: matrix.height * scaleY
            };
        });
    }

    function getRecentDownloads(){
        const recentDownloads = dashboardOptions().customDatasetsRecentDownloads || [];
        return recentDownloads
    }
    function addToRecentDownoads(item){
        const recentDownloads = getRecentDownloads();
        item.time = new Date();
        recentDownloads.push(item);
        if(recentDownloads.length > 15)recentDownloads.shift();
        dashboardOptions('customDatasetsRecentDownloads',recentDownloads)
    }
    function loadRecentDownloads(item){
        const recentDownloads = getRecentDownloads().reverse();
        var html = recentDownloads.map(item => `<li><a href="#" class="dropdown-item" customDataset-action="redownload" data-url="${item.url}" data-type="${item.type}"><small>${item.type}</small><br><small class="text-mute">${item.time}</small><br>${item.url}</a></li>`);
        customDatasetsRecentDownloads.html(html);
    }
    function writeToLogOutput(item){
        customDatasetLogOutput.append(`<div>${item instanceof Object ? JSON.stringify(item,null,3) : item}</div>`)
    }
    async function getCustomDatasetTypes(){
        return (await getRequest(`customDatasets/types`)).types
    }
    async function getCustomDatasets(datasetType){
        console.log('go',datasetType)
        return (await getRequest(`customDatasets/data/${datasetType}`)).datasets
    }
    async function getCustomDatasetInfo(datasetType, datasetName){
        return (await getRequest(`customDatasets/data/${datasetType}/${datasetName}`)).info;
    }
    async function getCustomDatasetFiles(datasetType, datasetName){
        return (await getRequest(`customDatasets/data/${datasetType}/${datasetName}?files=1`)).files;
    }
    async function linkCustomDatasetToActive(datasetType, datasetName, {
        classes,
    } = {}){
        return await postRequest(`customDatasets/link`,{
            type: datasetType,
            name: datasetName,
            classes,
        })
    }
    async function drawCustomDatasetTypes(preSelected){
        const types = await getCustomDatasetTypes();
        var html = ''
        var firstOne = null;
        $.each(types,function(n, type){
            const isFirst = preSelected === type || !preSelected && n === 0;
            if(isFirst)firstOne = type;
            html += `<option ${isFirst ? 'selected' : ''} value="${type}">${type}</option>`
        });
        customDatasetTypes.html(html)
        return firstOne;
    }
    async function drawCustomDatasets(datasetType){
        const datasetNames = await getCustomDatasets(datasetType);
        var html = ''
        for(datasetName of datasetNames){
            const info = await getCustomDatasetInfo(datasetType,datasetName)
            html += `<div class="card mb-3" dataset-folder="${datasetName}">
                <div class="card-body">
                    <div>${datasetName}</div>
                    <div><p><small>${info.link}</small></p></div>
                    <div><small class="badge bg-primary">${info.modelLicense}</small></div>
                    <div><a customDataset-action="view" dataset-type="${datasetType}" dataset-name="${datasetName}" dataset-classes="${info.classes.join(',')}" class="btn bg-success">${info.classes.join(', ')}</a></div>
                    <div class="mt-2">
                        <a customDataset-action="link" dataset-name="${datasetName}" class="btn btn-warning btn-sm">${lang['Link']}</a>
                        <a href="${info.link}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-home"></i></a>
                        <a href="${info.downloadUrl}" download class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>`
        }
        customDatasetList.html(html)
    }
    async function downloadDataset(type, url){
        const response = await postRequest(`customDatasets/download`,{
            type,
            url
        });
        if(response.ok){
            addToRecentDownoads({type, url});
        }
        return response;
    }
    function filterLoadedDatasets(){
        doSearchFilter('#customDatasetList', '[dataset-folder]','#customDatasetSearchLoaded')
    }
    function getAndDrawMatrices(datasetType, datasetName, imagePath){
        var el = $(`[dataset-image="${imagePath}"]`).find('.img');
        var newWidth = el.width();
        var newHeight = el.height();
        $.getJSON(`api/customDatasets/label/${datasetType}/${datasetName}?path=${imageUrlToLabelUrl(imagePath)}`,function(response){
            const { matrices: rawMatrices, width, height } = response.data;
            console.log('rawMatrices',rawMatrices);
            const matrices = rescaleMatrices(rawMatrices,{
                oldWidth: width,
                oldHeight: height,
                newWidth,
                newHeight
            });
            let html = '';
            matrices.forEach((matrix) => {
                html += `<div class="matrix" style="top:${matrix.y}px;left:${matrix.x}px;width:${matrix.width}px;height:${matrix.height}px;"><span class="tag label label-danger">${loadedClassNames[matrix.classNumber]}</span></div>`
            })
            el.append(html)
        })
    }
    async function viewDataset(datasetType,datasetName,page = 1){
        let html = ''
        let jpgFiles = []
        let itemsPerPage = 30
        let slice = (parseInt(page) - 1) * itemsPerPage;
        let targetFolder = `${datasetType}/${datasetName}`
        if(currentlyLoaded.target !== targetFolder){
            jpgFiles = await getCustomDatasetFiles(datasetType,datasetName);
            let numerOfPages = jpgFiles.length / itemsPerPage;
            numerOfPages = numerOfPages < 1 ? 1 : numerOfPages;
            currentlyLoaded = {
                target: targetFolder,
                files: jpgFiles,
                page,
                pages: numerOfPages,
                slice,
            };
        }else{
            currentlyLoaded.page = page;
        }

        let selectedVideos = currentlyLoaded.files.slice(slice,slice + itemsPerPage);
        selectedVideos.forEach(function(imagePath){
            const jsonTarget = `${targetFolder}/${imagePath}`
            html += `<div class="col-3 p-0 contain-image dataset-block" dataset-image="${jsonTarget}">
                <div class="img">
                    <img class="lazyload" data-src="customDatasets/${targetFolder}/${imagePath}" data-json="${jsonTarget}">
                </div>
            </div>`
        });
        customDatasetDisplay.html(html)
        var imageEls = customDatasetDisplay.find('img')
        imageEls.each(function(n,v){
            var el = $(v);
            var jsonTarget = el.attr('data-json');
            el[0].onload = function(){
                getAndDrawMatrices(datasetType, datasetName, jsonTarget)
            }
        });
        lazyload(imageEls)
    }
    $('[href="#customDatasets"]').on('shown.bs.tab', async function(e) {
        if(!openedTabOnce){
            openedTabOnce = true;
            const firstOne = await drawCustomDatasetTypes();
            if(firstOne)await drawCustomDatasets(firstOne);
        }
    });
    customDatasetsDownloadForm.submit(async function(e){
        e.preventDefault();
        const { type, url } = customDatasetsDownloadForm.serializeObject();
        writeToLogOutput(`Downloading... <b>${url}</b>`)
        const response = await downloadDataset(type, url);
        if(response.ok){
            await drawCustomDatasetTypes(type);
            await drawCustomDatasets(type);
            loadRecentDownloads()
            // customDatasetSearchLoaded.val(url).trigger('keyup')
            writeToLogOutput(`Downloaded! <b>${url}</b>`)
        }else{
            writeToLogOutput(`Failed! <b>${url}</b>`)
            writeToLogOutput(response);
        }
        return false;
    });

    customDatasetSearchLoaded
    .on('keyup',async function(){
        filterLoadedDatasets();
    });
    customDatasets
    .on('change','[name="types"]',async function(){
        const selectedType = $(this).val()
        await drawCustomDatasets(selectedType);
    })
    .on('change','[name="names"]',async function(){
        const selectedSets = $(this).val()
        console.log(selectedSets)
    });
    $('body')
    .on('click','[customDataset-action]',async function(){
        const el = $(this);
        const trainAction = el.attr('customDataset-action')
        let response;
        switch(trainAction){
            case'redownload':
                const type = el.attr('data-type');
                const url = el.attr('data-url');
                customDatasetsDownloadForm.find('[name="type"]').val(type);
                customDatasetsDownloadForm.find('[name="url"]').val(url);
                // customDatasetsDownloadForm.submit();
            break;
            case'reload':
                const firstOne = await drawCustomDatasetTypes();
                console.log(firstOne)
                if(firstOne)await drawCustomDatasets(firstOne);
            break;
            case'view':
                var parentCard = el.parents('[dataset-folder]')
                var datasetType = el.attr('dataset-type')
                var datasetName = el.attr('dataset-name')
                loadedClassNames = el.attr('dataset-classes').split(',')
                customDatasetList.find('[dataset-folder]').removeClass('bg-primary')
                viewDataset(datasetType,datasetName)
                parentCard.addClass('bg-primary')
            break;
            case'link':
                var datasetName = el.attr('dataset-name')
                var datasetType = customDatasetTypes.val();
                response = await linkCustomDatasetToActive(datasetType, datasetName)
                writeToLogOutput(response)
            break;
        }
    });
    loadRecentDownloads();
});
