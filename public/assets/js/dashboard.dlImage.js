const dlImageProgress = $('#dlImageProgress .progress-bar');
const dlImageProgressText = $('#dlImageProgress .progress-bar-text');
const dlImageResults = $('#dlImageResults');
const dlImageTemp = $('#dlImageTemp');
const dlImageForm = $('#dlImageForm');
function loadDLImageFormMemory(){
    var optionsInMemory = JSON.parse(localStorage.getItem('dlImageForm') || '{}')
    $.each(optionsInMemory,function(fieldName,value){
        dlImageForm.find(`[name="${fieldName}"]`).val(value)
    })
}
function getImageDownloaderSelectedClasses(){
    const dlOptions = dlImageForm.serializeObject()
    const classes = dlOptions.className.split(',');
    const relationships = dlOptions.relationships.split(',');
    return {
        classes,
        relationships,
    }
}
async function startImageDownloader(){
    const dlOptions = dlImageForm.serializeObject()
    response = await postRequest(`dlImage/start`,dlOptions)
    writeToResultsPane(response, dlImageResults)
}
async function stopImageDownloader(){
    response = await postRequest(`dlImage/stop`)
    writeToResultsPane(response, dlImageResults)
}
async function linkImageDownloaderDataToActive(){
    response = await postRequest(`dlImage/link`,getImageDownloaderSelectedClasses());
    writeToResultsPane(response, dlImageResults)
}

$(document).ready(function() {
    $('body').on('click','[dlimage-action]',async function(){
        const trainAction = $(this).attr('dlimage-action')
        let response;
        switch(trainAction){
            case'link':
                $.confirm.create({
                    title: lang['Link for Training'],
                    body: `${lang.confirmLinkingSelectedDatasets}
<div>${getImageDownloaderSelectedClasses().map(item => `<div><b>${item}</b></div>`)}</div>`,
                    clickOptions: {
                        class: 'btn-success',
                        title: 'Yes',
                    },
                    clickCallback: function(){
                        linkImageDownloaderDataToActive()
                    }
                })
            break;
            case'clear':
                dlImageResults.empty()
            break;
            case'start':
                startImageDownloader()
            break;
            case'stop':
                stopImageDownloader()
            break;
        }
    });
});
