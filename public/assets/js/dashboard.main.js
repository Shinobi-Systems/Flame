const trainForm = $('#trainForm');
const trainResults = $('#trainResults');
const trainResultsTemp = $('#trainResultsTemp');
const trainMetrics1 = $('#trainMetrics1');
const trainMetrics2 = $('#trainMetrics2');
const trainMetrics3 = $('#trainMetrics3');
const trainResultsProgress = $('#trainResultsProgress .progress-bar');
const trainResultsProgressText = $('#trainResultsProgress .progress-bar-text');
const trainResultsPartProgress = $('#trainResultsPartProgress .progress-bar');
const trainResultsPartProgressText = $('#trainResultsPartProgress .progress-bar-text');
let doTrailing = true;
let trainingMetrics = 0;
let trainSummaryDrawTimeout = null;
let mostRecent = {
    expectedRunTime: '00:00',
    progressTimeEstimate: '00:00',
    summaryTimeEstimate: '00:00',
};
function writeToResultsPane(msg,chosenPane,overwrite,isRow,forceToTop){
    const pane = chosenPane[0]
    const data = typeof msg === 'object' ? JSON.stringify(msg,null,3) : msg.trim();
    chosenPane[overwrite ? 'html' : 'append'](isRow ? `<div class="row m-0 px-3 pt-3">${data}</div>` : '<pre class="px-3 pt-3"><code>' + data + '</code></pre>');
    if(doTrailing)pane.scrollTop = forceToTop ? 0 : pane.scrollHeight;
}

async function loadSelectableGpus(){
    var trainFormOptionsInMemory = JSON.parse(localStorage.getItem('trainForm') || '{}')
    const gpus = (await getSystemInfo('gpu')).data.gpus;
    var html = ''
    $.each(gpus,function(id,gpuInfo){
        html += `<option ${id == 0 ? 'selected' : ''} value="${id}">(${id}) ${gpuInfo.name}</option>`
    })
    trainForm.find('[name="device"]').html(html)
}

function loadTrainFormMemory(){
    var trainFormOptionsInMemory = JSON.parse(localStorage.getItem('trainForm') || '{}')
    $.each(trainFormOptionsInMemory,function(fieldName,value){
        trainForm.find(`[name="${fieldName}"]`).val(value)
    })
}

function buildImageHtml(url,size = '6'){
    return `<div class="col-${size} mb-3"><div class="mb-2"><small>${url}</small></div><img src="${url}" style="width: 100%;"></div>`
}

function drawBarToMetrics(metrics){
    function makeBar(barVal,color){
        return `<div title="${barVal}" class="train-metric-bar-holder col p-0">
            <div class="train-metric-bar" style="bottom:${barVal * 1000}%;border-color:${color}"></div>
        </div>`
    }
    trainMetrics1.append(makeBar(metrics[0],'#ff0000'))
    trainMetrics2.append(makeBar(metrics[1],'#1F51FF'))
    trainMetrics3.append(makeBar(metrics[2],'#00ff00'))
}

function removeOldestMetricBar(metrics){
    [
        trainMetrics1,
        trainMetrics2,
        trainMetrics3,
    ].forEach((theBlock) => {
        theBlock.children().eq(0).remove()
    })
}

function staleColorMetricBar(){
    [
        trainMetrics1,
        trainMetrics2,
        trainMetrics3,
    ].forEach((theBlock) => {
        theBlock.find('.train-metric-bar').css('border-color','#d7d7d7')
    })
}

async function drawLatestTrainSummary(forceDraw){
    // prevent overflow
    if(trainSummaryDrawTimeout && !forceDraw)return;
    clearTimeout(trainSummaryDrawTimeout);
    trainSummaryDrawTimeout = setTimeout(() => {
        trainSummaryDrawTimeout = null;
    },5000);

    //
    var html = `<div><h3>${new Date()}</h3></div>`
    const expFolder = (await getRequest(`train/newest/result`)).exp;
    const response = await getRequest(`train/result/${expFolder}`);
    response.files.reverse().filter(item => item.endsWith('.jpg')).forEach((image) => {
        html += buildImageHtml(`yolov5/runs/train/${expFolder}/${image}`,'12')
    })
    writeToResultsPane(html, trainResults, true, true, true)
}

$(document).ready(function() {
    var socket = io({
        transports: ['websocket']
    });
    socket.on('connect',function(){
        console.log(`Connected`,socket.id)
    });
    [
        'message',
    ].forEach((handler) => {
        socket.on(handler, function(msg) {
            writeToResultsPane(msg.data ? msg.data : msg, trainResults)
        });
    });
    socket.on('trainTest', function(msg) {
        console.log('trainTest',msg)
        switch(msg.msg){
            case'end':
                const data = msg.data;
                var html = ''
                var expFolder = data.exp;
                data.images.forEach((image) => {
                    html += buildImageHtml(`yolov5/runs/detect/${expFolder}/${image}`)
                })
                writeToResultsPane(html, trainResults)
            break;
            default:
                writeToResultsPane(msg.data ? msg.data : msg, trainResults)
            break;
        }
    });
    socket.on('formInput', function({
        formName,
        key,
        value,
        senderId
    }) {
        if(senderId === socket.id)return;
        const theForm = $(`#${formName}`)
        theForm.find(`.form-control[name="${key}"]`).val(value)
    });
    socket.on('lastTrainSettings',function(lastTrainSettings){
        console.log(lastTrainSettings)
        let changesToForm = false;
        var currentForm = JSON.parse(localStorage.getItem('trainForm') || '{}')
        $.each(lastTrainSettings,function(key,value){
            var isSame = compareArraysOrSplit(value,currentForm[key])
            if(!isSame){
                console.log('new vertiform.',key,' last is ',value,' but you have ',currentForm[key])
                changesToForm = true;
            }
        });
        if(changesToForm){
            $.confirm.create({
                title: `Use Last Training Settings?`,
                body: `Another client was used to start a training process. Do you want to overwrite your settings to match the latest session?`,
                clickOptions: {
                    class: 'btn-success',
                    title: 'Yes',
                },
                clickCallback: function(){
                    $.each(lastTrainSettings,function(key,value){
                        trainForm.find(`.form-control[name="${key}"]`).val(value)
                    });
                    trainForm.find(`.form-control`).first().change()
                }
            })
        }
    });
    socket.on('train', function(msg) {
        switch(msg.msg){
            case'end':
                window.trainStartedOnThisPageLoad = false;
            break;
            case'progress':
                trainResultsProgress.css('width',`${msg.progress}%`)
                trainResultsProgressText.text(`${msg.progress}%, ${msg.creationSpeed}ms, ${msg.on} / ${msg.of}, ${msg.id}`);
                writeToResultsPane(msg, dlImageTemp, true)
            break;
            case'stderr':
                const data = msg.data;
                const isProgress = data.type === 'progress'
                const isSummary = data.type === 'summary'
                const isSummaryOrProgress = isProgress || isSummary
                const totalPossibleTimeForActivity = addTimes(data.elapsedTime, data.remainingTime);
                if(isSummary){
                    mostRecent.summaryTimeElapsed = data.elapsedTime;
                    mostRecent.summaryTimeEstimate = totalPossibleTimeForActivity;
                }
                if(isProgress){
                    mostRecent.progressTimeElapsed = data.elapsedTime;
                    mostRecent.progressTimeEstimate = totalPossibleTimeForActivity;
                    mostRecent.epoch = data.epoch;
                    mostRecent.epochs = data.totalEpochs;
                }
                const totalPossibleTimeForTraining = addTimes(mostRecent.progressTimeEstimate, mostRecent.summaryTimeEstimate);
                if(isSummaryOrProgress){
                    mostRecent.elapsedRunTime = formatTimeHumanReadable(calculateTotalTime(totalPossibleTimeForTraining, mostRecent.epoch).string);
                    mostRecent.expectedRunTime = formatTimeHumanReadable(calculateTotalTime(totalPossibleTimeForTraining, mostRecent.epochs).string);
                    trainResultsPartProgress.css('width',`${data.progress}%`)
                    trainResultsPartProgressText.text(`${data.elapsedTime} / ${data.remainingTime}, (${mostRecent.elapsedRunTime} / ${mostRecent.expectedRunTime})`)
                    writeToResultsPane(data, trainResultsTemp, true)
                }
                if(isSummary){
                    if(data.progress < 5){
                        staleColorMetricBar();
                    }else if(data.currentStep === data.totalSteps){
                        drawLatestTrainSummary();
                    }
                }
                if(isProgress){
                    const overallPercent = calculateTrainOverallProgress(data)
                    trainResultsProgress.css('width',`${overallPercent}`)
                    trainResultsProgressText.text(`${data.epoch} / ${data.totalEpochs}, ${data.memory}, ${data.itPerS}`);
                    if(data.metrics){
                        const metrics = data.metrics;
                        ++trainingMetrics;
                        drawBarToMetrics(metrics)
                        if(trainingMetrics >= 200){
                            --trainingMetrics;
                            removeOldestMetricBar()
                        }
                    }
                }else if(!isSummary){
                    writeToResultsPane(data, trainResults)
                }
            break;
            default:
                writeToResultsPane(msg.data ? msg.data : msg, trainResults)
            break;
        }
    });


    socket.on('openImageDownloader', function(msg) {
        switch(msg.msg){
            case'progress':
                dlImageProgress.css('width',`${msg.progress}%`)
                dlImageProgressText.text(`${msg.progress}%, ${msg.creationSpeed}ms, ${msg.on} / ${msg.of}, ${msg.id}`);
                writeToResultsPane(msg, dlImageTemp, true)
            break;
            default:
                writeToResultsPane(msg.data ? msg.data : msg, dlImageResults)
            break;
        }
    });

    $('[train-action]').click(async function(){
        const trainAction = $(this).attr('train-action')
        let response;
        switch(trainAction){
            case'clear':
                trainResults.empty()
            break;
            case'start':
                var epochs = parseInt(trainForm.find(`[name="epochs"]`).val()) || 3;
                mostRecent.epochs = epochs;
                trainForm.find(`[name="resume"]`).val('')
                response = await startTraining()
                writeToResultsPane(response, trainResults)
                window.trainStartedOnThisPageLoad = true;
            break;
            case'resume':
                response = await startTraining()
                writeToResultsPane(response, trainResults)
            break;
            case'stop':
                response = await postRequest(`train/stop`)
                writeToResultsPane(response, trainResults)
                window.trainStartedOnThisPageLoad = false;
            break;
            case'getSummary':
                drawLatestTrainSummary(true);
            break;
            case'testLatest':
                response = await postRequest(`train/test`, { weightsPath: 'latest' })
            break;
            case'trailToggle':
                doTrailing = !doTrailing;
            break;
        }
    });


    [
        'trainForm',
        'dlImageForm'
    ].forEach((formName) => {
        const theForm = $(`#${formName}`)
        theForm.find('.form-control').change(function(){
            const el = $(this)
            const key = el.attr('name')
            const value = el.val()
            const options = theForm.serializeObject()
            localStorage.setItem(formName,JSON.stringify(options))
            socket.emit('formInput',{
                formName,
                key,
                value,
                senderId: socket.id
            })
        });
    })

    // $('[name="maxImages"]').change(function(){
    //     const numberOfImages = parseInt($(this).val())
    //     const epochs = suggestEpochs(numberOfImages)
    //     console.log(epochs)
    //     $('[name="epochs"]').val(epochs).change()
    // })

    loadSelectableGpus().then(() => {
        loadTrainFormMemory()
        loadDLImageFormMemory()
    })
});
