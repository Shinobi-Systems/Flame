async function getSystemInfo(type = 'gpu'){
    const response = await getRequest(`system/${type}`)
    return response;
}
$(document).ready(function(){
    const theEnclosure = $('#systemStatus')
    const systemStatusGpu = $('#systemStatusGpu')
    const systemStatusDisplay = $('#systemStatusDisplay')
    const gpuProgressBars = {}
    let gpuBarsLoaded = false;
    let autoReloadTimer = null;
    async function drawSystemInfo(){
        let html = ''
        const gpu = (await getSystemInfo('gpu')).data;
        const ram = (await getSystemInfo('ram')).data;
        if(!gpuBarsLoaded){
            gpuBarsLoaded = true;
            html += buildGPUData(gpu);
            systemStatusGpu.html(html);
            loadGpuProgressBars(gpu);
        }else{
            setGpuData(gpu)
        }
        systemStatusDisplay.html(buildMemoryData(ram))
    }
    function generateProgressBar(label, percent, additionalText = '') {
        return `
            <div data-bar="${label}" class="mb-3">
                <p><strong>${label}:</strong> <span class="percent">${percent}</span>% <span class="postText">${additionalText}</span></p>
                <div class="progress">
                    <div class="progress-bar-text"><span class="percent">${percent}</span>%</div>
                    <div class="progress-bar" role="progressbar" style="width: ${percent}%" aria-valuenow="${percent}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>`;
    }
    function generateRegularRow({
        label, text, parameter, additionalText = ''
    }) {
        return `
            <div data-bar="${parameter}" class="mb-3">
                <p><strong>${label}:</strong> <span class="value">${text}</span> <span class="postText">${additionalText}</span></p>
            </div>`;
    }
    function getPercents(gpu){
        const gpuUtilPercent = parseInt(gpu.gpu_util);
        const fanSpeedPercent = parseInt(gpu.fan_speed);
        const memoryUsedPercent = parseInt((parseFloat(gpu.memory_usage.used[0]) / parseFloat(gpu.memory_usage.total[0])) * 100);
        const powerUsagePercent = parseInt((parseFloat(gpu.power_usage) / parseFloat(gpu.power_cap)) * 100);
        return {
            'Fan Speed': fanSpeedPercent,
            'Power Usage': powerUsagePercent,
            'Memory Usage': memoryUsedPercent,
            'GPU Utilization': gpuUtilPercent
        }
    }
    function getOtherInfo(gpu){
        return {
            Temperature: {label: "Temperature", text: gpu.temp, parameter: "gpu.temp"}
        }
    }
    function getPostTexts(gpu){
        return {
            'Power Usage': `${gpu.power_usage} of ${gpu.power_cap}`,
        }
    }
    function buildGPUData(data) {
        const numGPUs = data.gpus.length;

        let htmlContent = `<h2 class="text-center mb-4">Driver Version: ${data.driverVersion.join(', ')}</h2>
                           <div class="row">`;

        // Generate cards for each GPU
        data.gpus.forEach((gpu, index) => {
            const percents = getPercents(gpu);
            const otherInfo = getOtherInfo(gpu);
            const postTexts = getPostTexts(gpu);
            let percentHtml = ''
            let otherHtml = ''
            $.each(percents,(labelName, value) => {
                percentHtml += generateProgressBar(labelName, value, postTexts[labelName])
            });
            $.each(otherInfo,(label, item) => {
                otherHtml += generateRegularRow(item)
            })
            htmlContent += `
                <div data-gpu="${index}" class="col-md-${12 / numGPUs}">
                    <div class="card mb-3">
                        <div class="card-header gpu-header">
                            GPU ${index + 1}: ${gpu.name}
                        </div>
                        <div class="card-body">
                            <p><strong>ID:</strong> ${gpu.id}</p>
                            ${otherHtml}
                            ${percentHtml}
                        </div>
                    </div>
                </div>`;
        });

        htmlContent += `</div>`; // Close the row
        return htmlContent;
    }
    function loadGpuProgressBars(data){
        data.gpus.forEach((gpu,n) => {
            const gpuEl = $(`[data-gpu="${n}"]`);
            const newBars = {id: data.id, el: gpuEl}
            const percents = getPercents(gpu);
            const otherInfo = getOtherInfo(gpu);
            $.each(otherInfo,(label, item) => {
                const el = gpuEl.find(`[data-bar="${label}"]`);
                const valueEl = el.find('.text')
                newBars[label] = {
                    text: function(text){
                        valueEl.text(text)
                    }
                }
            })
            $.each(percents,(labelName, value) => {
                const el = gpuEl.find(`[data-bar="${labelName}"]`);
                const percentEls = el.find('.percent')
                const progessBar = el.find('.progress-bar')
                const progessBarText = el.find('.progress-bar-text')
                const postText = el.find('.postText')
                newBars[labelName] = {
                    percent: function(percent){
                        progessBar.css('width',`${percent}%`)
                        percentEls.text(`${percent}%`)
                    },
                    text: function(text){
                        postText.text(text)
                    }
                }
            });
            gpuProgressBars[n] = newBars;
        })
    }
    function setGpuData(data){
        console.log('Set data')
        data.gpus.forEach((gpu,n) => {
            const theBars = gpuProgressBars[n];
            const percents = getPercents(gpu);
            const otherInfo = getOtherInfo(gpu);
            const postTexts = getPostTexts(gpu);
            $.each(percents,(labelName, value) => {
                const postText = postTexts[labelName];
                theBars[labelName].percent(value)
                if(postText)theBars[labelName].text(postText)
            })
            $.each(otherInfo,(labelName, item) => {
                theBars[labelName].text(item.text)
            })
        })
    }
    function buildMemoryData(memoryData) {
        const memoryPercent = calculatePercentage(memoryData.memory.used, memoryData.memory.total);
        const swapPercent = memoryData.swap.total > 0 ? calculatePercentage(memoryData.swap.used, memoryData.swap.total) : 0;
        let htmlContent = `<h2 class="text-center mb-4">System Memory and Swap Usage</h2>
                           <div class="row">
                               <div class="col-md-6">
                                   ${generateProgressBar('Memory Usage', memoryPercent, `${formatMemory(memoryData.memory.used)} of ${formatMemory(memoryData.memory.total)}`)}
                                   <p>Free: ${formatMemory(memoryData.memory.free)}, Shared: ${formatMemory(memoryData.memory.shared)}, Buff/Cache: ${formatMemory(memoryData.memory.buffCache)}, Available: ${formatMemory(memoryData.memory.available)}</p>
                               </div>
                               <div class="col-md-6">
                                   ${generateProgressBar('Swap Usage', swapPercent, `${formatMemory(memoryData.swap.used)} of ${formatMemory(memoryData.swap.total)}`)}
                                   <p>Free: ${formatMemory(memoryData.swap.free)}</p>
                               </div>
                           </div>`;
        return htmlContent;
    }
    function calculatePercentage(used, total) {
        return total > 0 ? Math.round((used / total) * 100) : 0;
    }
    function formatMemory(memoryInMB) {
        const memoryInGB = memoryInMB / 1024;
        return `${memoryInGB.toFixed(2)} GB`;
    }
    function stopAutoReload() {
        clearInterval(autoReloadTimer)
    }
    function startAutoReload() {
        stopAutoReload()
        autoReloadTimer = setInterval(() => {
            drawSystemInfo()
        },5000)
    }
    theEnclosure.on('click','[systemStatus-action]',function(){
        var el = $(this)
        var expAction = el.attr('systemStatus-action')
        switch(expAction){
            case'getInfo':
                drawSystemInfo()
            break;
            case'startAutoReload':
                drawSystemInfo()
                startAutoReload()
            break;
            case'stopAutoReload':
                stopAutoReload()
            break;
        }
    });
    $('[href="#systemStatus"]')
        .on('shown.bs.tab', async function(e) {
            drawSystemInfo()
            startAutoReload()
        })
        .on('hidden.bs.tab', async function(e) {
            stopAutoReload()
        });
})
