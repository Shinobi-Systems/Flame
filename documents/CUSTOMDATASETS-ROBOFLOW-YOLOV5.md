> Flame must be updated to use this feature. Currently experimental.

# How to use RoboFlow YoloV5 Datasets

1. Find the dataset you want in **YOLO v5 PyTorch** format and download with **show download code** selected.
    - For Example : https://universe.roboflow.com/denis-shishkov-ldf7t/wine-bottles/dataset/4
2. Click continue and open the **Raw URL** tab of the **Your Download Code** window. Copy the URL shown.
3. Open Flame interface and go to `Custom Datasets` tab and paste the URL in the **Download URL** field and press the download icon next to it.
    - Once downloaded the list will reload.
4. Find the dataset in the listing and click `Link`. The dataset will become active for training.
5. Open `Dataset Viewer` to confirm which data is now active.
    - *`Dataset Viewer` previously displayed all images downloaded from Google Open Images instead of the Active Training datasets. Now it only displays the active datasets.*
