# Interface Notes

**Last Updated** : 2024-01-22

1. **Training Control**
    - You will see the status of the current training session here. Set the following fields to start training.
        - **Classes** : The Object Tags that will be trained upon. Must only use Classes that have data existing already.
        - **Relationships** : An additional parameter to find Object Tags doing certain things.
        - **Batch Size** : In simple terms, a higher number requires more VRAM on the GPU.
        - **Epochs** : Number of times for the trainer to iterate over the dataset for training. Beware of too many Epochs as it can cause "over-lifting", which means the model is looking mainly for features nearly identical to the training set. Leaving it loose allows the inference engine to allow lenience when appraising contour similarity.
        - **Base Weights** : This is the base model we use to train the data. In short we have 3 main models to start with. Each are for different deployment scenarios.
            - **yolov5s** : Runs on Edge Servers, Lower Accuracy
            - **yolov5m** : Runs on Mid-Level GPU, Medium Accuracy
            - **yolov5l** : Requires Powerful GPU
        - **Device** : The selected GPUs to train upon. By default the first GPU is always `0`. If you have more you can fill it like `0,1,2` to add the others.
2. **Dataset Viewer**
    - This allows us to see what files are existing in the file system. Currently only displays the datasets downloaded from Open Images Database.
3. **Download Images**
    - This image downloader is based on Open Images Database. It will download Images based on the chosen Classes and Relationships. You can also set a max number of images to download for each class.
4. **Training Sessions**
    - This will display the results from previous training sessions. You may see images in a selection. These are the images that are being trained and tested upon.
    - Some images will display only the ID number of the class (starting from 0 based on the Classes provided). While others will show the complete name. When it begins showing the complete name like "Person" you may notice it appears with a number "Person 0.9". This means that this image is a test result during the training, it has 90% confidence it sees a "Person".
5. **Tests**
    - This tool can run a test independently of the training process. These tests are logged here. By default there may be only 2 images here for the test. You can add more images to the tester by simply adding images to `FLAME_ROOT/yolov5/data/images`. The `FLAME_ROOT/yolov5` folder will only exist after installation of Flame is complete.
6. **Video Annotator** *(WIP, Incomplete)*
    - A tool to allow uploading a video and quickly annotating frames in it.
7. **System Status**
    - This will allow you to see RAM Usage and GPU Usage. Since CPU Usage is less relevant here it is not included.
8. **Software Used**
    - A list of softwares and repositories used to make Flame work.

# Class Names for Google Open Images Downloader

https://storage.googleapis.com/openimages/v7/oidv7-class-descriptions-boxable.csv

# Relationships

```
at
holds
wears
surf
hang
drink
holding_hands
on
ride
dance
skateboard
catch
highfive
inside_of
eat
cut
contain
handshake
kiss
talk_on_phone
interacts_with
under
hug
throw
hits
snowboard
is
kick
ski
plays
read
```
