# How to use a created Model (Weights set) with Shinobi

1. Open Flame
2. Go to **Training Sessions**
3. Find the session you want to use and click **Package**.
4. Wait until a notice appears to ask you to **Download**.
    - You can find these packages in the **Saved Packges** page.
5. Extract the contents of the downloaded zip into the `weights` folder of the `YoloV5 PyTorch Plugin` for Shinobi.


# Installing the YoloV5 Shinobi Plugin

https://gitlab.com/Shinobi-Systems/shinobi-plugins/-/tree/master/plugins/yolo-v5-pt

1. Download this zip and copy the `yolo-v5-pt` folder to your `Shinobi/plugins` folder.
2. Enter the `yolo-v5-pt` folder and run `npm i`.
3. *Copy the downloaded package contents of the model to `weights`*. The one downloaded from **Training Sessions**.
4. Simple start the plugin by running
    ```
    node shinobi-yolov5-pt.js
    ```
4. Daemonize the plugin by running. *You need to be root do this.*
    ```
    pm2 start shinobi-yolov5-pt.js && pm2 save
    ```
